# Development

# 1.4.1-2

* Support API level 32

# 1.4.1

## Features

* Add vertical scrollbar on article view ([!141](https://gitlab.com/christophehenry/freshrss-android/-/merge_requests/141))
* Add possibility to categorize when adding a feed ([!149](https://gitlab.com/christophehenry/freshrss-android/-/merge_requests/149))

## Bug fixes

* Fixes [#118](https://gitlab.com/christophehenry/freshrss-android/-/issues/118): Article position is now saved when app is swiped way ([!150](https://gitlab.com/christophehenry/freshrss-android/-/merge_requests/150))
* Fixes [#120](https://gitlab.com/christophehenry/freshrss-android/-/issues/120): Swipe right to mark read an article doesn't work when navigating back immediatly after ([!150](https://gitlab.com/christophehenry/freshrss-android/-/merge_requests/150))
* Fixes crash when adding a feed ([!149](https://gitlab.com/christophehenry/freshrss-android/-/merge_requests/149))

# 1.4.0

## Features

* Complete rewrite of the login page ([!115](https://git.feneas.org/christophehenry/freshrss-android/-/merge_requests/115))
* Support correct autoifill from password managers like KeePassDX ([!115](https://git.feneas.org/christophehenry/freshrss-android/-/merge_requests/115))

## Bug fixes

* Fixes [#27](https://git.feneas.org/christophehenry/freshrss-android/-/issues/27) adding indications about: ([!115](https://git.feneas.org/christophehenry/freshrss-android/-/merge_requests/115))
    * when entered URL seems incorrect,
    * when API endpoint seems not correct,
    * when /p/'s parent is exposed,
    * when API access is disabled,
    * when credentials are incorrect because you may not have set the API password.
* Fixes [#92](https://git.feneas.org/christophehenry/freshrss-android/-/issues/92): URL are not trimmed in the login page ([!115](https://git.feneas.org/christophehenry/freshrss-android/-/merge_requests/115))
* Fixes [#93](https://git.feneas.org/christophehenry/freshrss-android/-/issues/93): crash when login process returns something else than 200 ([!115](https://git.feneas.org/christophehenry/freshrss-android/-/merge_requests/115))
* Fixes [#102](https://git.feneas.org/christophehenry/freshrss-android/-/issues/102) and [#114](https://git.feneas.org/christophehenry/freshrss-android/-/issues/114): issue when sepcifying URL with port ([!115](https://git.feneas.org/christophehenry/freshrss-android/-/merge_requests/115))

# 1.3.6

## Bug fixes

* Fix regression in last sync timing display [!126](https://git.feneas.org/christophehenry/freshrss-android/-/merge_requests/126)

# 1.3.5

## Features

* Support self-signed certificated ([!124](https://git.feneas.org/christophehenry/freshrss-android/-/merge_requests/124))

# 1.3.4

## Bug fixes

* Fix [#106](https://git.feneas.org/christophehenry/freshrss-android/-/issues/106) regression introduced in 1.3.3 ([!119](https://git.feneas.org/christophehenry/freshrss-android/-/merge_requests/119)) 

# 1.3.3

## Features

* Add possibility to enable a debug mode ([!108](https://git.feneas.org/christophehenry/freshrss-android/-/merge_requests/108))
* Add possibility to send a report of failed refreshed when debug mode is enabled ([!108](https://git.feneas.org/christophehenry/freshrss-android/-/merge_requests/108))
* Update dependencies ([!117](https://git.feneas.org/christophehenry/freshrss-android/-/merge_requests/117))

## Bug fixes

* Fix some context crashes in fragments ([!110](https://git.feneas.org/christophehenry/freshrss-android/-/merge_requests/110))
* Fix [#103](https://git.feneas.org/christophehenry/freshrss-android/-/issues/103): retrieving articles restults in 404 ([!110](https://git.feneas.org/christophehenry/freshrss-android/-/merge_requests/110))
* Make english sentence more natural to native speakers ([!109](https://git.feneas.org/christophehenry/freshrss-android/-/merge_requests/109))
* Fix a few more crashes and concurrent problems ([!118](https://git.feneas.org/christophehenry/freshrss-android/-/merge_requests/118))

# 1.3.2

## Bug fixes

* Fix [#99](https://git.feneas.org/christophehenry/freshrss-android/-/issues/99): on first sync, images are not automatically refreshed anymore ([!105](https://git.feneas.org/christophehenry/freshrss-android/-/merge_requests/105))
* Fix [#100](https://git.feneas.org/christophehenry/freshrss-android/-/issues/100): refresh fails when unread article list is huge ([!106](https://git.feneas.org/christophehenry/freshrss-android/-/merge_requests/106))
* Updating french translations ([!116](https://git.feneas.org/christophehenry/freshrss-android/-/merge_requests/116))

# 1.3.1

## Features

* Updating arabic translations ([!101](https://git.feneas.org/christophehenry/freshrss-android/-/merge_requests/101))
* Updating french translations ([!95](https://git.feneas.org/christophehenry/freshrss-android/-/merge_requests/95))

## Bug fixes

* Fix [#95](https://git.feneas.org/christophehenry/freshrss-android/-/issues/95): App sometimes crash when closing article ([!97](https://git.feneas.org/christophehenry/freshrss-android/-/merge_requests/97))
* Fix [#96](https://git.feneas.org/christophehenry/freshrss-android/-/issues/96): app crashes when trying to login to a bad URL ([!98](https://git.feneas.org/christophehenry/freshrss-android/-/merge_requests/98))
* Fix [#98](https://git.feneas.org/christophehenry/freshrss-android/-/issues/98): automatically refresh if needed on activity resume ([!100](https://git.feneas.org/christophehenry/freshrss-android/merge_requests/100))
* Fix [#94](https://git.feneas.org/christophehenry/freshrss-android/-/issues/94): refresh fails when feed icon contains errors ([!102](https://git.feneas.org/christophehenry/freshrss-android/-/merge_requests/102))

# 1.3.0

## Features

* Slide animation on navigation transitions ([!92](https://git.feneas.org/christophehenry/freshrss-android/merge_requests/92))
* Stop refreshing on application startup for better resource consumption  ([!92](https://git.feneas.org/christophehenry/freshrss-android/merge_requests/92))
* Implements [#90](https://git.feneas.org/christophehenry/freshrss-android/issues/90): Retain scroll position throughout navigation on feed list & article ([!91](https://git.feneas.org/christophehenry/freshrss-android/merge_requests/91))
* Implement [#46](https://git.feneas.org/christophehenry/freshrss-android/issues/46): compute unread articles count for subscription category ([!89](https://git.feneas.org/christophehenry/freshrss-android/merge_requests/89))
* Better handle images embedded in a link by showing the link seperatly ([!63](https://git.feneas.org/christophehenry/freshrss-android/merge_requests/63))
* Add a notification to report crashes ([!67](https://git.feneas.org/christophehenry/freshrss-android/merge_requests/67))
* Implement [#52](https://git.feneas.org/christophehenry/freshrss-android/issues/52): support favorites ([!88](https://git.feneas.org/christophehenry/freshrss-android/merge_requests/88))

## Bug fixes

* Fix [#83](https://git.feneas.org/christophehenry/freshrss-android/issues/83): old categories are not removed after refresh ([!89](https://git.feneas.org/christophehenry/freshrss-android/merge_requests/89))
* Fix [#89](https://git.feneas.org/christophehenry/freshrss-android/issues/89): feed title is not correctly removed from article title ([!87](https://git.feneas.org/christophehenry/freshrss-android/merge_requests/87))
* Fix [#84](https://git.feneas.org/christophehenry/freshrss-android/issues/84): article view crashing on Android 5.0 and 5.1 ([!74](https://git.feneas.org/christophehenry/freshrss-android/merge_requests/74))
* Fix a bug preventing to refresh when subscription lists are empty ([!62](https://git.feneas.org/christophehenry/freshrss-android/merge_requests/62/))
* Fix [#82](https://git.feneas.org/christophehenry/freshrss-android/issues/82): home and back buttons are sometimes not displayed correctly or not displayed at all ([!71](https://git.feneas.org/christophehenry/freshrss-android/merge_requests/71))
* Fix [#80](https://git.feneas.org/christophehenry/freshrss-android/issues/80): multiple performance bugs in the subscriptions and subscription's articles pages display ([!65](https://git.feneas.org/christophehenry/freshrss-android/merge_requests/65))
* Fix [#67](https://git.feneas.org/christophehenry/freshrss-android/issues/67): swipe right to flag article as 'read' sometime doesn't work ([#65](https://git.feneas.org/christophehenry/freshrss-android/merge_requests/65))

## Refactoring

* Refactor the waiting fragment to lighten the interface ([!62](https://git.feneas.org/christophehenry/freshrss-android/merge_requests/62/))
* Fix [#81](https://git.feneas.org/christophehenry/freshrss-android/issues/81): Refresh mechanism doesn't work properly ([#80](https://git.feneas.org/christophehenry/freshrss-android/merge_requests/80))
* Drop Fuel & Kovenant [#77](https://git.feneas.org/christophehenry/freshrss-android/issues/77) in favor of native Kotlin coroutines and Retrofit ([79!](https://git.feneas.org/christophehenry/freshrss-android/merge_requests/79) & [!80](https://git.feneas.org/christophehenry/freshrss-android/merge_requests/80))

# 1.2.2

## Refactoring

* Bump all dependancies ([!59](https://git.feneas.org/christophehenry/freshrss-android/merge_requests/59))

# 1.2.1

## Features

* Add arabic translations ([!57](https://git.feneas.org/christophehenry/freshrss-android/merge_requests/57))

# 1.2.0

## Features

* Implements [#54](https://git.feneas.org/christophehenry/freshrss-android/issues/54): Add fast scroller bar with section display ([!17](https://git.feneas.org/christophehenry/freshrss-android/merge_requests/17))
* Implements [#39](https://git.feneas.org/christophehenry/freshrss-android/issues/39): Better accessibility for the article-related actions ([!19](https://git.feneas.org/christophehenry/freshrss-android/merge_requests/19))
* Implements [#49](https://git.feneas.org/christophehenry/freshrss-android/issues/49): Emotionnal design ([!35](https://git.feneas.org/christophehenry/freshrss-android/merge_requests/35))
* Implements [#66](https://git.feneas.org/christophehenry/freshrss-android/issues/66): French translations (Fipaddict, [!38](https://git.feneas.org/christophehenry/freshrss-android/merge_requests/38))
* Implements [#34](https://git.feneas.org/christophehenry/freshrss-android/issues/34): Implements feeds retrieval scheduling in settings as well as hability to disable it ([!53](https://git.feneas.org/christophehenry/freshrss-android/merge_requests/53))
* Implements [#74](https://git.feneas.org/christophehenry/freshrss-android/issues/74): Ability to add a new feed ([!54](https://git.feneas.org/christophehenry/freshrss-android/merge_requests/54))

## Bug fixes
 
* [#50](https://git.feneas.org/christophehenry/freshrss-android/issues/50): Categories are not alphabetically sorted
* [#51](https://git.feneas.org/christophehenry/freshrss-android/issues/51): Unread subscription time groups are not time-sorted
* [#55](https://git.feneas.org/christophehenry/freshrss-android/issues/55): Links click open page in the webview ([!18](https://git.feneas.org/christophehenry/freshrss-android/merge_requests/18))
* [#58](https://git.feneas.org/christophehenry/freshrss-android/issues/58): HTTP cleartext traffic is disallowed starting with Android 9 ([!36](https://git.feneas.org/christophehenry/freshrss-android/merge_requests/36))
* [#65](https://git.feneas.org/christophehenry/freshrss-android/issues/65): Synchronisation fails when article JSON is malformed ([!50](https://git.feneas.org/christophehenry/freshrss-android/merge_requests/50))

## Refactoring

* [#18](https://git.feneas.org/christophehenry/freshrss-android/issues/18): Setup a code linter and run lints ([!24](https://git.feneas.org/christophehenry/freshrss-android/merge_requests/24), [!26](https://git.feneas.org/christophehenry/freshrss-android/merge_requests/26))

# 1.1.0

## Features

* Swipe gesture to navigate between subscription sections ([a0a0b690](https://git.feneas.org/christophehenry/freshrss-android/commit/a0a0b690dc2e720e195449ea2865ff2ca2d11b41))
* Sort subscriptions alphabetically in *all* section and by newest item crawl date in *unread* section ([a0a0b690](https://git.feneas.org/christophehenry/freshrss-android/commit/a0a0b690dc2e720e195449ea2865ff2ca2d11b41))
* Implement [#9](https://git.feneas.org/christophehenry/freshrss-android/issues/9): pull-to-refresh pattern to sync with server ([65f48ded](https://git.feneas.org/christophehenry/freshrss-android/commit/65f48ded3bfb8b1cb2aa50578c5078f25c75a57a))
* Add animation in views's transtions ([4f84e6b5](https://git.feneas.org/christophehenry/freshrss-android/commit/4f84e6b5f6de7d42a24d541e099a402d034792da))
* Implement [#10](https://git.feneas.org/christophehenry/freshrss-android/issues/10) : fetch subscription's icons and display them ([!4](https://git.feneas.org/christophehenry/freshrss-android/merge_requests/4))
* Add sections and section headers to subscriptions ([!10](https://git.feneas.org/christophehenry/freshrss-android/merge_requests/5))
* Implement [#45](https://git.feneas.org/christophehenry/freshrss-android/issues/45): add badge to unread articles section to indicate total count of unread articles ([!9](https://git.feneas.org/christophehenry/freshrss-android/merge_requests/9))
* Implement [#31](https://git.feneas.org/christophehenry/freshrss-android/issues/31): browse feeds by category ([!10](https://git.feneas.org/christophehenry/freshrss-android/merge_requests/10))
* Implement [#12](https://git.feneas.org/christophehenry/freshrss-android/merge_requests/12): CSS for article detail ([!12](https://git.feneas.org/christophehenry/freshrss-android/merge_requests/12))

## Bug fixes

* Fix service lately binded to application causing stacktrace ([a58c00dd](https://git.feneas.org/christophehenry/freshrss-android/commit/a58c00dd8c8c6e292a0210a64a38238b253978a4))
* Fix loader displaying infinitely when a subscription section stays empty after refresh by displaying a hint text stating section is empty ([13b7c02c](https://git.feneas.org/christophehenry/freshrss-android/commit/13b7c02c28bfdfd543de668006dff161330e3b75))
* Fix [#38](<https://git.feneas.org/christophehenry/freshrss-android/issues/38>]: empty screen when going back from the initial screen ([4f84e6b5](https://git.feneas.org/christophehenry/freshrss-android/commit/4f84e6b5f6de7d42a24d541e099a402d034792da))
* Fix [#13](https://git.feneas.org/christophehenry/freshrss-android/issues/13) and [#14](https://git.feneas.org/christophehenry/freshrss-android/issues/14): erratic notification behavior ([0c1b5e76](https://git.feneas.org/christophehenry/freshrss-android/commit/0c1b5e7600888d905f67dc28b0389229b52b67f7))
* Fix crash hapening when object is returned from `unread-counts` endpoint without `newestItemTimestampUsec` property ([8cfcd932](https://git.feneas.org/christophehenry/freshrss-android/commit/8cfcd932619b601371b6f0d0a4bcb7d4b92ce3dd))
* Fix spinner infinitely loading when comming back from a feed with a single unread article ([3088922f](https://git.feneas.org/christophehenry/freshrss-android/commit/3088922f9405ee173d6a6cb81b8868a35759aedd))
* Fix articles in subscriptions not being sorted by publication date ([3088922f](https://git.feneas.org/christophehenry/freshrss-android/commit/3088922f9405ee173d6a6cb81b8868a35759aedd))
* Fix [#26](https://git.feneas.org/christophehenry/freshrss-android/issues/26): UTF-8 problem on article detail on android 5.0 ([!7](https://git.feneas.org/christophehenry/freshrss-android/merge_requests/7))

## Refactoring

* Feed subscriptions uses a local DB ([edfd4fc5](https://git.feneas.org/christophehenry/freshrss-android/commit/edfd4fc5cde846ca6040c55b31179e107b654ddf))
* Cleanup in DataBing classes ([353f37d1](https://git.feneas.org/christophehenry/freshrss-android/commit/353f37d15c12cad108610ec3061d7f09503b290b))
* [#21](https://git.feneas.org/christophehenry/freshrss-android/issues/21): Transfrom DB instance from a singleton to an application's property ([!8](https://git.feneas.org/christophehenry/freshrss-android/merge_requests/8))
* [#22](https://git.feneas.org/christophehenry/freshrss-android/issues/22): Refactor AuthTokenDelegates to use RxJava's Flowable ([!8](https://git.feneas.org/christophehenry/freshrss-android/merge_requests/8))

# 1.0.1

## Bug fixes

* Fix Jackson crash when runing on an API level < 24 ([90c65dc7](https://git.feneas.org/christophehenry/freshrss-android/commit/90c65dc7387c94689e8bffd4f80168d29c909bae))

# 1.0.0

## Features

Basic implementation using FreshRSS' GReader HTTP API implmentation. This lets you:

 * connect to one account
 * browse all subscriptions
 * browse subscriptions with unread articles
 * see articles for a subscription
 * see unread articles for a subscription
 * read an article
 * set an article as read/unread
 * share an article to other android application
 * open the original page of the article
 * refresh your feed
