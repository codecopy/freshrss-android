# FreshRSS Android application

Finally, an Android application specifically dedicated to the awesome [FreshRSS aggregator](https://freshrss.org/)

<img src="./fastlane/metadata/android/en-US/images/phoneScreenshots/1-splash-screen.png" alt="splash screen" width="200" />

<img src="./fastlane/metadata/android/en-US/images/phoneScreenshots/2-login-1.png" alt="splash screen" width="200" />&nbsp;<img src="./fastlane/metadata/android/en-US/images/phoneScreenshots/2-login-2.png" alt="login page" width="200" />  

<img src="./fastlane/metadata/android/en-US/images/phoneScreenshots/3-landing-all.png" alt="landing all" width="200" />&nbsp;<img src="./fastlane/metadata/android/en-US/images/phoneScreenshots/4-landing-unread.png" alt="landing unread" width="200" />  

<img src="./fastlane/metadata/android/en-US/images/phoneScreenshots/5-subscription-articles.png" alt="subscription articles" width="200" />&nbsp;<img src="./fastlane/metadata/android/en-US/images/phoneScreenshots/6-article-detail.png" alt="article details" width="200" />

<img src="./fastlane/metadata/android/en-US/images/phoneScreenshots/11-add-feed.png" alt="add feed panel" width="200" />

## How to use?

First of all, set the API password. This password is different from the one you use for login into your account. 
You can set it on `https://<your_instance>/i/?c=user&a=profile`.

<img src="./fastlane/metadata/img/api-password.png" alt="API password setup page on /i/?c=user&a=profile" width="200" />

Then, enter your regular account login, this password and the regular landing page URL of your FreshRSS instance. 
The application should compute the API URL automatically.

<img src="./fastlane/metadata/img/login.png" alt="API password setup page on /i/?c=user&a=profile" width="200" />

## Contact & troubleshoot

If you need help for contributing or using the application, you can contact us by [joining us on Framateam](https://framateam.org/signup_user_complete/?id=e2680d3e3128b9fac8fdb3003b0024ee).

## Getting started

This project is a fairly standard Android application. As long as you use an correct setup of [Android studio](https://developer.android.com/studio/), 
everything should be fine.

## Running the tests

Please refer to the [tests and linters section](./CONTRIBUTING.md#running-tests-and-linter) in 
the contributing guide to find how to run the tests.

## Contributing

 Please refer to the [contributing guide](./CONTRIBUTING.md)

## License

This project is licenced under GNU GPL v.3. See [LICENSE](https://git.feneas.org/christophehenry/freshrss-android/blob/develop/LICENSE).

## Aknowledgments

Thanks to [Marien Fressinaud](https://github.com/marienfressinaud), 
[Alexandre Alapetite](https://github.com/Alkarex) and all the FreshRSS contributors for their awesome work. 
And thanks again to Alexandre for helping in understanding their implementation of Google reader's API.
