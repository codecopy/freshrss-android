import java.util.Properties
import java.lang.NumberFormatException

import org.gradle.api.Project
import org.gradle.api.model.ObjectFactory
import org.gradle.api.GradleException

open class AppVersioningExtensions internal constructor(
    @Suppress("UNUSED_PARAMETER") objects: ObjectFactory,
    project: Project
) {
    private val filePath = "${project.rootProject.projectDir}/.versions.properties"

    private val properties by lazy {
        val versionsFile = project.file(filePath)

        if(!versionsFile.exists() || ! versionsFile.canRead()) {
            throw GradleException("$filePath does not exists or is inaccessible")
        }

        Properties().apply {
            load(versionsFile.reader())
        }
    }

    val versionName: String
        get() {
            try {
                return properties.getProperty("versionName").trim()
            } catch(e: Exception) {
                throw GradleException("Property versionName is not defined in $filePath")
            }
        }

    val versionCode: Int
        get() {
            try {
                return properties.getProperty("versionCode").trim().toInt()
            } catch(e: Exception) {
                throw GradleException(
                    "Property versionCode is not defined or is not a integer in $filePath"
                )
            }
        }
}
