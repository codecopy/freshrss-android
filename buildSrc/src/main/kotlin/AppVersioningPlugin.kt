import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.model.ObjectFactory
import org.gradle.kotlin.dsl.create

class AppVersioningPlugin: Plugin<Project> {
    override fun apply(project: Project) {
        project.extensions.create<AppVersioningExtensions>("appVersions", project)
    }
}
