# Foreword

## Contributing is not only about throwing code

Any project needs several different profiles. Contributing is not only about producing code. 
A good project is a project that has a triving community producing improvement propositions, 
graphic assets, communication, translations, etc.

If you think you are not able to contribute because you cannot produce code: **DON'T**. Please...

Developers are not all-knowing people. They need to colaborate with people having multiple talents. 
Opening a ticket to raise a bug or ask for a feature is already a huge contribution. 
Translating text, propose interface improvement or draw graphics also are.

## If you don't know something, ask

It's ok to not know how to develop an Android application or to use `git`. 
Documentations are good assets if you know what you are searching for and 
how to formulate the question, but sometimes, it's not enough.

In such cases, it's really ok to ask. It's faster and easier and you can be guided. 
Never hesitate to seek help among other contributor. Life is all about learning!

# Technique

## Setup

Before you start developing, you should install [Android studio](https://developer.android.com/studio/install), 
Android's, integrated development environment.

This is a fairly standard Android project. There are a few things to know, though, before contributing...

1. This project is a pure Kotlin project. No Java will be tolerated. 
The original author is highly allergic to Java and was very happy to see the Android team embrace Kotlin. 
Since Kotlin is becoming the standard to develop Android applications 
(Android's support library are being rewritten in Kotlin, Jetpack — the Android library for rapid application development — 
is also written in Kotlin, and examples in Android's documentation show Kotlin code by default), this project adopted Kotlin.

2. It also heavily relies on [Jetpack](https://developer.android.com/jetpack/), the Android's rapid application development library. 
In particular, it uses:
    * [JP's data binding](https://developer.android.com/topic/libraries/data-binding/)
    * [`LiveData` classes](https://developer.android.com/topic/libraries/architecture/livedata)
    * [Navigation](https://developer.android.com/topic/libraries/architecture/navigation/)
    * [Room persistence library](https://developer.android.com/topic/libraries/architecture/room) for database interaction,
    * [`ViewModel`s](https://developer.android.com/topic/libraries/architecture/viewmodel)
It would be advised to read [Android's guide to app architecture](https://developer.android.com/jetpack/docs/guide)

3. It uses [Kovenant](http://kovenant.komponents.nl/), as Kotlin promise library to perform async operations.

4. It uses [Fuel](https://fuel.gitbook.io/documentation/) for HTTP requests.

5. You can debug your application using [Facebook's Stetho](http://facebook.github.io/stetho/).

6. Serialization and deserialization of HTTP request is handled by [Jackson](https://github.com/FasterXML/jackson-core).

## Where to start

Pick [a ticket](https://git.feneas.org/christophehenry/freshrss-android/issues), 
preferably, on that is marked for the next release and start working on it.

Apart from application's features and development, this project is severely 
lacking tests and a good CI pipeline. Feel free to set them up if you feel 
you have the knowledge.

If you are not a developer, you can contribute by localizing the application in your language. 
Please read the [Android localization guide](https://developer.android.com/guide/topics/resources/localization).

## Running tests and linter

### Linters

Ensure you ran the linters:

```sh
./gradlew lintAllFix
```

You can automatically configure Android studio to respect the Kotlin lint rules. 
First, download and instal [ktlint](https://ktlint.github.io/) whereever you wish. 
Then, run:

```sh
ktlint --apply-to-idea-project
```

You can also add a git hook the will warn you of lint offenses before you commit:

```sh
ktlint installGitPreCommitHook 
```

To run the Kotlin lint rules alone:

```sh
ktlint -F "app/src/**/*.kt"
```

This will autocorrect what can be autocorrected. Other errors will be printed.

### Tests

You can run unit tests with the command:

```sh
./gradlew testDebugUnitTest
```

And integration tests with the command:

```sh
./gradlew clean connectedDebugAndroidTest
```

# Contributors

## The community and the users

Thank you a lot for using this app, talking about it, loving it ❤️

## Translators

If you want to translate in your language feel free to do so. 
Read [Android's documentation on translations](https://developer.android.com/studio/write/translations-editor).

* [Fipaddict](https://git.feneas.org/Fipaddict) (French)
* [ButterflyOfFire](https://git.feneas.org/BoFFire) (Arab)
* Brett Cornwall (English)

## UI Graphism

There are no graphists yet. If you think you can improve the look of the application 
please, submit propositions. We desperatly need you.

## UX

* Natouille

## Ticket openers

These section thanks the users opening ticket to raise bug or propose features

* [GLLM](https://git.feneas.org/GLLM)

And others: also thanks to you, anonymous ones that open only one and a fistful of tickets 
on the project. You, too, matter and deserve a thank you.

## Devs

* [Christophe](https://git.feneas.org/christophehenry)
* [Akshay S Dinesh](https://git.feneas.org/asdofindia)
