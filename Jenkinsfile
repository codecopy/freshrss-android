pipeline {
    agent any

    environment {
        MIN_SDK_IMAGE = "system-images;android-21;default;x86_64"
        MAX_SDK_IMAGE = "system-images;android-30;default;x86_64"
        APK_BUILD_BRANCH = "develop"
        MOCK_WEB_SERVER_PORT = "8888"
    }

    options {
        buildDiscarder(logRotator(numToKeepStr: "2"))
        skipStagesAfterUnstable()
        gitLabConnection("FreshRSSGitlab")
        gitlabBuilds(builds: getBuildSteps(env.BRANCH_NAME))
        disableConcurrentBuilds()
        newContainerPerStage()
        timestamps()
        timeout(time: 30, unit: "MINUTES")
    }

    triggers {
        gitlab(
            branchFilterType: "All",
            triggerOnPush: true,
            triggerOnMergeRequest: false,
            triggerOpenMergeRequestOnPush: "never",
            triggerOnNoteRequest: true,
            noteRegex: "Jenkins please retry a build",
            skipWorkInProgressMergeRequest: false,
            ciSkip: true,
            setBuildDescription: true,
            addNoteOnMergeRequest: true,
            addCiMessage: true,
            addVoteOnMergeRequest: true,
            acceptMergeRequestOnSuccess: false,
            cancelPendingBuildsOnUpdate: true
        )
    }

    libraries {
        lib("android-pipeline-steps")
    }

    stages {
        stage("Lint") {
            steps {
                gitlabCommitStatus("Lint") {
                    sh "./gradlew spotlessCheck lint"
                    recordIssues(tools: [androidLintParser(pattern: "**/lint-results*.xml")])
                }
            }
        }

        stage("Unit tests") {
            steps {
                gitlabCommitStatus("Unit tests") {
                    sh "./gradlew testDebugUnitTest"
                    recordIssues(tools: [junitParser(pattern: "**/test-results/**/*.xml")])
                }
            }
        }

        stage("on min SDK level") {
            steps {
                gitlabCommitStatus("Instrumented tests on min SDK image") {
                    withAvd(
                        hardwareProfile: "Nexus 5X",
                        systemImage: env.MIN_SDK_IMAGE,
                        headless: true
                    ) {
                        sh "./gradlew clean connectedDebugAndroidTest"
                    }
                }
            }
        }

        stage("on max SDK level") {
            steps {
                gitlabCommitStatus("Instrumented tests on max SDK image") {
                    withAvd(
                        hardwareProfile: "Nexus 5X",
                        systemImage: env.MAX_SDK_IMAGE,
                        headless: true
                    ) {
                        sh "./gradlew clean connectedDebugAndroidTest"
                    }
                }
            }
        }

        stage("Build APK") {
            steps {
                gitlabCommitStatus("Build APK") {
                    withCredentials([usernamePassword(
                        credentialsId: "freshrss-signkey",
                        usernameVariable: "SIGN_KEY_CREDENTIALS_USR",
                        passwordVariable: "SIGN_KEY_CREDENTIALS_PSW"
                    )]) {
                        sh("""
                            ./gradlew clean assembleJenkins \
                                -Pandroid.injected.signing.store.file=$SIGN_KEY_PATH \
                                -Pandroid.injected.signing.store.password=${'$'}SIGN_KEY_CREDENTIALS_PSW \
                                -Pandroid.injected.signing.key.alias=${'$'}SIGN_KEY_CREDENTIALS_USR \
                                -Pandroid.injected.signing.key.password=${'$'}SIGN_KEY_CREDENTIALS_PSW
                        """)
                    }

                    archiveArtifacts artifacts: "**/*.apk", fingerprint: true, onlyIfSuccessful: true
                }
            }

            when { branch env.APK_BUILD_BRANCH }
        }
    }
}

def getBuildSteps(String branch) {
    def buildSteps = [
        "Lint",
        "Unit tests",
        "Instrumented tests on min SDK image",
        "Instrumented tests on max SDK image",
    ]
    if(branch == env.APK_BUILD_BRANCH) return buildSteps + ["Build APK"]
    return buildSteps
}