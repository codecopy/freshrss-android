package fr.chenry.android.freshrss.store.api.models

import fr.chenry.android.freshrss.BaseTest
import fr.chenry.android.freshrss.Mocks
import fr.chenry.android.freshrss.store.api.utils.JACKSON_OBJECT_MAPPER
import org.joda.time.DateTimeZone
import org.joda.time.LocalDateTime
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class ContentItemsHandlerTest : BaseTest() {
    @Before
    fun setUp() {
        Mocks.mockLog()
    }

    @Test
    fun testDeserNominal() {
        val json = getFixture("ContentItemsHandlerFixtures/nominal.json")

        val items: ContentItems =
            listOf(
                ContentItem(
                    "tag:google.com,2005:reader/item/00058167f526c8be",
                    LocalDateTime("2019-02-08T20:39:38.127", DateTimeZone.UTC),
                    LocalDateTime("2019-02-08T20:39:38.127", DateTimeZone.UTC),
                    LocalDateTime("2019-01-14T19:18:00.000", DateTimeZone.UTC),
                    "Eolas contre Institut pour la Justice : Episode 1. Le Compteur Fantôme.",
                    listOf(Href("http://localhost/post/2019/01/14/abcd")),
                    listOf(
                        "user/-/state/com.google/reading-list",
                        "user/-/label/Misc",
                        "user/-/state/com.google/read"
                    ),
                    ContentItemOrigin("feed/10", "Journal d'un avocat"),
                    Summary("\n<p><span>L’affaire judiciaire m’ayant"),
                    "Eolas"
                ),
                ContentItem(
                    "tag:google.com,2005:reader/item/00058167f526c759",
                    LocalDateTime("2019-02-08T20:39:38.127", DateTimeZone.UTC),
                    LocalDateTime("2019-02-08T20:39:38.127", DateTimeZone.UTC),
                    LocalDateTime("2018-09-20T12:10:00.000", DateTimeZone.UTC),
                    "Petite leçon de droit à l'attention de Madame Le Pen (et de M. Mélenchon qui passait par là)",
                    listOf(Href("http://localhost/post/2019/01/14/defg")),
                    listOf(
                        "user/-/state/com.google/reading-list",
                        "user/-/label/Misc",
                        "user/-/state/com.google/read"
                    ),
                    ContentItemOrigin("feed/10", "Journal d'un avocat"),
                    Summary(
                        "\n<p>Mon ancienne consœur Marion “Marine” Le Pen s’offusque à qui veut l’entendre,"
                    ),
                    "Eolas"
                )
            )
        val target = JACKSON_OBJECT_MAPPER.readValue(json, ContentItemsHandler::class.java)

        val expected =
            ContentItemsHandler(
                "user/-/state/com.google/reading-list",
                1563874362,
                items,
                "1549658378126328"
            )

        assertEquals(expected, target)
    }

    @Test
    fun testDeserNullContent() {
        val json = getFixture("ContentItemsHandlerFixtures/with-null-content.json")

        val items: ContentItems =
            listOf(
                ContentItem(
                    "tag:google.com,2005:reader/item/00058167f526c8be",
                    LocalDateTime("2019-02-08T20:39:38.127", DateTimeZone.UTC),
                    LocalDateTime("2019-02-08T20:39:38.127", DateTimeZone.UTC),
                    LocalDateTime("2019-01-14T19:18:00.000", DateTimeZone.UTC),
                    "Eolas contre Institut pour la Justice : Episode 1. Le Compteur Fantôme.",
                    listOf(Href("http://localhost/post/2019/01/14/abcd")),
                    listOf(
                        "user/-/state/com.google/reading-list",
                        "user/-/label/Misc",
                        "user/-/state/com.google/read"
                    ),
                    ContentItemOrigin("feed/10", "Journal d'un avocat"),
                    Summary("\n<p><span>L’affaire judiciaire m’ayant"),
                    "Eolas"
                )
            )
        val target = JACKSON_OBJECT_MAPPER.readValue(json, ContentItemsHandler::class.java)

        val expected =
            ContentItemsHandler(
                "user/-/state/com.google/reading-list",
                1563874362,
                items,
                "1549658378126328"
            )

        assertEquals(expected, target)
    }
}
