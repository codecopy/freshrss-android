package fr.chenry.android.freshrss.components.feeds

import org.junit.Assert.assertEquals
import org.junit.Test

class FeedSectionTest {

    @Test
    fun fromNavigationButton() {
        FeedSection.values().forEach {
            assertEquals(
                "${it.name} was initialized with button id " +
                    "${it.navigationButtonId} but SubscriptionSection#fromNavigationButton " +
                    "maps ${it.navigationButtonId} to another section",
                FeedSection.fromNavigationButton(it.navigationButtonId),
                it
            )
        }
    }
}
