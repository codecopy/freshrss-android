package fr.chenry.android.freshrss.components.navigationdrawer

import org.junit.Assert.assertEquals
import org.junit.Test

class AddFeedDialogTest {

    @Test
    fun tryProcessYoutubeUrlTest() {
        val expectations =
            listOf(
                // Youtube URLs
                "https://www.youtube.com/channel/UCNHbHR2KOc851Kkb_TJ2Orw/blah" to
                    "https://www.youtube.com/feeds/videos.xml?channel_id=UCNHbHR2KOc851Kkb_TJ2Orw",
                "https://www.youtube.com/channel/UCNHbHR2KOc851Kkb_TJ2Orw/videos" to
                    "https://www.youtube.com/feeds/videos.xml?channel_id=UCNHbHR2KOc851Kkb_TJ2Orw",
                "https://www.youtube.com/channel/UCNHbHR2KOc851Kkb_TJ2Orw/" to
                    "https://www.youtube.com/feeds/videos.xml?channel_id=UCNHbHR2KOc851Kkb_TJ2Orw",
                "https://www.youtube.com/channel/UCNHbHR2KOc851Kkb_TJ2Orw" to
                    "https://www.youtube.com/feeds/videos.xml?channel_id=UCNHbHR2KOc851Kkb_TJ2Orw",
                "https://www.youtube.com/user/CosmicSoundwaves/videos" to
                    "https://www.youtube.com/feeds/videos.xml?user=CosmicSoundwaves",
                "https://www.youtube.com/user/CosmicSoundwaves/" to
                    "https://www.youtube.com/feeds/videos.xml?user=CosmicSoundwaves",
                "https://www.youtube.com/user/CosmicSoundwaves" to
                    "https://www.youtube.com/feeds/videos.xml?user=CosmicSoundwaves",
                "https://www.youtube.com/watch?v=2w7hAuA9dq8&list=PLygWqO36YZG039tHkjo0v4O-1vl7l74or" to
                    "https://www.youtube.com/feeds/videos.xml?playlist_id=PLygWqO36YZG039tHkjo0v4O-1vl7l74or",
                // invidio.us URLs
                "https://invidio.us/channel/UCNHbHR2KOc851Kkb_TJ2Orw/blah" to
                    "https://www.youtube.com/feeds/videos.xml?channel_id=UCNHbHR2KOc851Kkb_TJ2Orw",
                "https://invidio.us/channel/UCNHbHR2KOc851Kkb_TJ2Orw/videos" to
                    "https://www.youtube.com/feeds/videos.xml?channel_id=UCNHbHR2KOc851Kkb_TJ2Orw",
                "https://invidio.us/channel/UCNHbHR2KOc851Kkb_TJ2Orw/" to
                    "https://www.youtube.com/feeds/videos.xml?channel_id=UCNHbHR2KOc851Kkb_TJ2Orw",
                "https://invidio.us/channel/UCNHbHR2KOc851Kkb_TJ2Orw" to
                    "https://www.youtube.com/feeds/videos.xml?channel_id=UCNHbHR2KOc851Kkb_TJ2Orw",
                "https://invidio.us/user/CosmicSoundwaves/videos" to
                    "https://www.youtube.com/feeds/videos.xml?user=CosmicSoundwaves",
                "https://invidio.us/user/CosmicSoundwaves/" to
                    "https://www.youtube.com/feeds/videos.xml?user=CosmicSoundwaves",
                "https://invidio.us/user/CosmicSoundwaves" to
                    "https://www.youtube.com/feeds/videos.xml?user=CosmicSoundwaves",
                "https://invidio.us/watch?v=2w7hAuA9dq8&list=PLygWqO36YZG039tHkjo0v4O-1vl7l74or" to
                    "https://www.youtube.com/feeds/videos.xml?playlist_id=PLygWqO36YZG039tHkjo0v4O-1vl7l74or",
                // Something else
                "https://www.dadall.info/blog/feed.php?rss" to
                    "https://www.dadall.info/blog/feed.php?rss"
            )

        for ((target, expected) in expectations) {
            assertEquals(expected, AddSubscriptionDialog.tryProcessYoutubeUrl(target))
        }
    }
}
