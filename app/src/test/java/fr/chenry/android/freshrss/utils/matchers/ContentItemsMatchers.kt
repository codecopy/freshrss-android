package fr.chenry.android.freshrss.utils.matchers

import fr.chenry.android.freshrss.store.api.models.ContentItem
import fr.chenry.android.freshrss.store.api.models.ContentItemOrigin
import fr.chenry.android.freshrss.store.api.models.ContentItemsHandler
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.Matchers.containsInAnyOrder
import org.hamcrest.TypeSafeMatcher
import org.hamcrest.text.IsBlankString.blankOrNullString
import org.joda.time.DateTimeZone
import org.joda.time.LocalDateTime

class ContentItemsHandlerMatcher
private constructor(
    private val id: String,
    private val updated: Long,
    private val items: List<Matcher<ContentItem>>,
    private val continuation: String = ""
) : TypeSafeMatcher<ContentItemsHandler>() {

    override fun describeTo(description: Description) {
        description.appendText(
            "matches ContentItemsHandlerMatcher(id=$id, updated=$updated, " +
                "continuation=$continuation)"
        )
    }

    override fun matchesSafely(item: ContentItemsHandler?): Boolean =
        item?.id == id &&
            item.updated == updated &&
            item.continuation == continuation &&
            containsInAnyOrder(items).matches(item.items)

    companion object {
        fun contentItemsHandlerOf(
            id: String,
            updated: Long,
            items: List<Matcher<ContentItem>>,
            continuation: String = ""
        ) = ContentItemsHandlerMatcher(id, updated, items, continuation)
    }
}

class ContentItemMatcher
private constructor(
    private val id: String,
    private val crawled: String,
    private val timestamp: String,
    private val published: String,
    private val title: String,
    private val href: String,
    private val categories: List<String>,
    private val origin: ContentItemOrigin,
    private val author: String = ""
) : TypeSafeMatcher<ContentItem>() {

    override fun describeTo(description: Description) {
        description.appendText("matches ContentItem(id=$id, title=$title)")
    }

    override fun matchesSafely(item: ContentItem?): Boolean =
        id == item?.id &&
            LocalDateTime(crawled, DateTimeZone.UTC) == item.crawled &&
            LocalDateTime(timestamp, DateTimeZone.UTC) == item.timestamp &&
            LocalDateTime(published, DateTimeZone.UTC) == item.published &&
            title == item.title &&
            !blankOrNullString().matches(item.summary) &&
            href == item.alternate.firstOrNull()?.href &&
            containsInAnyOrder(*categories.toTypedArray()).matches(item.categories) &&
            origin.streamId == item.origin.streamId &&
            origin.title == item.origin.title &&
            author == item.author

    companion object {
        fun contentItemMatcherOf(
            id: String,
            crawled: String,
            timestamp: String,
            published: String,
            title: String,
            href: String,
            categories: List<String>,
            origin: ContentItemOrigin,
            author: String = ""
        ) =
            ContentItemMatcher(
                id,
                crawled,
                timestamp,
                published,
                title,
                href,
                categories,
                origin,
                author
            )
    }
}
