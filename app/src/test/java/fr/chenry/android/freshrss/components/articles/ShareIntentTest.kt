package fr.chenry.android.freshrss.components.articles

import fr.chenry.android.freshrss.components.articles.webviewutils.ShareIntent
import fr.chenry.android.freshrss.components.articles.webviewutils.chunkfilters.RemoveFeedTitleFilter
import fr.chenry.android.freshrss.components.articles.webviewutils.chunkfilters.SentenceCapFilter
import org.junit.Assert.assertEquals
import org.junit.Test

class ShareIntentTest {
    private val attributes
        get() =
            mapOf(
                "subscription" to "The subscription",
                "author" to "Humble Me",
                "title" to "My Title - My Series #1 | The Subscription - THIS IS AWESOME!",
                "content" to "Lorem ipsum",
                "href" to "http://example.com"
            )

    @Test
    fun stripAuthorTest() {
        assertEquals("My title", RemoveFeedTitleFilter.stripFragment("My title - Author", "Author"))
        assertEquals("My title", RemoveFeedTitleFilter.stripFragment("Author | My title", "Author"))
        assertEquals("My title", RemoveFeedTitleFilter.stripFragment("Author | My title", "author"))
        assertEquals("My title", RemoveFeedTitleFilter.stripFragment("author & My title", "Author"))
        assertEquals(
            "Video series #1 | Title",
            RemoveFeedTitleFilter.stripFragment("Video series #1 | Author | Title", "Author")
        )
    }

    @Test
    fun sentenceCapTest() {
        val origin =
            "Comment la Glorieuse Révolution a-t-elle favorisé l'essor économique de l'Angleterre ?"
        val expected =
            "Comment la glorieuse révolution a-t-elle favorisé l'essor économique de l'angleterre ?"
        val result = SentenceCapFilter.sentenceCap(origin)
        assertEquals(expected, result)
    }

    @Test
    fun format() {
        val expected =
            """
                |My title - My series #1 - This is awesome! — The Subscription

                |http://example.com
            """.trimMargin(
                "|"
            )
        assertEquals(expected, ShareIntent.format(attributes))
    }
}
