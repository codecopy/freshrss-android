package fr.chenry.android.freshrss.store.api

import com.github.javafaker.Faker
import fr.chenry.android.freshrss.BaseTest
import fr.chenry.android.freshrss.Mocks
import fr.chenry.android.freshrss.store.api.models.ContentItemOrigin
import fr.chenry.android.freshrss.store.api.models.FeedTagItem
import fr.chenry.android.freshrss.store.api.models.SubscriptionApiItem
import fr.chenry.android.freshrss.store.api.models.UnreadCount
import fr.chenry.android.freshrss.store.api.models.UnreadCountsHandler
import fr.chenry.android.freshrss.store.database.models.Account
import fr.chenry.android.freshrss.store.database.models.ReadStatus
import fr.chenry.android.freshrss.store.database.models.VoidAccount
import fr.chenry.android.freshrss.utils.matchers.ContentItemMatcher.Companion.contentItemMatcherOf
import fr.chenry.android.freshrss.utils.matchers.ContentItemsHandlerMatcher.Companion.contentItemsHandlerOf
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import okhttp3.HttpUrl.Companion.toHttpUrl
import okhttp3.mockwebserver.Dispatcher
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import okhttp3.mockwebserver.RecordedRequest
import org.hamcrest.MatcherAssert.assertThat
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import retrofit2.await
import java.nio.charset.Charset
import java.util.UUID

class APITest : BaseTest() {
    var server = MockWebServer()
    var account = VoidAccount
    var api = API.get(account)
    val writeToken = UUID.randomUUID().toString()
    val feedId: String = Faker.instance().idNumber().valid()
    private val dispatcher = APITestDispatcher()

    @Before
    fun setUp() {
        Mocks.mockLog()

        server = MockWebServer().apply {
            start(
                System.getenv().getOrDefault("MOCK_WEB_SERVER_PORT", "8888").toIntOrNull() ?: 8888
            )
            dispatcher = this@APITest.dispatcher
        }
        account = Account("SID", "Auth", "Karl Marx", server.url("/").toString())
        api = API.get(account)
    }

    @After
    fun tearDown() {
        server.close()
    }

    @Test
    fun getWriteToken() {
        val actual = runBlocking { withContext(Dispatchers.IO) { api.getWriteToken().await() } }

        assertEquals(actual, writeToken)
    }

    @Test
    fun getSubscriptions() {
        val actual = runBlocking { withContext(Dispatchers.IO) { api.getSubscriptions().await() } }
        val expected =
            listOf(
                SubscriptionApiItem(
                    "feed/1",
                    "Cédric Champeau",
                    listOf("user/-/label/Dev"),
                    "http://melix.github.io/blog/feed.xml",
                    "http://melix.github.io/blog/",
                    "https://flus.io/f.php?d7993d65"
                ),
                SubscriptionApiItem(
                    "feed/2",
                    "CommitStrip",
                    listOf("user/-/label/Dev"),
                    "http://www.commitstrip.com/fr/feed/",
                    "http://www.commitstrip.com",
                    "https://flus.io/f.php?97c8e4e9"
                ),
                SubscriptionApiItem(
                    "feed/3",
                    "e-penser",
                    listOf("user/-/label/Videos"),
                    "https://www.youtube.com/feeds/videos.xml?channel_id=UCcziTK2NKeWtWQ6kB5tmQ8Q",
                    "https://www.youtube.com/channel/UCcziTK2NKeWtWQ6kB5tmQ8Q",
                    "https://flus.io/f.php?db38b200"
                ),
                SubscriptionApiItem(
                    "feed/4",
                    "https://www.jesuisundev.com/",
                    listOf("user/-/label/Dev"),
                    "https://www.jesuisundev.com/feed/",
                    "https://www.jesuisundev.com/",
                    "https://flus.io/f.php?25792eb5"
                ),
                SubscriptionApiItem(
                    "feed/5",
                    "Kotlin Blog",
                    listOf("user/-/label/Dev"),
                    "https://blog.jetbrains.com/kotlin/feed/",
                    "https://blog.jetbrains.com/kotlin",
                    "https://flus.io/f.php?e4048cca"
                )
            )

        assertEquals(expected, actual)
    }

    @Test
    fun getUnreadCount() {
        val actual = runBlocking { withContext(Dispatchers.IO) { api.getUnreadCount().await() } }

        val expected =
            UnreadCountsHandler(
                10,
                listOf(
                    UnreadCount("feed/1", 7),
                    UnreadCount("feed/2", 3),
                    UnreadCount("feed/3", 0),
                    UnreadCount("feed/4", 0),
                    UnreadCount("feed/5", 0)
                )
            )

        assertEquals(expected, actual)
    }

    @Test
    fun getStreamContents() {
        val actual = runBlocking {
            withContext(Dispatchers.IO) { api.getStreamContents(feedId, "0").await() }
        }

        assertThat(
            actual,
            contentItemsHandlerOf(
                id = "user/-/state/com.google/reading-list",
                updated = 1582649613,
                continuation = "",
                items =
                listOf(
                    contentItemMatcherOf(
                        id = "tag:google.com,2005:reader/item/00058167f526c940",
                        crawled = "2019-02-08T20:39:38.127",
                        timestamp = "2019-02-08T20:39:38.127",
                        published = "2019-04-10T16:45:01.000",
                        title = "Gradle myth busting: lifecycle",
                        href = "https://melix.github.io/blog/2018/09/gradle-lifecycle.html",
                        categories =
                        listOf(
                            "user/-/state/com.google/reading-list",
                            "user/-/label/Misc",
                            "user/-/state/com.google/read"
                        ),
                        origin =
                        ContentItemOrigin(streamId = "feed/6", title = "Cédric Champeau"),
                        author = ""
                    ),
                    contentItemMatcherOf(
                        id = "tag:google.com,2005:reader/item/00058167f526c41c",
                        crawled = "2019-02-08T20:39:38.126",
                        timestamp = "2019-02-08T20:39:38.126",
                        published = "2016-05-21T22:00:00.000",
                        title = "Gradle and Kotlin, a personal perspective",
                        href = "http://melix.github.io/blog/2016/05/gradle-kotlin.html",
                        categories =
                        listOf(
                            "user/-/state/com.google/reading-list",
                            "user/-/label/Misc",
                            "user/-/state/com.google/read"
                        ),
                        origin =
                        ContentItemOrigin(streamId = "feed/6", title = "Cédric Champeau"),
                        author = ""
                    )
                )
            )
        )
    }

    @Test
    fun getTags() {
        val actual = runBlocking { withContext(Dispatchers.IO) { api.getTags().await() } }

        val expected =
            listOf(
                FeedTagItem(id = "user/-/label/Misc", type = "folder", unreadCount = 0),
                FeedTagItem(id = "user/-/label/Vids", type = "folder", unreadCount = 0),
                FeedTagItem(id = "user/-/label/#curiosité", type = "tag", unreadCount = 0),
                FeedTagItem(id = "user/-/label/#vulgarisation", type = "tag", unreadCount = 0)
            )
        assertEquals(expected, actual)
    }

    @Test
    fun getUnreadItemIds() {
        val actual = runBlocking {
            withContext(Dispatchers.IO) { api.getItemIds(mapOf("xt" to READ_ITEMS_ID)).await() }
        }

        assertEquals(
            actual,
            listOf(
                "tag:google.com,2005:reader/item/00059fbeeb6c9fb8",
                "tag:google.com,2005:reader/item/00059fbab96d7e0c",
                "tag:google.com,2005:reader/item/00059fb9aef6b79d",
                "tag:google.com,2005:reader/item/00059fb4704a906e"
            )
        )

        runBlocking {
            withContext(Dispatchers.IO) { api.getItemIds(mapOf("xt" to READ_ITEMS_ID)).await() }
        }
    }

    @Test
    fun postReadStatus() {
        runBlocking {
            withContext(Dispatchers.IO) {
                api.postEditTag(ReadStatus.READ.toPostFormData("reader/item/00058167f526c41c"))
                    .await()
            }
        }

        val request1 = server.takeRequest() // Request for refreshing token
        val request2 = server.takeRequest() // Request for posting read status

        val httpUrl2 =
            "http://localhost/?${request2.body.readString(Charset.forName("UTF-8"))}".toHttpUrl()

        assertEquals(2, server.requestCount)
        assertEquals("/reader/api/0/token", request1.path)
        assertEquals(httpUrl2.queryParameterNames, setOf("a", "i"))
        assertEquals("user/-/state/com.google/read", httpUrl2.queryParameter("a"))
        assertEquals("reader/item/00058167f526c41c", httpUrl2.queryParameter("i"))

        runBlocking {
            withContext(Dispatchers.IO) {
                api.postEditTag(ReadStatus.UNREAD.toPostFormData("reader/item/00058167f526c41c"))
                    .await()
            }
        }

        val request3 = server.takeRequest()

        val httpUrl3 =
            "http://localhost?${request3.body.readString(Charset.forName("UTF-8"))}".toHttpUrl()

        assertEquals(3, server.requestCount) // Token won't be refreshed
        assertEquals(httpUrl3.queryParameterNames, setOf("r", "i"))
        assertEquals("user/-/state/com.google/read", httpUrl3.queryParameter("r"))
    }

    @Test
    fun postAddSubscription() {
        val response = runBlocking {
            withContext(Dispatchers.IO) {
                api.postAddSubscription("feed/http://melix.github.io/blog/feed.xml").await()
            }
        }

        val request1 = server.takeRequest() // Request for refreshing token
        val request2 = server.takeRequest() // Request for posting read status

        assertEquals(2, server.requestCount)
        assertEquals("/reader/api/0/token", request1.path)
        assertEquals(
            "/reader/api/0/subscription/edit?s=feed%2Fhttp%3A%2F%2Fmelix.github.io%2Fblog%2Ffeed.xml&ac=subscribe",
            request2.path
        )
        assertEquals("OK", response)

        runBlocking {
            withContext(Dispatchers.IO) {
                api.postAddSubscription("feed/http://melix.github.io/blog/feed.xml").await()
            }
        }

        assertEquals(3, server.requestCount) // Token won't be refreshed
        assertEquals(
            "/reader/api/0/subscription/edit?s=feed%2Fhttp%3A%2F%2Fmelix.github.io%2Fblog%2Ffeed.xml&ac=subscribe",
            server.takeRequest().path
        )

        runBlocking {
            withContext(Dispatchers.IO) {
                api.postAddSubscription("feed/http://melix.github.io/blog/feed.xml", "Vids").await()
            }
        }

        assertEquals(4, server.requestCount) // Token won't be refreshed
        assertEquals(
            "/reader/api/0/subscription/edit?s=feed%2Fhttp%3A%2F%2Fmelix.github.io%2Fblog%2Ffeed.xml&a=Vids&ac=subscribe", // ktlint-disable  max-line-length
            server.takeRequest().path
        )
    }

    @Suppress("TestFunctionName")
    inner class APITestDispatcher : Dispatcher() {
        override fun dispatch(request: RecordedRequest): MockResponse {
            // getWriteToken()
            if (POST(request, "/reader/api/0/token") && hasAuth(request))
                return MockResponse().setBody(writeToken)
            if (GET(request, "/reader/api/0/subscription/list") &&
                hasAuth(request) &&
                hasJsonOutput(request)
            )
                return MockResponse().setBody(getFixture("APIFixtures/subscriptions.json"))
            if (GET(request, "/reader/api/0/unread-count") &&
                hasAuth(request) &&
                hasJsonOutput(request)
            )
                return MockResponse().setBody(getFixture("APIFixtures/unread-count.json"))
            if (GET(request, "/reader/api/0/stream/contents/$feedId") &&
                hasAuth(request) &&
                hasJsonOutput(request)
            )
                return MockResponse().setBody(getFixture("APIFixtures/stream-contents.json"))
            if (GET(request, "/reader/api/0/tag/list") && hasAuth(request) && hasJsonOutput(request)
            )
                return MockResponse().setBody(getFixture("APIFixtures/tags.json"))
            if (GET(request, "/reader/api/0/stream/items/ids") &&
                hasAuth(request) &&
                hasJsonOutput(request) &&
                hasParams(
                        request,
                        "xt" to "user/-/state/com.google/read",
                        "n" to "${1_000_000}",
                        "s" to ALL_ITEMS_ID
                    )
            )
                return MockResponse().setBody(getFixture("APIFixtures/unread-item-ids.json"))
            if (POST(request, "/reader/api/0/edit-tag") && hasToken(request) && hasAuth(request))
                return MockResponse().setResponseCode(200)
            if (POST(request, "/reader/api/0/subscription/edit") &&
                hasToken(request) &&
                hasAuth(request)
            ) return MockResponse().setBody("OK")

            return MockResponse().setResponseCode(404)
        }

        private fun hasAuth(request: RecordedRequest) =
            request.headers["Authorization"] == "GoogleLogin auth=${account.SID}"

        private fun hasJsonOutput(request: RecordedRequest) =
            request.requestUrl?.queryParameter("output") == "json"

        private fun hasParams(
            request: RecordedRequest,
            vararg params: Pair<String, String>
        ): Boolean =
            request.requestUrl?.let { self ->
                params.map { self.queryParameter(it.first) == it.second }.reduce { acc, unit ->
                    acc && unit
                }
            }
                ?: false

        private fun hasToken(request: RecordedRequest) = request.getHeader("T") == writeToken

        private fun GET(request: RecordedRequest, path: String) =
            pathMatches(request, path) && request.method == "GET"

        private fun POST(request: RecordedRequest, path: String) =
            pathMatches(request, path) && request.method == "POST"

        private fun pathMatches(request: RecordedRequest, path: String) =
            "http://localhost$path".toHttpUrl().encodedPath == request.requestUrl?.encodedPath
    }
}
