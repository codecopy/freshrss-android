package fr.chenry.android.freshrss

open class BaseTest {
    protected fun getFixture(reference: String): String =
        javaClass.classLoader!!.getResource("fixtures/$reference").readText()
}
