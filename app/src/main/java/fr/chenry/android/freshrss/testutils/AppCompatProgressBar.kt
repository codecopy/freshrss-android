package fr.chenry.android.freshrss.testutils

import android.content.Context
import android.util.AttributeSet
import android.widget.ProgressBar
import fr.chenry.android.freshrss.R

/**
 * This solves the problem of Espresso hanging when the progress bar is animating
 * See src/main/java/fr/chenry/android/freshrss/testutils/AppCompatProgressBar.kt
 * Source: https://jasonfry.co.uk/blog/android-espresso-test-hangs-with-indeterminate-progressbar/
 */
class AppCompatProgressBar @JvmOverloads constructor(
    context: Context,
    attributeSet: AttributeSet? = null,
    defStyleAttr: Int = R.attr.progressBarStyle,
    defStyleRes: Int = 0
) : ProgressBar(context, attributeSet, defStyleAttr, defStyleRes)
