package fr.chenry.android.freshrss.utils

import android.annotation.SuppressLint
import android.content.Context
import android.content.res.TypedArray
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.ColorFilter
import android.graphics.Paint
import android.graphics.PixelFormat
import android.graphics.drawable.Drawable
import android.util.TypedValue
import android.view.View
import androidx.appcompat.app.ActionBar
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.NavController
import fr.chenry.android.freshrss.R
import kotlin.math.max
import kotlin.math.min
import kotlin.math.roundToInt

class BurgerMenuDrawerListener(
    private val supportActionBar: ActionBar,
    private val navigation: NavController
) : DrawerLayout.DrawerListener {

    private val drawable = HamburgerCrossDrawable(supportActionBar.themedContext)

    override fun onDrawerStateChanged(newState: Int) {
        if (navigation.isTopLevelDestination()) supportActionBar.setHomeAsUpIndicator(drawable)
    }

    override fun onDrawerSlide(drawerView: View, slideOffset: Float) {
        if (navigation.isTopLevelDestination()) setPosition(slideOffset)
    }

    override fun onDrawerClosed(drawerView: View) {
        if (navigation.isTopLevelDestination()) {
            supportActionBar.setHomeActionContentDescription(R.string.navigation_drawer_open)
            setPosition(0f)
        }
    }

    override fun onDrawerOpened(drawerView: View) {
        if (navigation.isTopLevelDestination()) {
            supportActionBar.setHomeActionContentDescription(R.string.navigation_drawer_close)
            setPosition(1f)
        }
    }

    private fun setPosition(position: Float) {
        drawable.progress = position
    }

    // Adapted from https://stackoverflow.com/questions/35716854/hamburger-menu-icon-to-cross-animation-in-android/56134124#56134124
    private class HamburgerCrossDrawable
    @SuppressLint("PrivateResource")
    constructor(context: Context) : Drawable() {

        private val paint = Paint()
        private val size: Int
        private val barLength: Float
        private val halfBarLengh: Float
        private val gapBetweenBars: Float
        private val crossHeight: Float

        private var color: Int
            get() = paint.color
            set(value) {
                if (paint.color != value) {
                    paint.color = value
                    invalidateSelf()
                }
            }

        private var thickness: Float
            get() = paint.strokeWidth
            set(value) {
                if (paint.strokeWidth != value) {
                    paint.strokeWidth = value
                    invalidateSelf()
                }
            }

        var progress: Float = 0.0f
            set(value) {
                field = value.coerceIn(0.0f, 1.0f)
                invalidateSelf()
            }

        init {
            paint.style = Paint.Style.STROKE
            paint.strokeJoin = Paint.Join.MITER
            paint.strokeCap = Paint.Cap.BUTT
            paint.isAntiAlias = true

            val attributes: TypedArray = context.theme.obtainStyledAttributes(
                null,
                androidx.appcompat.R.styleable.DrawerArrowToggle,
                R.attr.drawerArrowStyle, 0
            )

            size = attributes.getDimensionPixelSize(androidx.appcompat.R.styleable.DrawerArrowToggle_drawableSize, 0)
            thickness = attributes.getDimension(androidx.appcompat.R.styleable.DrawerArrowToggle_thickness, 0f)
            color = attributes.getColor(androidx.appcompat.R.styleable.DrawerArrowToggle_color, Color.WHITE)

            barLength = attributes
                .getDimension(androidx.appcompat.R.styleable.DrawerArrowToggle_barLength, 0f)
                .roundToInt()
                .toFloat()

            gapBetweenBars = attributes.getDimension(
                androidx.appcompat.R.styleable.DrawerArrowToggle_gapBetweenBars,
                dpToPx(context, 3f)
            )

            halfBarLengh = barLength / 2.0f
            crossHeight = dpToPx(context, 16f)

            attributes.recycle()
        }

        override fun draw(canvas: Canvas) =
            if (progress < 0.5) drawHamburger(canvas)
            else drawCross(canvas)

        private fun drawHamburger(canvas: Canvas) {
            val center = bounds.exactCenterY()
            val minLengh = min(barLength, crossHeight)
            val maxLengh = max(barLength, crossHeight)
            val diff = maxLengh - minLengh
            val lenghtFromCenter = (minLengh + (1f - (progress * 2)) * diff) / 2f

            val left = center - lenghtFromCenter
            val right = center + lenghtFromCenter

            canvas.drawLine(left, center, right, center, paint)

            val offsetY = (gapBetweenBars + thickness) * (2f * (progress - 0.5f))

            canvas.drawLine(left, center - offsetY, right, center - offsetY, paint)
            canvas.drawLine(left, center + offsetY, right, center + offsetY, paint)
        }

        private fun drawCross(canvas: Canvas) {
            val centerX = bounds.exactCenterX()
            val centerY = bounds.exactCenterY()
            val halfCrossHeight = crossHeight / 2

            // Calculate current cross position
            val distanceY = halfCrossHeight * (2 * (progress - 0.5f))
            val top = centerY - distanceY
            val bottom = centerY + distanceY
            val left = centerX - halfCrossHeight
            val right = centerX + halfCrossHeight

            // Draw cross
            canvas.drawLine(left, top, right, bottom, paint)
            canvas.drawLine(left, bottom, right, top, paint)
        }

        override fun setAlpha(alpha: Int) {
            if (alpha != paint.alpha) {
                paint.alpha = alpha
                invalidateSelf()
            }
        }

        override fun setColorFilter(colorFilter: ColorFilter?) {
            paint.colorFilter = colorFilter
            invalidateSelf()
        }

        override fun getIntrinsicWidth(): Int = size

        override fun getIntrinsicHeight(): Int = size

        override fun getOpacity(): Int = PixelFormat.TRANSLUCENT

        private fun dpToPx(context: Context, value: Float): Float =
            TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, value, context.resources.displayMetrics)
    }
}
