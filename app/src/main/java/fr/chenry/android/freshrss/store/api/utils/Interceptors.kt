package fr.chenry.android.freshrss.store.api.utils

import android.util.Log
import fr.chenry.android.freshrss.store.api.API
import fr.chenry.android.freshrss.store.api.API.Companion.TOKEN_RENEWAL_FAILED_MESSAGE
import fr.chenry.android.freshrss.store.database.models.Account
import fr.chenry.android.freshrss.utils.unit
import kotlinx.coroutines.runBlocking
import okhttp3.Interceptor
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Invocation
import retrofit2.Response
import retrofit2.awaitResponse
import okhttp3.Response as OkHttp3Response

internal class APIInterceptor(private val account: Account) : Interceptor {
    lateinit var service: API

    override fun intercept(chain: Interceptor.Chain): OkHttp3Response {
        val request = chain.request()
        val invocation = request.tag(Invocation::class.java)!!
        val method = invocation.method()
        val builder = request.newBuilder()
        var reqUrl = request.url

        if (method.isAnnotationPresent(TokenRequired::class.java)) {
            if (account.isWriteTokenExpired) {
                val response = runBlocking { service.getWriteToken().awaitResponse() }
                if (!response.isSuccessful) return tokenRenewalFailResponse(response)
                account.writeToken = response.body()!!.trim()
            }

            builder.header("T", account.writeToken)
        }

        if (method.isAnnotationPresent(JsonOutput::class.java))
            reqUrl = reqUrl.newBuilder().setQueryParameter("output", "json").build()

        if (method.isAnnotationPresent(AuthorisationRequired::class.java)) builder.apply {
            header("Authorization", "GoogleLogin auth=${account.SID}")
        }

        return builder.url(reqUrl).build().let { chain.proceed(it) }
    }

    private fun <T> tokenRenewalFailResponse(response: Response<T>) =
        response.raw().newBuilder().message(TOKEN_RENEWAL_FAILED_MESSAGE).build()
}

private object FLogger : HttpLoggingInterceptor.Logger {
    override fun log(message: String) = Log.d(API::class.qualifiedName!!, message).unit()
}

internal val loggingInterceptor = HttpLoggingInterceptor(FLogger).apply {
    redactHeader("Authorization")
    redactHeader("T")
    level = HttpLoggingInterceptor.Level.BODY
}
