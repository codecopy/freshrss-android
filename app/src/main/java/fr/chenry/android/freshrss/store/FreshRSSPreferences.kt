package fr.chenry.android.freshrss.store

import android.content.SharedPreferences
import android.content.SharedPreferences.OnSharedPreferenceChangeListener
import androidx.annotation.VisibleForTesting
import androidx.core.content.edit
import androidx.preference.PreferenceManager
import fr.chenry.android.freshrss.FreshRSSApplication
import fr.chenry.android.freshrss.R
import fr.chenry.android.freshrss.components.feeds.FeedSection

class FreshRSSPreferences(private val application: FreshRSSApplication) {
    @VisibleForTesting
    val sharedPreferences: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(application)

    private val refreshFrequencies = application.resources.getStringArray(R.array.refresh_frequencies)
    private val refreshFrequencyValues = application.resources.getStringArray(R.array.refresh_frequency_values)
    private val defaultRefreshFrequencyValue =
        refreshFrequencies.indexOf(getKey(R.string.refresh_frequency_30m)).let { refreshFrequencyValues[it] }

    private val subscriptionSectionKey = getKey(R.string.subscription_section_preference)
    val refreshFrequencyKey = getKey(R.string.refresh_frequency_preference)
    val retainScrollPositionKey = getKey(R.string.retain_scroll_position_preference)
    val debugModeKey = getKey(R.string.debug_mode_preference)

    var refreshFrequency: Int
        get() {
            val pref = sharedPreferences.getString(refreshFrequencyKey, defaultRefreshFrequencyValue)!!
            return if (pref in refreshFrequencyValues) pref.toInt() else defaultRefreshFrequencyValue.toInt()
        }
        set(value) {
            val valueStr = value.toString()
            if (valueStr !in refreshFrequencyValues) return
            sharedPreferences.edit { putString(refreshFrequencyKey, valueStr) }
        }

    var feedSection: FeedSection
        get() {
            val pref = sharedPreferences.getString(subscriptionSectionKey, FeedSection.ALL.name)!!
            return runCatching { FeedSection.valueOf(pref) }.getOrDefault(FeedSection.ALL)
        }
        set(value) = sharedPreferences.edit { putString(subscriptionSectionKey, value.name) }

    var retainScrollPosition: Boolean
        get() = sharedPreferences.getBoolean(retainScrollPositionKey, true)
        set(value) = sharedPreferences.edit { putBoolean(retainScrollPositionKey, value) }

    var debugMode: Boolean
        get() = sharedPreferences.getBoolean(debugModeKey, false)
        set(value) = sharedPreferences.edit { putBoolean(debugModeKey, value) }

    fun initDefaults() {
        PreferenceManager.setDefaultValues(application, R.xml.preference_screen, false)
    }

    fun registerChangeListener(listener: OnSharedPreferenceChangeListener) =
        sharedPreferences.registerOnSharedPreferenceChangeListener(listener)

    fun unregisterChangeListener(listener: OnSharedPreferenceChangeListener) =
        sharedPreferences.unregisterOnSharedPreferenceChangeListener(listener)

    @VisibleForTesting
    fun reset() {
        sharedPreferences.edit(commit = true) { clear() }
        initDefaults()
    }

    private fun getKey(id: Int): String = application.resources.getString(id)
}
