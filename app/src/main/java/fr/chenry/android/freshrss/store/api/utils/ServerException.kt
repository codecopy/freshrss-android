package fr.chenry.android.freshrss.store.api.utils

import okhttp3.Response

class ServerException(val response: Response) : Exception(getMessage(response)) {
    companion object {
        private fun getMessage(response: Response): String {
            val messageEnding =
                if (response.message.isBlank()) "no message sent from server"
                else "server message: ${response.message}"
            return "Exception met while sending request to ${response.request.url}: " +
                "http error code: ${response.code}, $messageEnding"
        }
    }
}

class NotConnectedException : Exception("There is not currently connected session")
class NoNetworkException : Exception("Internet is currently unreachable")
