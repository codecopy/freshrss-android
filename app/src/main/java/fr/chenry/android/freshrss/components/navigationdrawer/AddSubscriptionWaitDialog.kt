package fr.chenry.android.freshrss.components.navigationdrawer

import android.app.Dialog
import android.os.Bundle
import android.widget.LinearLayout
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.view.ContextThemeWrapper
import androidx.fragment.app.DialogFragment
import fr.chenry.android.freshrss.R

class AddSubscriptionWaitDialog : DialogFragment() {
    private val dialogView by lazy {
        requireActivity().layoutInflater.inflate(
            R.layout.fragment_add_feed_wait_dialog,
            LinearLayout(context),
            false
        )
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog =
        AlertDialog.Builder(ContextThemeWrapper(requireActivity(), R.style.AppTheme_Wrapped_Waiting_Dialog_Alert))
            .setView(dialogView)
            .setCancelable(false)
            .create()

    override fun onResume() {
        super.onResume()
        isCancelable = false
    }
}
