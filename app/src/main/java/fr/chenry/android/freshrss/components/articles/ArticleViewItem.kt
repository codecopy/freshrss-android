package fr.chenry.android.freshrss.components.articles

import android.content.Context
import android.view.View
import android.widget.Toast
import android.widget.Toast.LENGTH_SHORT
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LifecycleCoroutineScope
import androidx.recyclerview.widget.RecyclerView
import eu.davidea.flexibleadapter.FlexibleAdapter
import eu.davidea.flexibleadapter.items.AbstractFlexibleItem
import eu.davidea.flexibleadapter.items.IFlexible
import eu.davidea.viewholders.FlexibleViewHolder
import fr.chenry.android.freshrss.BR
import fr.chenry.android.freshrss.R
import fr.chenry.android.freshrss.components.articles.ArticleViewItem.ArticleViewHolder
import fr.chenry.android.freshrss.databinding.FragmentSubscriptionArticleBinding
import fr.chenry.android.freshrss.store.Store
import fr.chenry.android.freshrss.store.database.models.Article
import fr.chenry.android.freshrss.store.database.models.FavoriteStatus
import fr.chenry.android.freshrss.store.database.models.Feed
import fr.chenry.android.freshrss.utils.e
import fr.chenry.android.freshrss.utils.isConnectedToNetwork
import fr.chenry.android.freshrss.utils.onFailure
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

class ArticleViewItem(
    private val context: Context,
    private val lifecycleScope: LifecycleCoroutineScope,
    private val feed: Feed,
    val article: Article
) : AbstractFlexibleItem<ArticleViewHolder>(), KoinComponent {
    val store by inject<Store>()

    init {
        isDraggable = false
        isSelectable = false
        isSwipeable = true
    }

    override fun bindViewHolder(
        adapter: FlexibleAdapter<IFlexible<RecyclerView.ViewHolder>>,
        holder: ArticleViewHolder,
        position: Int,
        payloads: MutableList<Any>
    ) = holder.bind(article)

    override fun createViewHolder(
        view: View,
        adapter: FlexibleAdapter<IFlexible<RecyclerView.ViewHolder>>
    ): ArticleViewHolder = ArticleViewHolder(DataBindingUtil.bind(view)!!, adapter)

    override fun getLayoutRes(): Int = R.layout.fragment_subscription_article

    override fun equals(other: Any?): Boolean {
        if (other !is ArticleViewItem) return false
        return article == other.article
    }

    override fun hashCode(): Int = article.hashCode()

    inner class ArticleViewHolder(
        private val binding: FragmentSubscriptionArticleBinding,
        flexibleAdapter: FlexibleAdapter<*>
    ) : FlexibleViewHolder(binding.root, flexibleAdapter) {

        private val frontView = binding.root.findViewById<View>(R.id.fragment_subscription_base_layout)
        private val rearLeftView = binding.root.findViewById<View>(R.id.fragment_subscription_article_rear_left_view)

        fun bind(article: Article) {
            val listener = FavoriteClickListener(this@ArticleViewItem.context, lifecycleScope, store, article)
            binding.setVariable(BR.article, article)
            binding.setVariable(BR.subscription, feed)
            binding.setVariable(BR.onFavoriteClickListener, listener)
            binding.executePendingBindings()
        }

        override fun getFrontView(): View = frontView
        override fun getRearLeftView(): View = rearLeftView
        override fun getActivationElevation(): Float = 1f
    }

    class FavoriteClickListener(
        private val context: Context,
        private val lifecycleScope: LifecycleCoroutineScope,
        private val store: Store,
        private val article: Article
    ) : View.OnClickListener {
        override fun onClick(v: View) {
            if (!context.isConnectedToNetwork()) {
                Toast.makeText(context, R.string.no_internet_connection_avaible, Toast.LENGTH_LONG).show()
                return
            }

            if (article.favoriteRequestOnGoing) {
                Toast.makeText(context, R.string.request_already_ongoing, LENGTH_SHORT).show()
                return
            }

            lifecycleScope.launch {
                article.favoriteRequestOnGoing = true
                val expected = article.favorite.toggle()
                store.postFavoriteStatus(article, expected).onFailure { err ->
                    this.e(err)
                    val message = when (expected) {
                        FavoriteStatus.FAVORITE -> context.getString(R.string.add_favorite_status, article.title)
                        FavoriteStatus.NOT_FAVORITE -> context.getString(
                            R.string.remove_favorit_status,
                            article.title
                        )
                    }

                    val toastText = context.getString(R.string.unable_to, message)

                    withContext(Dispatchers.Main) {
                        Toast.makeText(context, toastText, LENGTH_SHORT).show()
                    }
                }

                article.favoriteRequestOnGoing = false
            }
        }
    }
}
