package fr.chenry.android.freshrss.store.database.models

import androidx.room.Dao
import androidx.room.Entity
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Transaction

typealias FeedToTagRelations = List<FeedToTagRelation>

@Entity(
    primaryKeys = ["feedId", "tagId"],
    tableName = "feed_to_tag_relation"
)
data class FeedToTagRelation(
    val feedId: String,
    val tagId: String
)

@Dao
interface FeedToTagRelationDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(tags: FeedToTagRelations)

    @Query("DELETE FROM feed_to_tag_relation WHERE feedId = :feedId AND tagId NOT IN (:tagIds)")
    suspend fun deleteAll(feedId: String, tagIds: List<String>)

    @Transaction
    suspend fun syncForFeed(feedId: String, tagIds: List<String>) {
        insertAll(tagIds.map { FeedToTagRelation(feedId, it) })
        deleteAll(feedId, tagIds)
    }
}
