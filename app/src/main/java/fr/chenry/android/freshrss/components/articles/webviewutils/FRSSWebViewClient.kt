package fr.chenry.android.freshrss.components.articles.webviewutils

import android.content.Intent
import android.net.MailTo
import android.net.Uri
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.fragment.app.Fragment

class FRSSWebViewClient(private val fragment: Fragment, private val sourceUrl: Uri?) : WebViewClient() {
    override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
        if (request?.url?.scheme?.startsWith("mailto") == true) {
            val mt = MailTo.parse(request.url.toString())
            fragment.startActivity(MailIntent(mt.to, mt.subject, mt.body, mt.cc))

            return true
        } else if (request?.url?.scheme?.startsWith("http") == true) {
            if (request.url.isRelative && sourceUrl == null) return true

            val url = if (request.url.isRelative)
                sourceUrl!!.buildUpon().path(request.url.path ?: "").build() else request.url

            fragment.startActivity(Intent(Intent.ACTION_VIEW).apply { data = url })

            return true
        }

        return false
    }
}
