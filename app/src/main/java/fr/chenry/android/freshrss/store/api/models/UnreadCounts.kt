package fr.chenry.android.freshrss.store.api.models

data class UnreadCountsHandler(val max: Int, val unreadcounts: List<UnreadCount>)

data class UnreadCount(
    val id: String,
    val count: Int
)
