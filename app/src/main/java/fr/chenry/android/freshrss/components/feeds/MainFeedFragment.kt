package fr.chenry.android.freshrss.components.feeds

import android.content.SharedPreferences
import android.content.SharedPreferences.OnSharedPreferenceChangeListener
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.databinding.ObservableBoolean
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.map
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.navigation.NavigationBarView
import fr.chenry.android.freshrss.R
import fr.chenry.android.freshrss.databinding.FragmentMainFeedBinding
import fr.chenry.android.freshrss.store.database.models.VoidCategory
import fr.chenry.android.freshrss.store.viewmodels.AccountVM
import fr.chenry.android.freshrss.store.viewmodels.FeedSectionVM
import fr.chenry.android.freshrss.utils.requireF

class MainFeedFragment : Fragment(), OnSharedPreferenceChangeListener {
    private val viewModel by activityViewModels<AccountVM>()
    private val feedSectionVM by activityViewModels<FeedSectionVM>()
    private val autoFetchDisabled = ObservableBoolean(true)

    private lateinit var binding: FragmentMainFeedBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentMainFeedBinding.inflate(inflater, container, false)
        requireF().preferences.registerChangeListener(this)

        binding.apply {
            val prefs = requireF().preferences
            val feedSection = prefs.feedSection

            lifecycleOwner = this@MainFeedFragment.viewLifecycleOwner
            lastFetchText = viewModel.lastFetchText.map { it(requireContext()) }
            autoFetchDisabled = this@MainFeedFragment.autoFetchDisabled.apply {
                set(prefs.refreshFrequency == 0)
            }

            feedSectionVM.section.value = feedSection

            feedViewPager.adapter = MainSubscriptionPagerAdapter()
            feedViewPager.offscreenPageLimit = FeedSection.values().size
            feedViewPager.setCurrentItem(feedSection.ordinal, false)
            feedViewPager.registerOnPageChangeCallback(PageChangeCallback())

            feedBottomNavigation.selectedItemId = feedSection.navigationButtonId
            feedBottomNavigation.setOnItemSelectedListener(BottomNavigationChangeCallback())

            viewModel.account.observe(viewLifecycleOwner) {
                if (it.unreadCount == 0) feedBottomNavigation.removeBadge(
                    R.id.subscriptions_bottom_navigation_unread
                )
                else feedBottomNavigation.getOrCreateBadge(
                    R.id.subscriptions_bottom_navigation_unread
                ).number = viewModel.unreadCount
            }
        }

        return binding.root
    }

    override fun onPause() {
        super.onPause()
        requireF().preferences.feedSection = feedSectionVM.section.value!!
    }

    override fun onDestroy() {
        super.onDestroy()
        requireF().preferences.unregisterChangeListener(this)
    }

    override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences, key: String?) {
        if (key == requireF().preferences.refreshFrequencyKey) autoFetchDisabled.set(
            requireF().preferences.refreshFrequency == 0
        )
    }

    inner class MainSubscriptionPagerAdapter : FragmentStateAdapter(this) {
        override fun getItemCount(): Int = FeedSection.values().size

        override fun createFragment(position: Int): Fragment = childFragmentManager.fragmentFactory.instantiate(
            requireContext().classLoader, FeedFragment::class.java.name
        ).apply {
            arguments = MainFeedFragmentDirections.mainFeedsToArticlesCategory(
                VoidCategory, FeedSection.values().getOrElse(position) { FeedSection.ALL }
            ).arguments
        }
    }

    inner class PageChangeCallback : ViewPager2.OnPageChangeCallback() {
        override fun onPageSelected(position: Int) = FeedSection.byPosition(position).let {
            if (binding.feedBottomNavigation.selectedItemId != it.navigationButtonId) {
                feedSectionVM.section.postValue(it)
                binding.feedBottomNavigation.selectedItemId = it.navigationButtonId
            }
        }
    }

    inner class BottomNavigationChangeCallback : NavigationBarView.OnItemSelectedListener {
        override fun onNavigationItemSelected(item: MenuItem): Boolean {
            val next = FeedSection.fromNavigationButton(item.itemId).ordinal
            if (binding.feedViewPager.currentItem != next) binding.feedViewPager.currentItem = next
            return true
        }
    }
}
