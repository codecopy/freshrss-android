package fr.chenry.android.freshrss.store.database.models

import android.graphics.Bitmap

interface FeedLikeDisplayable {
    val id: String
    val title: String
    val unreadCount: Int
    val imageBitmap: Bitmap?
}
