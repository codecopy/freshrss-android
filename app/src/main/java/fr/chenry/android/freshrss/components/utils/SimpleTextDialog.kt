package fr.chenry.android.freshrss.components.utils

import android.app.Dialog
import android.content.res.ColorStateList
import android.os.Bundle
import android.text.method.LinkMovementMethod
import android.text.util.Linkify.EMAIL_ADDRESSES
import android.text.util.Linkify.PHONE_NUMBERS
import android.text.util.Linkify.WEB_URLS
import android.view.View
import android.widget.LinearLayout
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.core.widget.ImageViewCompat
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import fr.chenry.android.freshrss.R
import fr.chenry.android.freshrss.databinding.SimpleTextDialogBinding

class SimpleTextDialog(private val builder: SimpleTextDialogBuilder) : DialogFragment() {
    private val onOk inline get() = builder.onOk
    private val onCancel inline get() = builder.onCancel

    private val binding by lazy {
        SimpleTextDialogBinding.inflate(requireActivity().layoutInflater, LinearLayout(context), false)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog = requireActivity().let {
        binding.simpleTextDialogContent.text = builder.text

        if (builder.textContainsLinks) {
            binding.simpleTextDialogContent.movementMethod = LinkMovementMethod.getInstance()
            binding.simpleTextDialogContent.setTextIsSelectable(true)
            binding.simpleTextDialogContent.autoLinkMask = WEB_URLS or EMAIL_ADDRESSES or PHONE_NUMBERS
        }

        binding.simpleTextDialogTitle.text = builder.title
        if (builder.title.isNotEmpty()) {
            binding.simpleTextDialogTitleBlock.visibility = View.VISIBLE
            if (builder.icon >= 0) {
                binding.simpleTextDialogIcon.visibility = View.VISIBLE
                binding.simpleTextDialogIcon.setImageResource(builder.icon)
                if (builder.iconTint >= 0) ImageViewCompat.setImageTintList(
                    binding.simpleTextDialogIcon,
                    ColorStateList.valueOf(ContextCompat.getColor(requireContext(), builder.iconTint))
                )
            }
        }

        val dialog = AlertDialog
            .Builder(it)
            .setView(binding.root)
            .setPositiveButton(builder.okTextId) { _, _ -> onOk() }

        if (onCancel != null) dialog.setNegativeButton(builder.cancelTextId) { _, _ -> onCancel!!(this) }

        dialog.create()
    }

    @Suppress("MemberVisibilityCanBePrivate")
    class SimpleTextDialogBuilder {
        var text: CharSequence = ""
        var textContainsLinks = false
        var title: CharSequence = ""

        @setparam:DrawableRes
        var icon: Int = -1

        @setparam:ColorRes
        var iconTint: Int = -1
        var isCancelable: Boolean = true
        var okTextId: Int = android.R.string.ok
        var onOk: SimpleTextDialog.() -> Unit = { dismiss() }
        var cancelTextId: Int = android.R.string.cancel
        var onCancel: (SimpleTextDialog.() -> Unit)? = null

        fun onOk(cb: SimpleTextDialog.() -> Unit) {
            onOk = cb
        }

        fun onOk(@StringRes textId: Int, cb: SimpleTextDialog.() -> Unit) {
            onOk = cb
            okTextId = textId
        }

        fun onCancel(cb: SimpleTextDialog.() -> Unit) {
            onCancel = cb
        }

        fun onCancel(@StringRes textId: Int, cb: SimpleTextDialog.() -> Unit) {
            onCancel = cb
            cancelTextId = textId
        }

        fun build() = SimpleTextDialog(this).apply { isCancelable = this@SimpleTextDialogBuilder.isCancelable }
    }
}

inline fun FragmentActivity.simpleTextDialog(
    crossinline init: SimpleTextDialog.SimpleTextDialogBuilder.() -> Unit = {}
) = SimpleTextDialog.SimpleTextDialogBuilder()
    .apply(init)
    .build()
    .apply {
        show(this@simpleTextDialog.supportFragmentManager, SimpleTextDialog::class.simpleName)
    }

inline fun FragmentActivity.simpleWarningDialog(
    crossinline init: SimpleTextDialog.SimpleTextDialogBuilder.() -> Unit = {}
) = simpleTextDialog {
    title = getString(R.string.warning)
    icon = R.drawable.warning
    iconTint = R.color.warning
    isCancelable = false
    init(this)
}

inline fun FragmentActivity.simpleErrorDialog(
    crossinline init: SimpleTextDialog.SimpleTextDialogBuilder.() -> Unit = {}
) = simpleTextDialog {
    title = getString(R.string.error)
    icon = R.drawable.error
    iconTint = R.color.alert
    isCancelable = false
    init(this)
}

fun FragmentActivity.simpleTextDialog(text: CharSequence) = simpleTextDialog {
    this.text = text
}

fun Fragment.simpleTextDialog(
    init: SimpleTextDialog.SimpleTextDialogBuilder.() -> Unit = {}
) = requireActivity().simpleTextDialog(init)

fun Fragment.simpleWarningDialog(
    init: SimpleTextDialog.SimpleTextDialogBuilder.() -> Unit = {}
) = requireActivity().simpleWarningDialog(init)

fun Fragment.simpleErrorDialog(
    init: SimpleTextDialog.SimpleTextDialogBuilder.() -> Unit = {}
) = requireActivity().simpleErrorDialog(init)
