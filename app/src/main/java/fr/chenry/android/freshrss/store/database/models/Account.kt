package fr.chenry.android.freshrss.store.database.models

import androidx.room.Dao
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.PrimaryKey
import androidx.room.Query
import androidx.room.Update
import io.reactivex.Flowable
import kotlinx.coroutines.flow.Flow
import org.joda.time.DateTime
import org.joda.time.LocalDateTime
import java.util.concurrent.atomic.AtomicReference

data class TokenResponse(
    val SID: String,
    val Auth: String
)

@Entity(tableName = "accounts")
data class Account(
    val SID: String,
    val Auth: String,
    val login: String = "default_user",
    val serverInstance: String = "",
    val unreadCount: Int = 0,
    val lastFetchDate: DateTime = DateTime(0L)
) {
    // Limit to only one user for now
    @PrimaryKey
    var id = 1
        set(_) = Unit

    @Ignore
    private val mWriteToken = AtomicReference("")

    @Ignore
    private var writeTokenBirth = AtomicReference(LocalDateTime(0))

    var writeToken: String
        get() = mWriteToken.get()
        set(value) {
            mWriteToken.set(value)
            writeTokenBirth.set(LocalDateTime.now())
        }

    val isWriteTokenExpired
        get() = writeToken.isBlank() || writeTokenBirth.get().isBefore(LocalDateTime.now().minusMinutes(30))
}

val VoidAccount = Account("", "", serverInstance = "https://localhost")

data class AccountLastFetchDateUpdate(
    val id: Int,
    val lastFetchDate: DateTime
)

@Dao
interface AccountsDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(account: Account)

    @Update(entity = Account::class)
    suspend fun updateLastFetchDate(accountLastFetchDateUpdate: AccountLastFetchDateUpdate)

    @Query(ACCOUNT_SQL_QUERY)
    fun getAccountFlowable(): Flowable<List<Account>>

    @Query(ACCOUNT_SQL_QUERY)
    fun getAccountFlow(): Flow<Account?>

    @Query(ACCOUNT_SQL_QUERY)
    suspend fun getAccount(): Account?

    @Query("UPDATE accounts SET unreadCount = :unreadCount WHERE id = :id")
    suspend fun updateUnreadCount(id: Int, unreadCount: Int)

    @Query("UPDATE accounts SET unreadCount = unreadCount + 1 WHERE id = :id")
    suspend fun incrementUnreadCount(id: Int)

    @Query("UPDATE accounts SET unreadCount = unreadCount - 1 WHERE id = :id AND unreadCount > 0")
    suspend fun decrementUnreadCount(id: Int)

    companion object {
        const val ACCOUNT_SQL_QUERY = "SELECT * FROM accounts LIMIT 1"
    }
}
