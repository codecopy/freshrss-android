package fr.chenry.android.freshrss.components.navigationdrawer

import android.app.Dialog
import android.content.ClipboardManager
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import fr.chenry.android.freshrss.R
import fr.chenry.android.freshrss.databinding.FragmentAddFeedDialogBinding
import fr.chenry.android.freshrss.store.database.FreshRSSDabatabase
import fr.chenry.android.freshrss.utils.CustomDispatchers
import fr.chenry.android.freshrss.utils.addTrailingSlash
import fr.chenry.android.freshrss.utils.extractURLs
import fr.chenry.android.freshrss.utils.queryParameters
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import java.net.URL

class AddSubscriptionDialog(private val callback: (CallbackInfos) -> Unit) : DialogFragment(), KoinComponent {
    private val dispatchers by inject<CustomDispatchers>()
    private val db by inject<FreshRSSDabatabase>()

    private val clipboard by lazy {
        requireContext().getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
    }

    private val binding by lazy {
        FragmentAddFeedDialogBinding.inflate(
            requireActivity().layoutInflater,
            LinearLayout(context),
            false
        )
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog = requireActivity().let {
        val categories = runBlocking {
            withContext(dispatchers.db) {
                db.allCategories()
            }
        }

        val termsList = listOf(
            FeedCat(getString(R.string.default_category), null),
            *categories.map { t -> FeedCat(t.label, t.id) }.toTypedArray()
        )

        val adapter = Adaptater(requireActivity(), termsList)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.category.adapter = adapter

        val result = AlertDialog.Builder(it).setView(binding.root)
            .setPositiveButton(android.R.string.ok) { _, _ ->
                val url = tryProcessYoutubeUrl(binding.urlField.text.toString().trim())
                val cat = binding.category.selectedItem?.let { cat ->
                    cat as FeedCat
                    if (cat.id == null) null else categories.find { item -> item.label == cat.label }!!.id
                }
                callback(CallbackInfos(url = url, category = cat))
            }
            .setNegativeButton(android.R.string.cancel) { dialog, _ -> dialog.cancel() }
            .create()

        binding.urlField.apply {
            if (clipboard.hasPrimaryClip()) {
                val item = clipboard.primaryClip!!.getItemAt(0)
                val uris = when {
                    item.text != null -> item.text.toString().extractURLs()
                    item.uri != null -> listOf(item.uri)
                    else -> listOf()
                }

                if (uris.isNotEmpty()) {
                    setText(uris[0].toString(), TextView.BufferType.EDITABLE)
                    setSelection(0)
                }
            }
        }

        result
    }

    companion object {
        /**
         * Will try to detect a Youtube's user profile or playlist URL and return the correct
         * Youtube's RSS feed for the user or playlist.
         *
         * If provided URL is not a Youtube or invidio.us URL, the provided URL will be returned as is.
         *
         * Examples :
         *
         *     >>>> tryProcessYoutubeUrl("https://www.youtube.com/channel/UCNHbHR2KOc851Kkb_TJ2Orw")
         *     "https://www.youtube.com/feeds/videos.xml?channel_id=UCNHbHR2KOc851Kkb_TJ2Orw"
         *
         *     >>>> tryProcessYoutubeUrl("https://www.youtube.com/user/CosmicSoundwaves/videos")
         *     "https://www.youtube.com/feeds/videos.xml?user=CosmicSoundwaves"
         *
         *     >>>> tryProcessYoutubeUrl("https://www.youtube.com/watch?v=2w7hAuA9dq8&list=PLygWqO36YZG039tHkjo0v4O-1vl7l74or")
         *     "https://www.youtube.com/feeds/videos.xml?playlist_id=PLygWqO36YZG039tHkjo0v4O-1vl7l74or"
         */
        fun tryProcessYoutubeUrl(url: String): String {
            val ytUrl = runCatching { URL(url) }.getOrNull() ?: return url
            val path = ytUrl.path!!.addTrailingSlash()

            val domain = ytUrl.host!!.split(".").takeLast(2).joinToString(".")

            if (!domain.startsWith("youtube.") && ytUrl.host != "invidio.us") return url

            val channelRegexGroups = "^/channel/([^/]+)/.*$".toRegex().matchEntire(path)?.groupValues ?: listOf()
            if (channelRegexGroups.size == 2)
                return "https://www.youtube.com/feeds/videos.xml?channel_id=${channelRegexGroups[1]}"

            val userRegexGroups = "^/user/([^/]+)/.*$".toRegex().matchEntire(path)?.groupValues ?: listOf()
            if (userRegexGroups.size == 2)
                return "https://www.youtube.com/feeds/videos.xml?user=${userRegexGroups[1]}"

            val queryParameters = ytUrl.queryParameters()
            if ("list" in queryParameters.keys)
                return "https://www.youtube.com/feeds/videos.xml?playlist_id=${queryParameters["list"]}"

            return url
        }
    }

    data class CallbackInfos(
        val url: String,
        val category: String? = null
    )

    private data class FeedCat(val label: String, val id: String?)

    private class Adaptater(context: Context, items: List<FeedCat>) : ArrayAdapter<FeedCat>(
        context, android.R.layout.simple_spinner_item, items
    ) {
        private val inflater = LayoutInflater.from(context)
        private var dropdownResource = android.R.layout.simple_spinner_item

        override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
            val view = convertView ?: inflater.inflate(android.R.layout.simple_spinner_item, parent, false)
            val text = view.findViewById<TextView>(android.R.id.text1)
            text.text = getItem(position)?.label
            return view
        }

        override fun setDropDownViewResource(resource: Int) {
            this.dropdownResource = resource
        }

        override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
            val view = convertView ?: inflater.inflate(dropdownResource, parent, false)
            val text = view.findViewById<TextView>(android.R.id.text1)
            text.text = getItem(position)?.label
            return view
        }
    }
}
