package fr.chenry.android.freshrss.utils

import android.app.NotificationManager
import android.os.Build
import fr.chenry.android.freshrss.R

object NotificationHelper {
    const val ONGOING_REFRESH_NOTIFICATION = 1
    const val FAIL_REFRESH_NOTIFICATION = 2
}

enum class NotificationChannels(val nameResourceId: Int, val descriptionResourceId: Int, val importance: Importance) {
    REFRESH(
        R.string.notification_channel_refresh_title,
        R.string.notification_channel_refresh_description,
        Importance.IMPORTANCE_DEFAULT
    ),
    ERRORS(
        R.string.notification_channel_errors_title,
        R.string.notification_channel_errors_description,
        Importance.IMPORTANCE_HIGH
    );

    val channelId: String get() = this.name

    enum class Importance {
        IMPORTANCE_NONE,
        IMPORTANCE_MIN,
        IMPORTANCE_LOW,
        IMPORTANCE_DEFAULT,
        IMPORTANCE_HIGH,
        IMPORTANCE_MAX;

        fun asAndroid() = if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) 0 else when (this) {
            IMPORTANCE_NONE -> NotificationManager.IMPORTANCE_NONE
            IMPORTANCE_MIN -> NotificationManager.IMPORTANCE_MIN
            IMPORTANCE_LOW -> NotificationManager.IMPORTANCE_LOW
            IMPORTANCE_DEFAULT -> NotificationManager.IMPORTANCE_DEFAULT
            IMPORTANCE_HIGH -> NotificationManager.IMPORTANCE_HIGH
            IMPORTANCE_MAX -> NotificationManager.IMPORTANCE_MAX
        }
    }
}
