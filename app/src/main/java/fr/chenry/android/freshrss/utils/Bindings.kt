package fr.chenry.android.freshrss.utils

import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.Typeface
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageView
import androidx.databinding.BindingAdapter

@BindingAdapter("font")
fun textStyle(textView: TextView, typeface: String) {
    textView.typeface = when (typeface) {
        "bold" -> Typeface.create(textView.typeface, Typeface.BOLD)
        "italic" -> Typeface.create(textView.typeface, Typeface.ITALIC)
        "bold_italic" -> Typeface.create(textView.typeface, Typeface.BOLD_ITALIC)
        else -> Typeface.create(textView.typeface, Typeface.NORMAL)
    }
}

@BindingAdapter("android:src")
fun setImageViewResource(imageView: ImageView, resource: Any?) = when (resource) {
    is Bitmap -> imageView.setImageBitmap(resource)
    is Int -> imageView.setImageResource(resource)
    is Drawable -> imageView.setImageDrawable(resource)
    else -> imageView.setImageDrawable(ColorDrawable(Color.TRANSPARENT))
}

@BindingAdapter("srcCompat")
fun setImageViewResourceCompat(imageView: AppCompatImageView, resource: Any?) =
    setImageViewResource(imageView, resource)

@BindingAdapter("android:visibility")
fun View.setVisibility(visible: Boolean?) {
    visible?.let { visibility = if (it) VISIBLE else GONE }
}
