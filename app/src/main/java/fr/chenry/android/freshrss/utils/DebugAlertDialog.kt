package fr.chenry.android.freshrss.utils

import android.annotation.SuppressLint
import android.app.Dialog
import android.os.Bundle
import android.text.SpannableStringBuilder
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.LinearLayout
import androidx.appcompat.app.AlertDialog
import androidx.core.text.bold
import androidx.core.text.toSpanned
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.LiveData
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.work.Data
import androidx.work.WorkInfo
import fr.chenry.android.freshrss.databinding.DebugAlertDialogBinding
import fr.chenry.android.freshrss.databinding.DebugAlertDialogWorkInfosBinding
import fr.chenry.android.freshrss.store.viewmodels.RefreshWorkInfosVM

class DebugAlertDialog : DialogFragment() {
    val viewModel by viewModels<RefreshWorkInfosVM>()

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog = requireActivity().let {
        val view = DebugAlertDialogBinding.inflate(
            requireActivity().layoutInflater,
            LinearLayout(context),
            false
        )

        val divider = DividerItemDecoration(view.root.context, DividerItemDecoration.HORIZONTAL)
        view.lifecycleOwner = requireActivity()
        view.periodicWorkInfos = viewModel.periodicRefreshWorksLiveData
        view.alertDialogWorkInfos.layoutManager = LinearLayoutManager(view.root.context)
        view.alertDialogWorkInfos.adapter = WorkInfosAdapter(viewModel.periodicRefreshWorksLiveData)
        view.alertDialogWorkInfos.addItemDecoration(divider)

        view.finishedWorkInfos = viewModel.finishedWorkLiveData
        view.alertDialogFinishedWorkInfos.layoutManager = LinearLayoutManager(view.root.context)
        view.alertDialogFinishedWorkInfos.adapter = WorkInfosAdapter(viewModel.finishedWorkLiveData)
        view.alertDialogFinishedWorkInfos.addItemDecoration(divider)

        AlertDialog.Builder(it)
            .setView(view.root)
            .setCancelable(false)
            .setPositiveButton(android.R.string.ok) { _, _ -> dismiss() }
            .create()
    }

    override fun onResume() {
        super.onResume()
        isCancelable = false
    }

    @SuppressLint("NotifyDataSetChanged")
    private inner class WorkInfosAdapter(private val workInfos: LiveData<List<WorkInfo>>) :
        RecyclerView.Adapter<WorkInfosAdapter.ViewHolder>() {

        init {
            workInfos.observe(requireActivity()) {
                notifyDataSetChanged()
            }
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
            ViewHolder(DebugAlertDialogWorkInfosBinding.inflate(layoutInflater, parent, false))

        override fun getItemCount(): Int = workInfos.value?.size ?: 0

        override fun onBindViewHolder(holder: ViewHolder, position: Int) =
            workInfos.value?.get(position)?.let(holder::bind).unit()

        private inner class ViewHolder(
            private val binding: DebugAlertDialogWorkInfosBinding
        ) : RecyclerView.ViewHolder(binding.root) {

            fun bind(workinfos: WorkInfo) {
                val outData = workinfos.outputData.keyValueMap.map {
                    SpannableStringBuilder().apply {
                        bold { append("${it.key}: ") }
                        append(it.value.toString())
                    }.toSpanned()
                }
                binding.resultInfosVisibility = if (workinfos.outputData != Data.EMPTY) View.VISIBLE else View.GONE
                binding.periodicWorkInfo = workinfos
                binding.debugWorkInfoOut.adapter = ArrayAdapter(
                    requireContext(),
                    android.R.layout.simple_list_item_1,
                    outData
                )
            }
        }
    }
}
