package fr.chenry.android.freshrss.components.login

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.NavigationUI.setupActionBarWithNavController
import fr.chenry.android.freshrss.R
import fr.chenry.android.freshrss.activities.LoginActivity
import fr.chenry.android.freshrss.utils.isTopLevelDestination

class LoginWrapperFragment : Fragment(R.layout.fragment_login_wrapper), LoginActivity.ChildFragment {
    val navigation: NavController by lazy {
        val navHost = childFragmentManager.findFragmentById(R.id.connection_infos_host_fragment) as NavHostFragment
        navHost.navController
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val appBarConfiguration = AppBarConfiguration(navigation.graph)
        setupActionBarWithNavController(requireActivity() as AppCompatActivity, navigation, appBarConfiguration)
    }

    override fun canGoBack(): Boolean = !navigation.isTopLevelDestination()
    override fun onSupportNavigateUp(): Boolean = navigation.navigateUp()
}
