package fr.chenry.android.freshrss.store.database

import androidx.sqlite.db.SupportSQLiteDatabase
import dev.matrix.roomigrant.rules.OnMigrationStartRule

class ManualMigrations {
    @OnMigrationStartRule(version1 = 6, version2 = 7)
    fun migrate6To7before(
        db: SupportSQLiteDatabase,
        @Suppress("UNUSED_PARAMETER") version1: Int,
        @Suppress("UNUSED_PARAMETER") version2: Int
    ) {
        val cursor = db.query("SELECT DISTINCT serverInstance FROM accounts")
        if (cursor.count == 0) return
        cursor.moveToFirst()

        do {
            val serverInstance = cursor.getString(0)
            val newServerInstance = serverInstance
                .let { if (!it.contains("://")) "https://$it" else it }
                .let { if (!it.endsWith("/api/greader.php")) "$it${"/api/greader.php"}" else it }

            db.execSQL(
                "UPDATE accounts SET serverInstance = '$newServerInstance' " +
                    "WHERE serverInstance LIKE '$serverInstance' " +
                    "AND NOT serverInstance LIKE '$newServerInstance'"
            )
        } while (cursor.moveToNext())
    }
}
