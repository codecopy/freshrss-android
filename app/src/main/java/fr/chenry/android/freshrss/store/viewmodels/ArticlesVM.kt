package fr.chenry.android.freshrss.store.viewmodels

import android.app.Application
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.map
import fr.chenry.android.freshrss.FreshRSSApplication
import fr.chenry.android.freshrss.components.feeds.FeedSection
import fr.chenry.android.freshrss.store.database.models.Articles
import fr.chenry.android.freshrss.store.database.models.Feed

class ArticlesVM(
    app: Application,
    source: LiveData<Articles>,
    val feed: Feed,
) : AndroidViewModel(app) {
    val sortedFeedsLiveData = source.map { it.sortedWith(ArticleComparator) }
}

class ArticlesViewModelProvider(
    activity: FragmentActivity,
    feed: Feed,
    section: FeedSection
) : ViewModelProvider(
    activity.viewModelStore, ArticlesVMF(activity.application as FreshRSSApplication, feed, section)
) {

    private val key = "stream=${feed.id}:section=$section"

    override fun <T : ViewModel> get(modelClass: Class<T>): T = super.get(key, modelClass)

    class ArticlesVMF(
        private val application: FreshRSSApplication,
        private val feed: Feed,
        private val section: FeedSection
    ) : ViewModelProvider.NewInstanceFactory() {

        @Suppress("UNCHECKED_CAST")
        override fun <T : ViewModel> create(modelClass: Class<T>): T = when (section) {
            FeedSection.ALL -> ArticlesVM(application, application.db.getArticlesByStreamId(feed.id), feed)
            FeedSection.UNREAD -> ArticlesVM(
                application, application.db.getArticleByStreamIdAndUnread(feed.id), feed
            )
            FeedSection.FAVORITES -> ArticlesVM(
                application, application.db.getArticleByStreamIdAndFavorite(feed.id), feed
            )
        } as T
    }
}
