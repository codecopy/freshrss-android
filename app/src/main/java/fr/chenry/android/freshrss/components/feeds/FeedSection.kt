package fr.chenry.android.freshrss.components.feeds

import android.os.Parcelable
import fr.chenry.android.freshrss.R
import kotlinx.parcelize.Parcelize

@Parcelize
enum class FeedSection(val navigationButtonId: Int) : Parcelable {
    FAVORITES(R.id.subscriptions_bottom_navigation_favorites),
    ALL(R.id.subscriptions_bottom_navigation_all),
    UNREAD(R.id.subscriptions_bottom_navigation_unread);

    companion object {
        fun byPosition(position: Int) = values().let {
            if (position > it.size - 1) ALL else it[position]
        }

        fun fromNavigationButton(buttonId: Int) = when (buttonId) {
            R.id.subscriptions_bottom_navigation_favorites -> FAVORITES
            R.id.subscriptions_bottom_navigation_all -> ALL
            R.id.subscriptions_bottom_navigation_unread -> UNREAD
            else -> ALL
        }
    }
}
