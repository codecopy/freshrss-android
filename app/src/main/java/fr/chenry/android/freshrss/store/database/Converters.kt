package fr.chenry.android.freshrss.store.database

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import androidx.room.TypeConverter
import fr.chenry.android.freshrss.store.database.models.FavoriteStatus
import fr.chenry.android.freshrss.store.database.models.ReadStatus
import fr.chenry.android.freshrss.utils.e
import fr.chenry.android.freshrss.utils.escape
import fr.chenry.android.freshrss.utils.getOrDefault
import fr.chenry.android.freshrss.utils.onFailure
import fr.chenry.android.freshrss.utils.safeResult
import fr.chenry.android.freshrss.utils.unescape
import fr.chenry.android.freshrss.utils.unixEpochMillis
import org.joda.time.DateTime
import org.joda.time.LocalDateTime
import org.joda.time.format.ISODateTimeFormat
import java.io.ByteArrayOutputStream

interface DbSerializable {

    val dbName: String
}

class Converters {

    @TypeConverter
    fun readStatusFromString(value: String?) = ReadStatus.dbValueOf(value)

    @TypeConverter
    fun favoriteStatusFromString(value: String?) = FavoriteStatus.dbValueOf(value)

    @TypeConverter
    fun dbSerializableToString(dbSerializable: DbSerializable) = dbSerializable.dbName

    @TypeConverter
    fun listOfStringToString(list: List<String>) = list.joinToString(" ") { it.escape() }

    @TypeConverter
    fun stringToListOfString(string: String) = string.split("\\s+".toRegex()).map { it.unescape() }

    @TypeConverter
    fun bitmapToBlob(bitmap: Bitmap?) =
        (
            if (bitmap == null) ByteArray(0) else
                ByteArrayOutputStream().apply {
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, this)
                }.toByteArray()
            )!!

    @TypeConverter
    fun blobToBitmap(byteArray: ByteArray?) =
        if (byteArray?.isEmpty() != false) null else BitmapFactory.decodeStream(byteArray.inputStream())

    @TypeConverter
    fun localDateTimeToLong(localDateTime: LocalDateTime) = localDateTime.unixEpochMillis

    @TypeConverter
    fun longToLocalDateTime(epoch: Long) = safeResult { LocalDateTime(epoch) }
        .onFailure(this::e)
        .getOrDefault(LocalDateTime(0))

    @TypeConverter
    fun dateTimeToString(dateTime: DateTime): String = dateTime.toString(ISODateTimeFormat.basicDateTime())

    @TypeConverter
    fun stringToDateTime(string: String): DateTime =
        safeResult { DateTime.parse(string, ISODateTimeFormat.basicDateTime()) }
            .onFailure(this::e)
            .getOrDefault(DateTime(0))
}
