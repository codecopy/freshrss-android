package fr.chenry.android.freshrss.store.api.utils

import okhttp3.ResponseBody
import retrofit2.Converter
import retrofit2.Retrofit
import java.lang.reflect.Type
import kotlin.reflect.full.primaryConstructor

internal class APIConverter : Converter.Factory() {
    override fun responseBodyConverter(
        type: Type,
        annotations: Array<Annotation>,
        retrofit: Retrofit
    ): Converter<ResponseBody, *>? {
        annotations.forEach {
            if (it is UseDeserializer) {
                return it.deserializer.objectInstance
                    ?: it.deserializer.primaryConstructor?.call()
                    ?: it.deserializer.constructors.find { c -> c.parameters.isEmpty() }?.call()
                    ?: throw NoSuchMethodException(
                        "Unable to find a parameterless constructor for class ${it.deserializer.simpleName}"
                    )
            }
        }

        return null
    }
}
