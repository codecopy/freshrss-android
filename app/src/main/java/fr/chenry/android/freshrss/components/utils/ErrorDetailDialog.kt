package fr.chenry.android.freshrss.components.utils

import android.app.Dialog
import android.content.ClipData
import android.content.ClipboardManager
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import fr.chenry.android.freshrss.R
import fr.chenry.android.freshrss.databinding.ErrorDetailDialogBinding
import org.acra.ktx.sendSilentlyWithAcra
import java.util.UUID

class ErrorDetailDialog(private val error: Throwable) : DialogFragment() {
    private val binding by lazy {
        ErrorDetailDialogBinding.inflate(requireActivity().layoutInflater, LinearLayout(context), false)
    }

    private val clipboard by lazy {
        ContextCompat.getSystemService(requireContext(), ClipboardManager::class.java)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        binding.errorDetailDialogExpandable.parentLayout.setOnClickListener {
            binding.errorDetailDialogExpandable.toggleLayout()
        }

        binding.errorDetailDialogExpandable.findViewById<TextView>(
            R.id.error_detail_dialog_stacktrace
        ).text = error.stackTraceToString()

        binding.errorDetailDialogClose.setOnClickListener { dismiss() }
        binding.errorDetailDialogSend.setOnClickListener {
            error.sendSilentlyWithAcra()
            dismiss()
        }

        clipboard?.let {
            val button = binding.errorDetailDialogExpandable.findViewById<Button>(R.id.error_detail_dialog_copy)
            button.visibility = View.VISIBLE
            button.setOnClickListener { _ ->
                it.setPrimaryClip(
                    ClipData.newPlainText(
                        UUID.randomUUID().toString(),
                        error.stackTraceToString()
                    )
                )
                Toast.makeText(
                    requireContext(),
                    getString(R.string.error_copied_to_clipboard),
                    Toast.LENGTH_SHORT
                ).show()
            }
        }

        return AlertDialog
            .Builder(requireActivity())
            .setView(binding.root)
            .create()
    }

    override fun onResume() {
        super.onResume()
        isCancelable = false
    }
}

fun FragmentActivity.errorDetailDialog(error: Throwable) =
    ErrorDetailDialog(error).show(supportFragmentManager, ErrorDetailDialog::class.qualifiedName)

fun Fragment.errorDetailDialog(error: Throwable) = requireActivity().errorDetailDialog(error)
