package fr.chenry.android.freshrss.components.articles

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.Toast
import android.widget.Toast.LENGTH_SHORT
import androidx.annotation.VisibleForTesting
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.view.MenuProvider
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.navArgs
import fr.chenry.android.freshrss.R
import fr.chenry.android.freshrss.components.articles.webviewutils.ShareIntent
import fr.chenry.android.freshrss.databinding.FragmentSubscriptionArticleDetailBinding
import fr.chenry.android.freshrss.store.FreshRSSPreferences
import fr.chenry.android.freshrss.store.Store
import fr.chenry.android.freshrss.store.database.FreshRSSDabatabase
import fr.chenry.android.freshrss.store.database.models.Article
import fr.chenry.android.freshrss.store.database.models.ReadStatus
import fr.chenry.android.freshrss.testutils.EspressoIdlingResource.displayIdlingResource
import fr.chenry.android.freshrss.testutils.EspressoIdlingResource.unaryMinus
import fr.chenry.android.freshrss.testutils.EspressoIdlingResource.unaryPlus
import fr.chenry.android.freshrss.utils.e
import fr.chenry.android.freshrss.utils.isConnectedToNetwork
import fr.chenry.android.freshrss.utils.onFailure
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.koin.android.ext.android.inject

class ArticleDetailFragment : Fragment(), MenuProvider {
    private val args by navArgs<ArticleDetailFragmentArgs>()
    private val articleId by lazy { args.articleId }

    val store by inject<Store>()
    private val db by inject<FreshRSSDabatabase>()
    private val preferences by inject<FreshRSSPreferences>()
    private val articleLiveData by lazy { db.getArticleById(articleId) }
    private var feedTitle = ""

    @VisibleForTesting
    val supportActionBar get() = (requireActivity() as AppCompatActivity).supportActionBar!!
    private val article: Article get() = articleLiveData.value!!

    private lateinit var binding: FragmentSubscriptionArticleDetailBinding

    private val initObserver = Observer(::init)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        requireActivity().addMenuProvider(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        requireActivity().removeMenuProvider(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        +displayIdlingResource
        binding = FragmentSubscriptionArticleDetailBinding.inflate(inflater, container, false)
        articleLiveData.observe(viewLifecycleOwner, initObserver)
        articleLiveData.observe(viewLifecycleOwner) {
            lifecycleScope.launchWhenStarted {
                feedTitle = db.getFeedTitleById(it.streamId)
            }
        }
        return binding.root
    }

    override fun onCreateMenu(menu: Menu, menuInflater: MenuInflater) {
        menuInflater.inflate(R.menu.article_actionbar, menu)
        val menuItem = menu.findItem(R.id.action_mark_read_status) ?: return
        val context = requireContext()

        articleLiveData.observe(viewLifecycleOwner) {
            val (drawableRes, titleRes) = when (it.readStatus) {
                ReadStatus.READ -> R.drawable.ic_is_read_24dp to R.string.mark_unread
                ReadStatus.UNREAD -> R.drawable.ic_is_unread_24dp to R.string.mark_read
            }
            val title = context.getString(titleRes)
            menuItem.icon = ContextCompat.getDrawable(context, drawableRes)
            menuItem.title = title
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) menuItem.contentDescription = title
        }
    }

    override fun onMenuItemSelected(menuItem: MenuItem): Boolean = when (menuItem.itemId) {
        R.id.action_mark_read_status -> setReadStatus(article.readStatus.toggle()).let { true }
        else -> false
    }

    override fun onResume() {
        super.onResume()
        requireActivity().window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
    }

    override fun onPause() {
        super.onPause()
        requireActivity().window.clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        val scrollPosition = binding.fragmentSubscriptionArticleDetailWebView.scrollY
        lifecycleScope.launch {
            db.updateArticleScrollPosition(article.id, scrollPosition)
        }
    }

    override fun onDestroyView() {
        supportActionBar.subtitle = null
        super.onDestroyView()
    }

    private fun init(article: Article) {
        val activityInfos = getBrowserIntent().resolveActivityInfo(requireActivity().packageManager, 0)

        binding.apply {
            fragmentSubscriptionArticleDetailLoader.visibility = GONE
            fragmentSubscriptionArticleDetailWebView.visibility = VISIBLE
            canOpenBrowser = activityInfos?.exported ?: false
            fabOpenBrowser.setOnClickListener { openInBrowser() }
            fabShare.setOnClickListener { shareInToApp() }

            fragmentSubscriptionArticleDetailWebView.apply {
                load(this@ArticleDetailFragment, this@ArticleDetailFragment.article)
                if (preferences.retainScrollPosition) {
                    scrollY = article.scrollPosition
                }
            }
        }

        if (article.readStatus == ReadStatus.UNREAD) setReadStatus(ReadStatus.READ)
        else -displayIdlingResource

        articleLiveData.observe(viewLifecycleOwner) {
            supportActionBar.subtitle = it.title
        }

        articleLiveData.removeObserver(initObserver)
    }

    private fun getBrowserIntent() = Intent(Intent.ACTION_VIEW, article.url)

    private fun openInBrowser() = startActivity(getBrowserIntent())

    private fun shareInToApp() = startActivity(
        Intent.createChooser(
            ShareIntent.create(feedTitle, article),
            getString(R.string.share_article, feedTitle)
        )
    )

    private fun setReadStatus(readStatus: ReadStatus) {
        if (!requireContext().isConnectedToNetwork()) {
            Toast.makeText(requireContext(), R.string.no_internet_connection_avaible, Toast.LENGTH_LONG).show()
            return
        }

        if (article.readStatusRequestOnGoing) {
            Toast.makeText(requireContext(), R.string.request_already_ongoing, LENGTH_SHORT).show()
            return
        }

        val ctx = requireContext()

        /**
         * Major mistake: Kotlin's [Result] cannot be used as the result of a [kotlinx.coroutines.GlobalScope.launch]
         * call. An intermediary function uses it as the indicator that the coroutine failed.
         * Now I know why using [Result] as a return type in Kotlin a forbidden...
         */
        lifecycleScope.launchWhenStarted {
            val result = store.postReadStatus(article, readStatus)
            -displayIdlingResource

            result.onFailure {
                this@ArticleDetailFragment.apply {
                    this.e(it)

                    val readText = when (readStatus) {
                        ReadStatus.READ -> getString(R.string.read)
                        ReadStatus.UNREAD -> getString(R.string.unread)
                    }.let { msg ->
                        getString(R.string.mark_read_status_authorization, msg)
                    }

                    val toastText = getString(R.string.unable_to, readText)

                    withContext(Dispatchers.Main) {
                        Toast.makeText(ctx, toastText, LENGTH_SHORT).show()
                    }
                }
            }
        }
    }
}
