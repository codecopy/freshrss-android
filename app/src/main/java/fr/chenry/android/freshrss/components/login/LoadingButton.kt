package fr.chenry.android.freshrss.components.login

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import androidx.core.content.ContextCompat
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import androidx.vectordrawable.graphics.drawable.Animatable2Compat
import androidx.vectordrawable.graphics.drawable.AnimatedVectorDrawableCompat
import com.google.android.material.button.MaterialButton
import fr.chenry.android.freshrss.R
import fr.chenry.android.freshrss.utils.NOOP
import kotlinx.coroutines.CompletableDeferred
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class LoadingButton @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = R.attr.materialButtonStyle
) : MaterialButton(context, attrs, defStyleAttr) {
    val state: LiveData<ConnectionUrlState> get() = mState
    private val mState = MutableLiveData(ConnectionUrlState.IDLE)

    private val circularProgressDrawable = getCircularProgressDrawable(context)
    private val doneDrawable = getDoneDrawable(context)
    private val nextArrowDrawable = getNextArrowDrawable(context)

    init {
        mState.observeForever {
            circularProgressDrawable.stop()
            doneDrawable.stop()

            isClickable = it == ConnectionUrlState.IDLE

            when (it!!) {
                ConnectionUrlState.IDLE -> {
                    icon = nextArrowDrawable
                }
                ConnectionUrlState.LOADING -> {
                    icon = circularProgressDrawable
                    circularProgressDrawable.callback = getDrawableCallback(::invalidate)
                    circularProgressDrawable.start()
                }
                ConnectionUrlState.OK -> {
                    icon = doneDrawable
                    doneDrawable.start()
                }
            }
        }
    }

    fun idle() = mState.postValue(ConnectionUrlState.IDLE)
    fun loading() = mState.postValue(ConnectionUrlState.LOADING)
    suspend fun okAsync(): Deferred<Unit> = withContext(Dispatchers.Main) {
        val deferred = CompletableDeferred<Unit>(null)

        doneDrawable.registerAnimationCallback(object : Animatable2Compat.AnimationCallback() {
            override fun onAnimationEnd(drawable: Drawable?) {
                doneDrawable.unregisterAnimationCallback(this)
                deferred.complete(Unit)
            }
        })

        mState.postValue(ConnectionUrlState.OK)

        deferred
    }

    enum class ConnectionUrlState { IDLE, LOADING, OK }

    companion object {
        private fun getCircularProgressDrawable(context: Context) = CircularProgressDrawable(context).apply {
            setStyle(CircularProgressDrawable.DEFAULT)
            val ta = context.obtainStyledAttributes(R.style.AppTheme, intArrayOf(R.attr.colorOnPrimary))
            val color = ta.getColor(0, Color.WHITE)
            ta.recycle()
            setColorSchemeColors(color)
        }

        private fun getDoneDrawable(context: Context) =
            AnimatedVectorDrawableCompat.create(context, R.drawable.avd_done_24dp)!!

        private fun getNextArrowDrawable(context: Context) =
            ContextCompat.getDrawable(context, R.drawable.ic_baseline_arrow_forward_24)

        private inline fun getDrawableCallback(crossinline cb: () -> Unit) = object : Drawable.Callback {
            override fun unscheduleDrawable(who: Drawable, what: Runnable) = NOOP()
            override fun invalidateDrawable(who: Drawable) = cb()
            override fun scheduleDrawable(who: Drawable, what: Runnable, `when`: Long) = NOOP()
        }
    }
}
