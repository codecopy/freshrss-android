package fr.chenry.android.freshrss.utils

import android.content.Context
import android.util.AttributeSet
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import androidx.annotation.DrawableRes
import androidx.appcompat.widget.AppCompatImageView
import fr.chenry.android.freshrss.R

class EmotionnalImageSubtext @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0,
    defStyleRes: Int = 0
) : LinearLayout(context, attrs, defStyle, defStyleRes) {

    var text: CharSequence?
        get() = textView.text
        set(value) {
            textView.text = value
        }

    private var imageView: AppCompatImageView
    private var textView: TextView

    init {
        val a = context.obtainStyledAttributes(attrs, R.styleable.EmotionnalImageSubtext, defStyle, 0)
        val text = a.getString(R.styleable.EmotionnalImageSubtext_text)
        val src = a.getDrawable(R.styleable.EmotionnalImageSubtext_src)
        a.recycle()

        orientation = VERTICAL
        gravity = Gravity.CENTER
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        inflater.inflate(R.layout.emotional_image_subtext, this, true)

        imageView = getChildAt(0) as AppCompatImageView
        textView = getChildAt(1) as TextView

        imageView.setImageDrawable(src)
        textView.visibility = if (text.isNullOrBlank()) View.GONE else View.VISIBLE
        textView.text = text
    }

    fun setImageResource(@DrawableRes resId: Int) = imageView.setImageResource(resId)
}
