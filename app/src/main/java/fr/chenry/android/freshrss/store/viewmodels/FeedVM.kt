package fr.chenry.android.freshrss.store.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import fr.chenry.android.freshrss.FreshRSSApplication
import fr.chenry.android.freshrss.components.feeds.FeedSection
import fr.chenry.android.freshrss.components.feeds.FeedSection.ALL
import fr.chenry.android.freshrss.components.feeds.FeedSection.FAVORITES
import fr.chenry.android.freshrss.components.feeds.FeedSection.UNREAD
import fr.chenry.android.freshrss.store.database.models.Feed
import fr.chenry.android.freshrss.store.database.models.FeedCategory
import fr.chenry.android.freshrss.store.database.models.FeedTags
import fr.chenry.android.freshrss.store.database.models.Feeds
import fr.chenry.android.freshrss.store.database.models.VoidCategory
import fr.chenry.android.freshrss.utils.requireF
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.runBlocking

sealed class FeedVM(
    application: Application,
    rawFeeds: Flow<Feeds>,
    rawCategories: Flow<FeedTags>,
    private val comparator: Comparator<Feed>,
    private val grouper: Grouper,
) : AndroidViewModel(application) {
    val categories: StateFlow<List<FeedCategory>> = rawCategories.map(::sortCategories).distinctUntilChanged().stateIn(
        scope = viewModelScope,
        started = SharingStarted.Eagerly,
        initialValue = runBlocking { sortCategories(rawCategories.first()) }
    )
    val feeds: StateFlow<Map<String, Feeds>> = rawFeeds.map(::sortFeeds).distinctUntilChanged().stateIn(
        scope = viewModelScope,
        started = SharingStarted.Eagerly,
        initialValue = runBlocking { sortFeeds(rawFeeds.first()) }
    )

    private fun sortCategories(rawCategories: FeedTags) = rawCategories.sortedWith(LabelComparator)
    private fun sortFeeds(rawFeeds: Feeds) = rawFeeds.sortedWith(comparator).groupBy {
        grouper(requireF().applicationContext, it)
    }
}

class AllFeedVM(app: Application) : FeedVM(
    app as FreshRSSApplication,
    app.db.getAllFeeds(),
    app.db.getAllFolders(),
    TitleComparator,
    TitleGrouper
)

class UnreadFeedVM(app: Application) : FeedVM(
    app as FreshRSSApplication,
    app.db.getAllUnreadSubcriptions(),
    app.db.getFoldersWithUnreadItems(),
    DateComparator,
    DateGrouper
)

class FavoritesFeedVM(app: Application) : FeedVM(
    app as FreshRSSApplication,
    app.db.getAllFeedsWithFavorites(),
    app.db.getFoldersWithFavorites(),
    TitleComparator,
    TitleGrouper
)

class FeedForCategoryVM(
    app: Application,
    category: FeedCategory,
    section: FeedSection
) : FeedVM(
    app as FreshRSSApplication,
    getFeedFlow(app, category, section),
    flowOf(listOf()),
    if (section == UNREAD) DateComparator else TitleComparator,
    if (section == UNREAD) DateGrouper else TitleGrouper,
) {
    companion object {
        private fun getFeedFlow(app: FreshRSSApplication, category: FeedCategory, section: FeedSection) = when (section) {
            FAVORITES -> app.db.getFeedsByCategoryAndFavorites(category)
            ALL -> app.db.getFeedsByCategory(category)
            UNREAD -> app.db.getFeedsByCategoryAndUnreadCount(category)
        }
    }
}

class FeedVMFactory(
    private val application: Application,
    private val category: FeedCategory,
    private val section: FeedSection
) : ViewModelProvider.NewInstanceFactory() {
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T =
        if (category != VoidCategory) FeedForCategoryVM(application, category, section) as T
        else when (section) {
            ALL -> AllFeedVM(application)
            UNREAD -> UnreadFeedVM(application)
            FAVORITES -> FavoritesFeedVM(application)
        } as T
}
