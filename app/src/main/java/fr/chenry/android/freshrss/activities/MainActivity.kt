package fr.chenry.android.freshrss.activities

import android.Manifest.permission.POST_NOTIFICATIONS
import android.content.Context
import android.content.SharedPreferences
import android.content.SharedPreferences.OnSharedPreferenceChangeListener
import android.os.Build
import android.os.Bundle
import android.view.MenuItem
import android.view.SubMenu
import android.widget.TextView
import androidx.activity.addCallback
import androidx.activity.viewModels
import androidx.annotation.VisibleForTesting
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.text.parseAsHtml
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import com.permissionx.guolindev.PermissionX
import fr.chenry.android.freshrss.R
import fr.chenry.android.freshrss.components.feeds.MainFeedFragmentDirections
import fr.chenry.android.freshrss.components.navigationdrawer.AddSubscriptionDialog
import fr.chenry.android.freshrss.components.navigationdrawer.AddSubscriptionWaitDialog
import fr.chenry.android.freshrss.components.utils.simpleTextDialog
import fr.chenry.android.freshrss.databinding.ActivityMainBinding
import fr.chenry.android.freshrss.store.Store
import fr.chenry.android.freshrss.store.viewmodels.AccountVM
import fr.chenry.android.freshrss.utils.BurgerMenuDrawerListener
import fr.chenry.android.freshrss.utils.DebugAlertDialog
import fr.chenry.android.freshrss.utils.e
import fr.chenry.android.freshrss.utils.isTopLevelDestination
import fr.chenry.android.freshrss.utils.loadKoinModules
import fr.chenry.android.freshrss.utils.onFailure
import fr.chenry.android.freshrss.utils.onSuccess
import fr.chenry.android.freshrss.utils.requireF
import org.koin.android.ext.android.inject
import org.koin.dsl.module

class MainActivity : AppCompatActivity(), OnSharedPreferenceChangeListener {
    private val accountVM by viewModels<AccountVM>()
    private val binding: ActivityMainBinding by lazy { ActivityMainBinding.inflate(layoutInflater) }
    private val store by inject<Store>()

    @VisibleForTesting
    val navigation: NavController by lazy {
        val navHost = supportFragmentManager.findFragmentById(R.id.main_activity_host_fragment) as NavHostFragment
        navHost.navController
    }

    private val drawerLayout get() = binding.activityMainNavigationDrawer
    private val leftMenu get() = binding.applicationLeftMenu

    private val appBarConfiguration by lazy {
        AppBarConfiguration(navigation.graph, drawerLayout)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        loadKoinModules(module { single<Context> { this@MainActivity } })
        setupActionBarWithNavController(navigation, appBarConfiguration)
        onBackPressedDispatcher.addCallback(this) { navigateUp(true) }
        drawerLayout.addDrawerListener(BurgerMenuDrawerListener(supportActionBar!!, navigation))
        navigation.addOnDestinationChangedListener { _, _, _ -> drawerLayout.closeDrawers() }
        accountVM.account.observe(this) {
            leftMenu.getHeaderView(0)
                .findViewById<TextView>(R.id.navigation_header_container_user)
                .text = it.login
        }

        enableDebugSection()
        requireF().preferences.registerChangeListener(this)
    }

    override fun onStart() {
        super.onStart()

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            val okText = getString(android.R.string.ok)
            val cancelText = getString(android.R.string.cancel)

            PermissionX.init(this)
                .permissions(POST_NOTIFICATIONS)
                .onExplainRequestReason { scope, deniedList ->
                    scope.showRequestReasonDialog(
                        deniedList, getString(R.string.notification_permission_required), okText, cancelText
                    )
                }.request(null)
        }
    }

    override fun onResume() {
        super.onResume()
        requireF().ensurePeriodicRequest()
    }

    fun onAddSubscriptionClick(@Suppress("UNUSED_PARAMETER") item: MenuItem) {
        drawerLayout.closeDrawers()
        AddSubscriptionDialog(::okCallback).show(supportFragmentManager, AddSubscriptionDialog::class.simpleName)
    }

    fun onSettingsItemClick(@Suppress("UNUSED_PARAMETER") item: MenuItem) =
        navigation.navigate(MainFeedFragmentDirections.actionGlobalSettingsFragment())

    /**
     * Called by onOptionsItemSelected when menu button is Home
     * When on the first screen of the app, the home button cyles between
     * opening and closing the drawer. Here, we handle only the first case
     * (app is on the first screen and drawer is closed). If drawer is closed,
     * open it. Otherwise, delegate to MainActivity#navigateUp since code is
     * common with the back button.
     */
    override fun onSupportNavigateUp(): Boolean = when {
        navigation.isTopLevelDestination() && !isDrawerOpen() -> openDrawer()
        else -> navigateUp()
    }

    override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences, key: String?) {
        if (key != requireF().preferences.debugModeKey) return
        enableDebugSection()
    }

    private fun enableDebugSection() {
        val debugMode = requireF().preferences.debugMode
        val submenu = leftMenu.menu.findItem(R.id.app_left_menu_app_sub)?.subMenu
        if (submenu !is SubMenu) return

        if (debugMode) submenu.add(0, R.id.debug_info_section_id, submenu.size(), R.string.debug_infos).apply {
            icon = ContextCompat.getDrawable(this@MainActivity, R.drawable.ic_build_black_24dp)
            setOnMenuItemClickListener {
                closeDrawer().apply {
                    DebugAlertDialog().show(supportFragmentManager, DebugAlertDialog::class.simpleName)
                }
            }
        } else submenu.removeItem(R.id.debug_info_section_id)
    }

    private fun isDrawerOpen() = drawerLayout.isDrawerOpen(leftMenu)
    private fun closeDrawer() = drawerLayout.closeDrawers().let { true }
    private fun openDrawer() = drawerLayout.openDrawer(leftMenu).let { true }

    /**
     * Handles pressing back button or home (burger menu) button.
     *
     * The logic is the following: is the drawer is opened, it should be closed and
     * process stops here.
     *
     * If the drawer is closed, then there's 2 possibilities: we're on the first
     * page of the app or not.
     *
     * If not then we should navigate up, whoever is the caller (home or
     * back button).
     *
     * If we're on the first page of the app, and MainActivity#navigateUp is
     * called from back button, then app should be gracely closed.
     *
     * The last case should never happen so we don't do anything but still
     * aknowledge the event.
     */
    private fun navigateUp(fromBackButton: Boolean = false): Boolean = when {
        isDrawerOpen() -> closeDrawer()
        !navigation.isTopLevelDestination() -> navigation.navigateUp()
        fromBackButton && !isDrawerOpen() -> finishAfterTransition().let { true }
        else -> true
    }

    private fun okCallback(input: AddSubscriptionDialog.CallbackInfos) {
        val dialog = AddSubscriptionWaitDialog()
        dialog.show(supportFragmentManager, AddSubscriptionWaitDialog::class.simpleName)
        lifecycleScope.launchWhenStarted {
            val postAsync = store.postAddSubscriptionAsync(input.url, input.category)

            dialog.dismiss()

            fun getTextDialog(id: Int) = getString(id, "<a href='${input.url}'>${input.url}</a>")
                .replace("\n", "<br/>")
                .parseAsHtml()

            postAsync.onSuccess {
                requireF().cancelOnGoingRefresh()
                requireF().forceRefresh()
            }.onFailure {
                when {
                    it.message!!.startsWith("Already subscribed", true) ->
                        simpleTextDialog(getTextDialog(R.string.feed_already_subscribed))
                    it.message!!.startsWith("A feed could not be found", true) ->
                        simpleTextDialog(getTextDialog(R.string.no_feed_found))
                    else -> {
                        simpleTextDialog(getTextDialog(R.string.add_feed_failed_unknown_error))
                        this.e("Error while trying to add feed ${input.url}: $it")
                    }
                }
            }
        }
    }
}
