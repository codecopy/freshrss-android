package fr.chenry.android.freshrss.components.articles.webviewutils

import android.content.Intent

class MailIntent(address: String, subject: String = "", body: String = "", cc: String = "") :
    Intent(ACTION_SEND) {

    init {
        putExtra(EXTRA_EMAIL, arrayOf(address))
        putExtra(EXTRA_TEXT, body)
        putExtra(EXTRA_SUBJECT, subject)
        putExtra(EXTRA_CC, cc)
        type = "message/rfc822"
    }
}
