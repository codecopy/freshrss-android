package fr.chenry.android.freshrss.utils

import fr.chenry.android.freshrss.store.Store
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import org.koin.core.context.loadKoinModules
import org.koin.core.module.Module
import org.koin.core.module.dsl.singleOf
import org.koin.dsl.module

data class CustomDispatchers(
    val network: CoroutineDispatcher,
    val db: CoroutineDispatcher,
)

val storeModule = module {
    singleOf(::Store)
}

val dispatchersModule = module {
    single {
        CustomDispatchers(Dispatchers.IO, Dispatchers.IO)
    }
}

fun loadKoinModules(vararg modules: Module) = loadKoinModules(modules.toList())
