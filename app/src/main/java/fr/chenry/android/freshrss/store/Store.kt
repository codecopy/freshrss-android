package fr.chenry.android.freshrss.store

import android.app.Application
import com.squareup.picasso.Picasso
import fr.chenry.android.freshrss.R
import fr.chenry.android.freshrss.store.api.API
import fr.chenry.android.freshrss.store.api.FAVORITE_ITEMS_ID
import fr.chenry.android.freshrss.store.api.READ_ITEMS_ID
import fr.chenry.android.freshrss.store.api.utils.NoNetworkException
import fr.chenry.android.freshrss.store.api.utils.NotConnectedException
import fr.chenry.android.freshrss.store.api.utils.ServerException
import fr.chenry.android.freshrss.store.database.FreshRSSDabatabase
import fr.chenry.android.freshrss.store.database.models.Account
import fr.chenry.android.freshrss.store.database.models.Article
import fr.chenry.android.freshrss.store.database.models.ArticleFavoriteStatusUpdate
import fr.chenry.android.freshrss.store.database.models.ArticleReadStatusUpdate
import fr.chenry.android.freshrss.store.database.models.FavoriteStatus
import fr.chenry.android.freshrss.store.database.models.Feed
import fr.chenry.android.freshrss.store.database.models.FeedCategory
import fr.chenry.android.freshrss.store.database.models.ReadStatus
import fr.chenry.android.freshrss.store.database.models.VoidAccount
import fr.chenry.android.freshrss.utils.CustomDispatchers
import fr.chenry.android.freshrss.utils.SafeResult
import fr.chenry.android.freshrss.utils.e
import fr.chenry.android.freshrss.utils.isConnectedToNetwork
import fr.chenry.android.freshrss.utils.onSuccess
import fr.chenry.android.freshrss.utils.safeResult
import fr.chenry.android.freshrss.utils.unixEpochSeconds
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import org.koin.core.component.KoinComponent
import retrofit2.Call
import java.util.concurrent.atomic.AtomicReference
import kotlin.reflect.KProperty

class Store(
    private val app: Application,
    private val db: FreshRSSDabatabase,
    private val dispatchers: CustomDispatchers,
) : KoinComponent {
    val account get() = accountFlow.value

    private val scope = CoroutineScope(SupervisorJob() + dispatchers.db)
    private val accountFlow = runBlocking { db.getAccountFlow().stateIn(scope) }
    private var service by object : AtomicReference<API?>(accountToAPI(account)) {
        operator fun getValue(_1: Any?, _2: KProperty<*>) = this.get()
        operator fun setValue(_1: Any?, _2: KProperty<*>, value: API?) = this.set(value)
    }

    init {
        scope.launch {
            accountFlow.collect {
                service = accountToAPI(it)
            }
        }
    }

    private fun accountToAPI(account: Account) = if (account == VoidAccount) null else API.get(account)

    //region TOP-LEVEL REFRESH API
    suspend fun getTags() = executeSafeApiCall { getTags() }.onSuccess { result ->
        withContext(dispatchers.db) {
            db.syncAllFeedCategories(result.map(FeedCategory.Companion::fromFeedTagItem))
        }
    }

    suspend fun getSubscriptions() = executeSafeApiCall { getSubscriptions() }.onSuccess { result ->
        withContext(dispatchers.db) {
            val feedInsertAsync = async {
                val subscriptions = result.associate { it.id to Feed.fromFeedApiItem(it) }
                db.syncSubscriptions(subscriptions)
            }

            val feedTagRelationInsertAsync = async {
                result.forEach { db.syncFeedToTagRelationForFeed(it.id, it.categories) }
            }

            listOf(feedInsertAsync, feedTagRelationInsertAsync).awaitAll()
        }
    }

    suspend fun fetchImages() {
        db.getFeedsWithImageToUpdate().forEach { feed ->
            runCatching {
                withContext(dispatchers.network) {
                    Picasso.get().load(feed.iconUrl).resizeDimen(R.dimen.feed_icon_size, R.dimen.feed_icon_size).get()
                }
            }.map { bmp ->
                withContext(dispatchers.db) {
                    val updatedFeed = feed.copy(iconUrl = feed.iconUrl.trim()).apply {
                        iconUrlFlag = iconUrl
                        imageBitmap = bmp
                    }
                    db.updateFeed(updatedFeed)
                }
            }.onFailure(this::e)
        }
    }

    suspend fun getUnreadCount() = executeSafeApiCall { getUnreadCount() }.onSuccess { result ->
        withContext(dispatchers.db) {
            val asyncs = result.unreadcounts.map { async { db.updateSubscriptionCount(it.id, it.count) } }
            val updateAsync = async { db.updateTotalUnreadCount(account.id, result.max) }
            (listOf(updateAsync) + asyncs).awaitAll()
        }
    }

    suspend fun getStreamContents(id: String) = executeSafeApiCall {
        val date = db.getArticlesMostRecentCrawled()?.minusDays(1)?.unixEpochSeconds ?: 0
        getStreamContents(id, "$date")
    }.onSuccess { result ->
        withContext(dispatchers.db) {
            val articles = result.items.map(Article.Companion::fromContentItem)
            val articlesInsertAsync = async { db.insertArticles(articles) }
            val insertAsync = result.items.map {
                async { db.updateSubscriptionNewestArticleDate(it.origin.streamId, it.crawled) }
            }
            (insertAsync + articlesInsertAsync).awaitAll()
        }
    }

    suspend fun getUnreadItemIds() = executeSafeApiCall { getItemIds(mapOf("xt" to READ_ITEMS_ID)) }.onSuccess {
        withContext(dispatchers.db) {
            db.syncAllArticlesReadStatus(it)
        }
    }

    suspend fun getFavoriteItemIds() = executeSafeApiCall { getItemIds(mapOf("s" to FAVORITE_ITEMS_ID)) }.onSuccess {
        withContext(dispatchers.db) {
            db.syncAllArticlesFavoriteStatus(it)
        }
    }
    //endregion

    suspend fun postReadStatus(
        article: Article,
        readStatus: ReadStatus,
    ): SafeResult<Unit> = withContext(dispatchers.network) {
        val resultAsync = async {
            executeSafeApiCall { postEditTag(readStatus.toPostFormData(article.id)) }
        }
        val delay = async { delay(200) }

        listOf(resultAsync, delay).awaitAll()

        resultAsync.await().onSuccess {
            db.updateArticleReadStatuses(listOf(ArticleReadStatusUpdate(article.id, readStatus)))
            when (readStatus) {
                ReadStatus.READ -> {
                    db.decrementSubscriptionCount(article.streamId)
                    db.decrementTotalUnreadCount(account.id)
                }
                ReadStatus.UNREAD -> {
                    db.incrementSubscriptionCount(article.streamId)
                    db.incrementTotalUnreadCount(account.id)
                }
            }
        }
    }

    suspend fun postFavoriteStatus(article: Article, favoriteStatus: FavoriteStatus): SafeResult<Unit> =
        withContext(dispatchers.network) {
            val resultAsync = async {
                executeSafeApiCall { postEditTag(favoriteStatus.toPostFormData(article.id)) }
            }
            val delay = async { delay(200) }

            listOf(resultAsync, delay).awaitAll()

            val result = resultAsync.await()

            result.onSuccess {
                db.updateArticleFavoriteStatuses(
                    listOf(
                        ArticleFavoriteStatusUpdate(
                            article.id,
                            favoriteStatus
                        )
                    )
                )
            }

            result
        }

    suspend fun postAddSubscriptionAsync(
        url: String,
        category: String?
    ): SafeResult<String> = withContext(dispatchers.network) {
        executeSafeApiCall { postAddSubscription("feed/$url", category) }
    }

    private suspend inline fun <T : Any> executeSafeApiCall(
        crossinline block: suspend API.() -> Call<T>
    ): SafeResult<T> = safeResult {
        if (!app.isConnectedToNetwork()) throw NoNetworkException()
        if (service == null) throw NotConnectedException()
        val result = withContext(dispatchers.network) { block(service!!).execute() }
        if (result.isSuccessful) return@safeResult result.body()!!
        else throw ServerException(result.raw())
    }
}
