package fr.chenry.android.freshrss.store.api.utils

import okhttp3.ResponseBody
import retrofit2.Converter
import kotlin.reflect.KClass

/** This annotation will ensure that a valid token is available.
if not, it will perform a query to get a new one */
@Target(AnnotationTarget.FUNCTION)
@Retention(AnnotationRetention.RUNTIME)
internal annotation class TokenRequired

/** This annotation always applies the correct `Authorization` header to the request */
@Target(AnnotationTarget.FUNCTION)
@Retention(AnnotationRetention.RUNTIME)
internal annotation class AuthorisationRequired

/** This annotation applies `output=json` query parameter
to the request so that API always return JSON */
@Target(AnnotationTarget.FUNCTION)
@Retention(AnnotationRetention.RUNTIME)
internal annotation class JsonOutput

/** This annotation let use a custom response deserializer */
@Target(AnnotationTarget.FUNCTION)
@Retention(AnnotationRetention.RUNTIME)
internal annotation class UseDeserializer(val deserializer: KClass<out Converter<ResponseBody, out Any>>)
