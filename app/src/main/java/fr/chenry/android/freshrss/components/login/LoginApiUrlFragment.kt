package fr.chenry.android.freshrss.components.login

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.AdapterView.OnItemSelectedListener
import androidx.core.widget.addTextChangedListener
import androidx.lifecycle.lifecycleScope
import fr.chenry.android.freshrss.R
import fr.chenry.android.freshrss.components.utils.errorDetailDialog
import fr.chenry.android.freshrss.components.utils.simpleErrorDialog
import fr.chenry.android.freshrss.components.utils.simpleWarningDialog
import fr.chenry.android.freshrss.databinding.FragmentLoginApiUrlBinding
import fr.chenry.android.freshrss.store.api.API_URL_OK_RESPONSE
import fr.chenry.android.freshrss.store.api.ConnectionWrapper
import fr.chenry.android.freshrss.testutils.EspressoIdlingResource.networkIdlingResource
import fr.chenry.android.freshrss.testutils.EspressoIdlingResource.unaryMinus
import fr.chenry.android.freshrss.testutils.EspressoIdlingResource.unaryPlus
import fr.chenry.android.freshrss.utils.getHtmlString
import fr.chenry.android.freshrss.utils.isWebUrl
import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.net.UnknownHostException

class LoginApiUrlFragment : LoginWrappedFragment<FragmentLoginApiUrlBinding>() {
    override val focusField get() = binding.instanceField
    override val imeNextField get() = binding.instanceField
    override val nextButton get() = binding.toCredentialsButton
    override val resetFields by lazy { arrayOf(binding.instanceContainer) }

    override fun inflateBinding(inflater: LayoutInflater, container: ViewGroup?) =
        FragmentLoginApiUrlBinding.inflate(inflater, container, false)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val view = super.onCreateView(inflater, container, savedInstanceState)

        binding.domain = viewModel.instanceDomain
        binding.actualUrl = viewModel.completeApiUrlWithStyle

        binding.instanceField.addTextChangedListener {
            binding.instanceContainer.error = ""
        }

        binding.protocolSelection.onItemSelectedListener = getSelectedItemListener {
            viewModel.protocol.postValue(binding.protocolSelection.selectedItem.toString())
        }

        viewModel.protocol.observe(viewLifecycleOwner) {
            val idx = viewModel.authorizedProtocols.indexOf(it)
            if (idx >= 0) requireActivity().runOnUiThread { binding.protocolSelection.setSelection(idx) }
            else viewModel.protocol.postValue(viewModel.authorizedProtocols[0])
        }

        binding.toCredentialsButton.state.observe(viewLifecycleOwner) {
            if (it == LoadingButton.ConnectionUrlState.IDLE) showKeyboard()
        }

        resetErrors()

        return view
    }

    override fun nextAction() {
        resetErrors()

        if (viewModel.instanceDomain.value.isNullOrBlank()) return displayError(
            binding.instanceContainer, R.string.error_field_required
        )

        val instanceUrl = viewModel.completeApiUrl

        if (instanceUrl.isWebUrl()) performLoginRequest()
        else simpleWarningDialog {
            val okTextId = android.R.string.ok
            text = getHtmlString(R.string.url_nok, "<em>${viewModel.instanceDomain.value!!}</em>", getString(okTextId))
            onOk(okTextId) {
                dismiss()
                this@LoginApiUrlFragment.performLoginRequest()
            }
            onCancel {
                binding.toCredentialsButton.idle()
                dismiss()
            }
        }
    }

    private fun performLoginRequest() = lifecycleScope.launch {
        +networkIdlingResource

        hideKeyboard()
        binding.toCredentialsButton.loading()

        val delayDeferred = async { delay(600) }
        val checkApiUrleferred = async { loginActivity.connectionWrapper.checkApiUrl() }

        listOf(delayDeferred, checkApiUrleferred).awaitAll()

        when (val result = checkApiUrleferred.await()) {
            ConnectionWrapper.ApiUrlValidationResult.Validated -> performAnimationThenNavigate()
            ConnectionWrapper.ApiUrlValidationResult.InvalidResponseError -> onInvalidResponse()
            is ConnectionWrapper.ApiUrlValidationResult.RootExposedWarning -> onRootExposedWarn()
            is ConnectionWrapper.ApiUrlValidationResult.UnknownError -> handleErrors(result.exception)
            ConnectionWrapper.ApiUrlValidationResult.InvalidUrlError -> onInvalidUrlError()
        }
    }

    private fun performAnimationThenNavigate() = lifecycleScope.launch {
        binding.toCredentialsButton.okAsync().await()
        -networkIdlingResource
        navigation.navigate(LoginApiUrlFragmentDirections.toCredentialFragment())
    }

    private fun onInvalidUrlError() {
        binding.toCredentialsButton.idle()
        binding.instanceContainer.error = getString(R.string.invalid_url_error_message)
        -networkIdlingResource
    }

    private fun handleErrors(error: Throwable) {
        binding.toCredentialsButton.idle()
        if (error is UnknownHostException) binding.instanceContainer.error = getString(R.string.unreachable_host)
        else errorDetailDialog(error)
        -networkIdlingResource
    }

    private fun onInvalidResponse() = simpleErrorDialog {
        text = getHtmlString(R.string.api_url_invalid_response, viewModel.completeApiUrl, API_URL_OK_RESPONSE)
        onOk {
            binding.toCredentialsButton.idle()
            dismiss()
        }
    }.let { -networkIdlingResource }

    private fun onRootExposedWarn() = simpleWarningDialog {
        text = getHtmlString(R.string.root_exposed_warning, getString(R.string.freshrss_documentation_url))
        onOk {
            dismiss()
            performAnimationThenNavigate()
        }
    }.let { -networkIdlingResource }

    companion object {
        //region Noisy code goes here
        private inline fun getSelectedItemListener(crossinline cb: () -> Unit) = object : OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) = Unit
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) = cb()
        }
        //endregion
    }
}
