package fr.chenry.android.freshrss.utils

import android.app.ActionBar.LayoutParams
import android.content.Context
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatImageView
import kotlin.math.max
import kotlin.math.min

class SquaredImageView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : AppCompatImageView(context, attrs, defStyleAttr) {
    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)

        if (layoutParams.height == LayoutParams.MATCH_PARENT && layoutParams.width == LayoutParams.MATCH_PARENT)
            return min(maxWidth, maxHeight).let { setMeasuredDimension(it, it) }

        if (layoutParams.height == LayoutParams.MATCH_PARENT)
            return setMeasuredDimension(measuredHeight, measuredHeight)

        if (layoutParams.width == LayoutParams.MATCH_PARENT) return setMeasuredDimension(measuredWidth, measuredWidth)

        return max(measuredWidth, measuredHeight).let { setMeasuredDimension(it, it) }
    }
}
