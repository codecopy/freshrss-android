package fr.chenry.android.freshrss.components.articles

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import android.widget.Toast.LENGTH_SHORT
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.map
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import eu.davidea.flexibleadapter.FlexibleAdapter
import eu.davidea.flexibleadapter.common.FlexibleItemDecoration
import fr.chenry.android.freshrss.FreshRSSApplication
import fr.chenry.android.freshrss.R
import fr.chenry.android.freshrss.components.feeds.FeedSection
import fr.chenry.android.freshrss.databinding.FragmentSubscriptionArticlesBinding
import fr.chenry.android.freshrss.store.Store
import fr.chenry.android.freshrss.store.database.models.Feed
import fr.chenry.android.freshrss.store.database.models.ReadStatus.READ
import fr.chenry.android.freshrss.store.database.models.ReadStatus.UNREAD
import fr.chenry.android.freshrss.store.viewmodels.ArticlesVM
import fr.chenry.android.freshrss.store.viewmodels.ArticlesViewModelProvider
import fr.chenry.android.freshrss.store.viewmodels.RefreshWorkInfosVM
import fr.chenry.android.freshrss.utils.e
import fr.chenry.android.freshrss.utils.isConnectedToNetwork
import fr.chenry.android.freshrss.utils.onFailure
import fr.chenry.android.freshrss.utils.unit
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.koin.android.ext.android.inject

class ArticlesFragment : Fragment() {
    private val args: ArticlesFragmentArgs by navArgs()
    private val feed: Feed by lazy { args.feed }
    private val section: FeedSection by lazy { args.section }
    private val workInfos by viewModels<RefreshWorkInfosVM>()

    private lateinit var binding: FragmentSubscriptionArticlesBinding

    private val model: ArticlesVM by lazy {
        ArticlesViewModelProvider(requireActivity(), feed, section)[ArticlesVM::class.java]
    }

    private val recyclerViewItems by lazy {
        model.sortedFeedsLiveData.map { articles ->
            articles.map { ArticleViewItem(requireContext(), lifecycleScope, feed, it) }
        }
    }

    private val fragmentStateLiveData by lazy {
        MediatorLiveData<Pair<Boolean, Boolean>>().apply {
            value = false to false
            addSource(model.sortedFeedsLiveData) {
                postValue(it.isEmpty() to value!!.second)
            }
            addSource(workInfos.isWorkerRunningLiveData) {
                postValue(value!!.first to it)
            }
        }
    }

    private val store by inject<Store>()

    // Tricky trap: using method references produces a different variable each time; which is expected provided
    // method references are just anonymous classes underhood. We need e clearly defined class property here
    // so we can correctly remove the LiveData observer
    private val observer = Observer(::initRecycler)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentSubscriptionArticlesBinding.inflate(inflater, container, false)
        binding.lifecycleOwner = viewLifecycleOwner

        binding.feedArticlesVisibility = fragmentStateLiveData.map { (isEmpty, _) -> !isEmpty }

        binding.feedWaitingScreenVisibility = fragmentStateLiveData.map { (isEmpty, isWorkerRuuning) ->
            isEmpty && isWorkerRuuning
        }

        binding.feedEmptyScreenVisibility = fragmentStateLiveData.map { (isEmpty, isWorkerRuuning) ->
            isEmpty && !isWorkerRuuning
        }

        when (section) {
            FeedSection.ALL -> R.drawable.ic_undraw_blogging
            FeedSection.UNREAD -> R.drawable.ic_undraw_all_read
            FeedSection.FAVORITES -> R.drawable.ic_undraw_favorite
        }.let(binding.fragmentFeedEmptyScreen::setImageResource)

        binding.fragmentFeedEmptyScreen.text = when (section) {
            FeedSection.ALL -> getString(R.string.empty_subscription_list, model.feed.title)
            FeedSection.UNREAD -> getString(R.string.empty_subscription_unread_list, model.feed.title)
            FeedSection.FAVORITES -> getString(R.string.empty_subscription_favorite_list, model.feed.title)
        }

        recyclerViewItems.observe(viewLifecycleOwner, observer)
        return binding.root
    }

    private fun initRecycler(firstValue: List<ArticleViewItem>): Unit = binding.fragmentFeedArticles.apply {
        recyclerViewItems.removeObserver(observer)

        val itemDecoration = FlexibleItemDecoration(requireContext())
            .withDefaultDivider()
            .withDrawOver(true)
            .withDrawDividerOnLastItem(true)

        addItemDecoration(itemDecoration)

        val articlesAdapter = ArticleAdapter(firstValue)
        layoutManager = LinearLayoutManager(context)
        adapter = articlesAdapter

        recyclerViewItems.observe(viewLifecycleOwner) {
            articlesAdapter.updateDataSet(it, true)
        }
    }.unit()

    private inner class ArticleAdapter(
        items: List<ArticleViewItem>
    ) : FlexibleAdapter<ArticleViewItem>(items, null, true),
        FlexibleAdapter.OnItemClickListener,
        FlexibleAdapter.OnItemSwipeListener {

        override fun getItemId(position: Int): Long {
            return getItem(position)?.article?.id?.hashCode()?.toLong() ?: RecyclerView.NO_ID
        }

        override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
            super.onAttachedToRecyclerView(recyclerView)
            isSwipeEnabled = true
            itemTouchHelperCallback.setSwipeFlags(ItemTouchHelper.RIGHT)

            addListener(this as OnItemClickListener)
            addListener(this as OnItemSwipeListener)
        }

        override fun onActionStateChanged(viewHolder: RecyclerView.ViewHolder?, actionState: Int) {}

        override fun onItemClick(view: View, position: Int): Boolean {
            val item = getArticleId(position) ?: return false
            val direction = ArticlesFragmentDirections.articlesToArticleDetail(item)
            findNavController().navigate(direction)
            return true
        }

        override fun onItemSwipe(position: Int, direction: Int) {
            if (direction == ItemTouchHelper.RIGHT) toggleArticleReadStatus(position)
        }

        private fun getArticleId(position: Int) = getItem(position)?.article?.id

        private fun toggleArticleReadStatus(position: Int) {
            val item = getItem(position) ?: return

            if (!requireContext().isConnectedToNetwork()) {
                Toast.makeText(context, R.string.no_internet_connection_avaible, Toast.LENGTH_LONG).show()
                return
            }

            if (item.article.readStatusRequestOnGoing) {
                Toast.makeText(context, R.string.request_already_ongoing, LENGTH_SHORT).show()
                return
            }

            item.article.readStatusRequestOnGoing = true

            val ctx = requireContext()
            val app = ctx.applicationContext as FreshRSSApplication
            // Using application scope so that any request survives destruction of the component
            app.applicationScope.launch {

                val expected = item.article.readStatus.toggle()
                store.postReadStatus(item.article, expected).onFailure { err ->
                    this@ArticleAdapter.e(err)
                    val message = when (expected) {
                        READ -> getString(R.string.read)
                        UNREAD -> getString(R.string.unread)
                    }.let { getString(R.string.mark_read_status_authorization, it) }

                    val toastText = ctx.getString(R.string.unable_to, message)

                    withContext(Dispatchers.Main) {
                        Toast.makeText(ctx, toastText, LENGTH_SHORT).show()
                    }
                }

                if (section == FeedSection.ALL) withContext(Dispatchers.Main) {
                    this@ArticleAdapter.notifyItemChanged(position)
                }

                item.article.readStatusRequestOnGoing = false
            }
        }
    }
}
