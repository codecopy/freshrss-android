package fr.chenry.android.freshrss.store.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.map
import androidx.work.WorkInfo
import androidx.work.WorkManager
import fr.chenry.android.freshrss.utils.getFinishedWorkLiveData
import fr.chenry.android.freshrss.utils.getPeriodicRefreshWorksLiveData
import fr.chenry.android.freshrss.utils.getRefreshWorksLiveData

class RefreshWorkInfosVM(app: Application) : AndroidViewModel(app) {
    private val workManager: WorkManager = WorkManager.getInstance(getApplication())

    private val refreshWorksLiveData = workManager.getRefreshWorksLiveData()
    val periodicRefreshWorksLiveData = workManager.getPeriodicRefreshWorksLiveData()
    val finishedWorkLiveData = workManager.getFinishedWorkLiveData()

    val isWorkerRunningLiveData: LiveData<Boolean> = refreshWorksLiveData.map {
        it.find { wi -> wi.state == WorkInfo.State.RUNNING } != null
    }
}
