package fr.chenry.android.freshrss

import android.app.Application
import android.app.NotificationChannel
import android.content.Context
import android.content.SharedPreferences
import android.os.Build
import androidx.annotation.StringRes
import androidx.annotation.VisibleForTesting
import androidx.core.app.NotificationManagerCompat
import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.ProcessLifecycleOwner
import androidx.room.Room
import androidx.work.ExistingPeriodicWorkPolicy
import androidx.work.ExistingWorkPolicy
import androidx.work.WorkInfo
import androidx.work.WorkManager
import androidx.work.await
import fr.chenry.android.freshrss.store.FreshRSSPreferences
import fr.chenry.android.freshrss.store.database.FreshRSSDabatabase
import fr.chenry.android.freshrss.store.database.FreshRSSDabatabase_Migrations
import fr.chenry.android.freshrss.utils.NotificationChannels
import fr.chenry.android.freshrss.utils.dispatchersModule
import fr.chenry.android.freshrss.utils.getPeriodicRefreshWorks
import fr.chenry.android.freshrss.utils.getRunningRefreshWork
import fr.chenry.android.freshrss.utils.getUserTimeZone
import fr.chenry.android.freshrss.utils.requireF
import fr.chenry.android.freshrss.utils.storeModule
import fr.chenry.android.freshrss.utils.unit
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.acra.config.httpSender
import org.acra.config.notification
import org.acra.data.StringFormat
import org.acra.ktx.initAcra
import org.acra.sender.HttpSender
import org.joda.time.DateTime
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.module.Module
import org.koin.dsl.module
import java.util.concurrent.TimeUnit

open class FreshRSSApplication : Application(), SharedPreferences.OnSharedPreferenceChangeListener {
    open val preferences by lazy { FreshRSSPreferences(this) }
    open val notificationManager by lazy { NotificationManagerCompat.from(this) }
    open val db by lazy {
        Room.databaseBuilder(this, FreshRSSDabatabase::class.java, DB_NAME)
            .addMigrations(*FreshRSSDabatabase_Migrations.build())
            .build()
    }

    open val applicationScope = MainScope()

    protected open val workManager by lazy { WorkManager.getInstance(this) }

    private val isPeriodicRefreshEnabled
        get() = preferences.refreshFrequency > 0

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        acraInit()
    }

    override fun onCreate() {
        super.onCreate()
        koinInit()

        ProcessLifecycleOwner.get().lifecycle.addObserver(NotificationCreator())

        preferences.apply {
            initDefaults()
            registerChangeListener(this@FreshRSSApplication)
            debugMode = getString(R.string.debug_active).toBooleanStrictOrNull() ?: false
        }
    }

    override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences, key: String?) {
        /* TODO: test changing the refresh frequency updates the refresh job
            Mais là, la flemme et de toutes façons, faut release */
        if (key == preferences.refreshFrequencyKey) applicationScope.launch {
            workManager.cancelAllWorkByTag(RefreshWorker.PERIODIC_WORK_TAG).await()
            if (isPeriodicRefreshEnabled) enqueuePeriodicWork(preferences.refreshFrequency)
        }
    }

    override fun onTerminate() {
        super.onTerminate()
        applicationScope.cancel()
    }

    open fun forceRefresh() = runBlocking {
        if (workManager.getRunningRefreshWork().isEmpty()) enqueueUniqueWork()
        if (
            workManager.getPeriodicRefreshWorks().none { it.state == WorkInfo.State.ENQUEUED }
        ) enqueuePeriodicWork(preferences.refreshFrequency)
    }

    open fun refresh() = runBlocking {
        if (workManager.getRunningRefreshWork().isEmpty()) {
            if (isPeriodicRefreshEnabled) enqueuePeriodicWork()
            else enqueueUniqueWork()
        }
    }

    open fun cancelOnGoingRefresh(): Unit = runBlocking {
        workManager.cancelAllWorkByTag(RefreshWorker.REFRESH_WORK_TAG).await()
    }.unit()

    open fun ensurePeriodicRequest() = applicationScope.launch {
        if (!isPeriodicRefreshEnabled) return@launch
        if (workManager.getRunningRefreshWork().isNotEmpty()) return@launch

        val account = db.getAccount()
        val isEmpty = requireF().workManager.getPeriodicRefreshWorks().isEmpty()
        val isExpired = account
            .lastFetchDate
            .plusMinutes(preferences.refreshFrequency)
            .isBefore(DateTime.now(getUserTimeZone()))

        if (isEmpty || isExpired) enqueuePeriodicWork()
    }

    // Allows to disengage ACRA in tests
    @VisibleForTesting
    open fun acraInit() = initAcra {
        reportFormat = StringFormat.JSON
        buildConfigClass = BuildConfig::class.java
        httpSender {
            uri = "https://android-report.christophe-henry.dev/report"
            basicAuthLogin = "3lJP3VyigIr9nxuF"
            basicAuthPassword = "pjhEv0b0rtIM7rrp"
            httpMethod = HttpSender.Method.POST
        }
        notification {
            channelName = getString(R.string.notification_channel_errors_reports_title)
            channelImportance = NotificationManagerCompat.IMPORTANCE_MAX
            text = getString(R.string.notification_crash_description)
            title = getString(R.string.notification_crash_title)
            sendButtonText = getString(android.R.string.ok)
            discardButtonText = getString(android.R.string.cancel)
        }
    }

    @VisibleForTesting
    open fun koinInit(vararg modules: Module) = startKoin {
        androidLogger()
        androidContext(this@FreshRSSApplication)
        modules(
            module {
                single { db }
                single { preferences }
            },
            storeModule,
            dispatchersModule,
            *modules
        )
        createEagerInstances()
    }.unit()

    private suspend fun enqueuePeriodicWork(initialDelay: Int = 0, initialDelayTimeUnit: TimeUnit = TimeUnit.MINUTES) =
        workManager.enqueueUniquePeriodicWork(
            RefreshWorker.PERIODIC_WORK_NAME,
            ExistingPeriodicWorkPolicy.REPLACE,
            RefreshWorker.periodicWorkRequest(
                preferences.refreshFrequency,
                TimeUnit.MINUTES,
                initialDelay,
                initialDelayTimeUnit
            )
        ).await()

    private suspend fun enqueueUniqueWork() = workManager.enqueueUniqueWork(
        RefreshWorker.ONE_TIME_WORK_NAME,
        ExistingWorkPolicy.REPLACE,
        RefreshWorker.manualWorkRequest
    ).await()

    companion object {
        const val DB_NAME = "freshrss.db"
    }

    private inner class NotificationCreator : DefaultLifecycleObserver {
        override fun onStart(owner: LifecycleOwner) =
            notificationManager.createNotificationChannels(getNotificationChannels())

        private fun getNotificationChannels(): List<NotificationChannel> =
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) listOf()
            else NotificationChannels.values().map {
                NotificationChannel(it.name, getString(it.nameResourceId), it.importance.asAndroid()).apply {
                    description = getString(it.descriptionResourceId)
                    setSound(null, null)
                }
            }

        private fun getString(@StringRes resId: Int): String = this@FreshRSSApplication.getString(resId)
    }
}
