package fr.chenry.android.freshrss.utils

import android.content.Context
import androidx.core.app.NotificationManagerCompat
import com.google.auto.service.AutoService
import fr.chenry.android.freshrss.FreshRSSApplication
import org.acra.config.CoreConfiguration
import org.acra.config.ReportingAdministrator
import org.acra.data.CrashReportData

/**
 * This class will be autowired to Acra intialization.
 * Function shouldSendReport() is used to perform cleanups when application crashes
 */
@AutoService(ReportingAdministrator::class)
class CleanupReportingAdministrator : ReportingAdministrator {
    override fun shouldSendReport(
        context: Context,
        config: CoreConfiguration,
        crashReportData: CrashReportData
    ): Boolean {
        val app = context.applicationContext
        if (app is FreshRSSApplication) {
            app.cancelOnGoingRefresh()
            NotificationManagerCompat.from(app).cancelAll()
        }
        return super.shouldSendReport(context, config, crashReportData)
    }
}
