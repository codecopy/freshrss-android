package fr.chenry.android.freshrss.store.database.models

import android.graphics.Bitmap
import android.os.Parcelable
import androidx.room.Dao
import androidx.room.Entity
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.PrimaryKey
import androidx.room.Query
import androidx.room.Transaction
import fr.chenry.android.freshrss.store.api.models.FeedTagItem
import kotlinx.coroutines.flow.Flow
import kotlinx.parcelize.Parcelize

typealias FeedTags = List<FeedCategory>

const val FOLDER_TYPE = "folder"
const val TAG_TYPE = "tag"

@Parcelize
@Entity(tableName = "feed_tags")
data class FeedCategory(
    @PrimaryKey
    override val id: String,
    val label: String,
    val type: String,
    override val unreadCount: Int = 0
) : FeedLikeDisplayable, Parcelable {

    override val title: String get() = label
    override val imageBitmap: Bitmap? get() = null

    companion object {
        fun fromFeedTagItem(feedTagItem: FeedTagItem) = FeedCategory(
            id = feedTagItem.id,
            label = feedTagItem.label,
            type = feedTagItem.type!!,
            unreadCount = feedTagItem.unreadCount
        )
    }
}

val VoidCategory = FeedCategory("", "", "")

@Dao
interface FeedCategoriesDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(categories: FeedTags)

    @Query("DELETE FROM feed_tags WHERE id NOT IN (:ids)")
    suspend fun deleteAllAbsent(ids: List<String>)

    @Transaction
    suspend fun syncAll(categories: FeedTags) {
        insertAll(categories)
        deleteAllAbsent(categories.map { it.id })
    }

    @Query(
        """
        SELECT DISTINCT t.id, t.type, t.label, SUM(s.unreadCount) as unreadCount FROM feed_tags t
            INNER JOIN feed_to_tag_relation f ON f.tagId = t.id
            INNER JOIN subscriptions s ON s.id = f.feedId
        WHERE t.type ='$FOLDER_TYPE'
        GROUP BY t.id
        ORDER BY t.label COLLATE NOCASE ASC
        """
    )
    fun allFolders(): Flow<FeedTags>

    @Query(
        """
        SELECT DISTINCT t.id, t.type, t.label, SUM(s.unreadCount) as unreadCount FROM feed_tags t
            INNER JOIN feed_to_tag_relation f ON f.tagId = t.id
            INNER JOIN subscriptions s ON s.id = f.feedId
        WHERE t.type ='$FOLDER_TYPE'
        GROUP BY t.id
        ORDER BY t.label COLLATE NOCASE ASC
        """
    )
    suspend fun allCategories(): FeedTags

    @Query(
        """
        SELECT DISTINCT t.id, t.type, t.label, SUM(s.unreadCount) as unreadCount FROM feed_tags t
            INNER JOIN feed_to_tag_relation f ON f.tagId = t.id
            INNER JOIN subscriptions s ON s.id = f.feedId
        WHERE t.type ='$FOLDER_TYPE'
        AND s.unreadCount > 0
        GROUP BY t.id
        ORDER BY t.label COLLATE NOCASE ASC
        """
    )
    fun foldersWithUnreadItems(): Flow<FeedTags>

    @Query(
        """
        SELECT DISTINCT t.id, t.type, t.label, SUM(s.unreadCount) as unreadCount FROM feed_tags t
            INNER JOIN feed_to_tag_relation f ON f.tagId = t.id
            INNER JOIN subscriptions s ON s.id = f.feedId
            INNER JOIN articles a ON a.streamId = s.id
        WHERE t.type ='$FOLDER_TYPE'
        AND a.favorite = '$FAVORITE_DB_NAME'
        GROUP BY t.id
        ORDER BY t.label COLLATE NOCASE ASC
        """
    )
    fun foldersWithFavorites(): Flow<FeedTags>
}
