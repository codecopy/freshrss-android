package fr.chenry.android.freshrss.activities

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ObservableField
import androidx.lifecycle.lifecycleScope
import fr.chenry.android.freshrss.R
import fr.chenry.android.freshrss.databinding.ActivitySplashScreenBinding
import fr.chenry.android.freshrss.store.database.FreshRSSDabatabase
import fr.chenry.android.freshrss.store.database.models.VoidAccount
import fr.chenry.android.freshrss.utils.CustomDispatchers
import fr.chenry.android.freshrss.utils.getUserTimeZone
import fr.chenry.android.freshrss.utils.startNextActivity
import kotlinx.coroutines.delay
import kotlinx.coroutines.withContext
import org.joda.time.DateTime
import org.koin.android.ext.android.inject

/*
 Android's splash screen API is a huge pile of shit and even
 the workaround the documentation provides to disable the
 default splash screen doesn't work! So I'm just disabling
 the lint warning and keeping it like this. I've lost enough
 time on google's poor decisions.
 */

@SuppressLint("CustomSplashScreen")
class SplashScreenActivity : AppCompatActivity() {
    private val welcomeText = ObservableField("")
    private val dispatchers by inject<CustomDispatchers>()
    private val db by inject<FreshRSSDabatabase>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        DataBindingUtil.setContentView<ActivitySplashScreenBinding>(this, R.layout.activity_splash_screen).apply {
            welcomeText = this@SplashScreenActivity.welcomeText
        }

        lifecycleScope.launchWhenResumed { startNextActivityDelayed() }
    }

    private suspend fun startNextActivityDelayed() {
        val account = withContext(dispatchers.db) {
            db.getAccount()
        }

        val now = DateTime.now(getUserTimeZone())
        val compl = if (account == VoidAccount) "" else this@SplashScreenActivity.getString(
            R.string.greeting_user_complement,
            account.login
        )

        val text = when (DaySpan.of(now)) {
            DaySpan.MORNING -> this@SplashScreenActivity.getString(R.string.good_morning_user, compl)
            DaySpan.DAY -> this@SplashScreenActivity.getString(R.string.hello_user, compl)
            DaySpan.EVENING -> this@SplashScreenActivity.getString(R.string.good_evening_user, compl)
        }

        delay(100)
        welcomeText.set(text)
        delay(300)

        val klass = when (account) {
            VoidAccount -> LoginActivity::class.java
            else -> MainActivity::class.java
        }

        this@SplashScreenActivity.startNextActivity(klass)
    }

    enum class DaySpan {
        MORNING, DAY, EVENING;

        companion object {
            fun of(date: DateTime) = when {
                date.hourOfDay < 11 -> MORNING
                date.hourOfDay < 18 -> DAY
                else -> EVENING
            }
        }
    }
}
