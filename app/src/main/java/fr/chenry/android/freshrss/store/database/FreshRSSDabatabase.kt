package fr.chenry.android.freshrss.store.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import dev.matrix.roomigrant.GenerateRoomMigrations
import fr.chenry.android.freshrss.store.database.models.Account
import fr.chenry.android.freshrss.store.database.models.AccountLastFetchDateUpdate
import fr.chenry.android.freshrss.store.database.models.AccountsDAO
import fr.chenry.android.freshrss.store.database.models.Article
import fr.chenry.android.freshrss.store.database.models.ArticleFavoriteStatusUpdate
import fr.chenry.android.freshrss.store.database.models.ArticleReadStatusUpdate
import fr.chenry.android.freshrss.store.database.models.ArticleScrollPositionUpdate
import fr.chenry.android.freshrss.store.database.models.Articles
import fr.chenry.android.freshrss.store.database.models.ArticlesDAO
import fr.chenry.android.freshrss.store.database.models.FavoriteArticleId
import fr.chenry.android.freshrss.store.database.models.Feed
import fr.chenry.android.freshrss.store.database.models.FeedCategoriesDAO
import fr.chenry.android.freshrss.store.database.models.FeedCategory
import fr.chenry.android.freshrss.store.database.models.FeedDAO
import fr.chenry.android.freshrss.store.database.models.FeedTags
import fr.chenry.android.freshrss.store.database.models.FeedToTagRelation
import fr.chenry.android.freshrss.store.database.models.FeedToTagRelationDAO
import fr.chenry.android.freshrss.store.database.models.UnreadArticleId
import fr.chenry.android.freshrss.store.database.models.VoidAccount
import fr.chenry.android.freshrss.utils.CustomDispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.joinAll
import kotlinx.coroutines.withContext
import org.joda.time.DateTime
import org.joda.time.LocalDateTime
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

@Database(
    version = 10,
    entities = [
        Account::class, Article::class, UnreadArticleId::class,
        FavoriteArticleId::class, Feed::class,
        FeedCategory::class, FeedToTagRelation::class
    ]
)
@TypeConverters(Converters::class)
@GenerateRoomMigrations(ManualMigrations::class)
abstract class FreshRSSDabatabase : RoomDatabase(), KoinComponent {
    private val dispatchers by inject<CustomDispatchers>()

    protected abstract fun getAccountsDAO(): AccountsDAO
    protected abstract fun getArticlesDAO(): ArticlesDAO
    protected abstract fun getSubscriptionsDAO(): FeedDAO
    protected abstract fun getSubscriptionCategoriesDAO(): FeedCategoriesDAO
    protected abstract fun getFeedToTagRelationDAO(): FeedToTagRelationDAO

    // Accounts
    fun getAccountFlowable() = getAccountsDAO().getAccountFlowable()
    fun getAccountFlow(): Flow<Account> = getAccountsDAO().getAccountFlow().map { it ?: VoidAccount }
    suspend fun getAccount(): Account = getAccountsDAO().getAccount() ?: VoidAccount

    suspend fun insertAccount(account: Account) = getAccountsDAO().insert(account)
    suspend fun updateLastFetchDate(id: Int, date: DateTime) = withContext(dispatchers.db) {
        getAccountsDAO().updateLastFetchDate(AccountLastFetchDateUpdate(id, date))
    }

    suspend fun updateTotalUnreadCount(id: Int, unreadCount: Int) = withContext(dispatchers.db) {
        getAccountsDAO().updateUnreadCount(id, unreadCount)
    }

    suspend fun incrementTotalUnreadCount(id: Int) = withContext(dispatchers.db) {
        getAccountsDAO().incrementUnreadCount(id)
    }

    suspend fun decrementTotalUnreadCount(id: Int) = withContext(dispatchers.db) {
        getAccountsDAO().decrementUnreadCount(id)
    }

    // Articles
    fun getArticlesByStreamId(streamId: String) = getArticlesDAO().getByStreamId(streamId)

    fun getArticleById(id: String) = getArticlesDAO().getById(id)
    fun getArticleByStreamIdAndUnread(streamId: String) = getArticlesDAO().getByStreamIdAndUnread(streamId)
    fun getArticleByStreamIdAndFavorite(streamId: String) = getArticlesDAO().getByStreamIdAndFavorite(streamId)

    suspend fun insertArticles(articles: Articles) = withContext(dispatchers.db) {
        getArticlesDAO().insertAll(articles)
    }

    suspend fun forceInsertArticles(articles: Articles) = withContext(dispatchers.db) {
        getArticlesDAO().forceInsertAll(articles)
    }

    suspend fun updateArticleReadStatuses(readStatuses: List<ArticleReadStatusUpdate>) = withContext(dispatchers.db) {
        getArticlesDAO().updateReadStatuses(readStatuses)
    }

    suspend fun updateArticleFavoriteStatuses(favoriteStatuses: List<ArticleFavoriteStatusUpdate>) =
        getArticlesDAO().updateFavoriteStatuses(favoriteStatuses)

    suspend fun updateArticleScrollPosition(id: String, scrollPosition: Int) =
        getArticlesDAO().updateScrollPosition(ArticleScrollPositionUpdate(id, scrollPosition))

    suspend fun getArticlesMostRecentCrawled() = getArticlesDAO().mostRecentCrawledDate()

    suspend fun syncAllArticlesReadStatus(ids: List<String>) = withContext(dispatchers.db) {
        getArticlesDAO().syncAllReadStatuses(ids)
    }

    suspend fun syncAllArticlesFavoriteStatus(ids: List<String>) = withContext(dispatchers.db) {
        getArticlesDAO().syncAllFavoriteStatus(ids)
    }

    // Subscriptions
    suspend fun syncSubscriptions(subscriptions: Map<String, Feed>) = withContext(dispatchers.db) {
        val suppressTask = async {
            getSubscriptionsDAO().deleteAllAbsentsById(subscriptions.keys)
        }

        val idsInDB = getAllSubcriptionsIds()
        val (toUpdate, toInsert) = subscriptions.values.partition { it.id in idsInDB }

        val updateTask = async {
            getSubscriptionsDAO().updateValues(toUpdate.map { it.toFeedUpdate() })
        }

        val insertTask = async {
            getSubscriptionsDAO().insertAll(toInsert)
        }

        listOf(suppressTask, updateTask, insertTask).joinAll()
    }

    suspend fun updateSubscriptionCount(id: String, count: Int) = withContext(dispatchers.db) {
        getSubscriptionsDAO().updateCount(id, count)
    }

    suspend fun updateSubscriptionNewestArticleDate(id: String, newestArticleDate: LocalDateTime) =
        getSubscriptionsDAO().updateNewestArticleDate(id, newestArticleDate)

    fun incrementSubscriptionCount(id: String) = getSubscriptionsDAO().incrementCount(id)

    suspend fun decrementSubscriptionCount(id: String) = withContext(dispatchers.db) {
        getSubscriptionsDAO().decrementCount(id)
    }

    suspend fun updateFeed(feed: Feed) = withContext(dispatchers.db) {
        getSubscriptionsDAO().update(feed)
    }

    fun getSubcriptionsById(id: String) = getSubscriptionsDAO().byId(id)
    suspend fun getFeedTitleById(id: String) = getSubscriptionsDAO().titleById(id)
    fun getFeedsByCategory(feedCategory: FeedCategory) =
        getSubscriptionsDAO().byCategory(feedCategory.id)

    fun getFeedsByCategoryAndUnreadCount(feedCategory: FeedCategory) =
        getSubscriptionsDAO().byCategoryAndUnreadCount(feedCategory.id)

    fun getFeedsByCategoryAndFavorites(feedCategory: FeedCategory) =
        getSubscriptionsDAO().bySubscriptionAndFavorites(feedCategory.id)

    fun getAllFeeds() = getSubscriptionsDAO().getAll()
    fun getAllUnreadSubcriptions() = getSubscriptionsDAO().getAllUnread()
    fun getAllFeedsWithFavorites() = getSubscriptionsDAO().getAllWithFavorites()

    suspend fun getAllSubcriptionsIds() = getSubscriptionsDAO().getAllIds()
    suspend fun getFeedsWithImageToUpdate() = withContext(dispatchers.db) {
        getSubscriptionsDAO().withImageToUpdate()
    }

    // Subscription catagoeries
    suspend fun syncAllFeedCategories(feedTags: FeedTags) = withContext(dispatchers.db) {
        getSubscriptionCategoriesDAO().syncAll(feedTags)
    }

    fun getAllFolders() = getSubscriptionCategoriesDAO().allFolders()
    suspend fun allCategories() = getSubscriptionCategoriesDAO().allCategories()
    fun getFoldersWithUnreadItems() = getSubscriptionCategoriesDAO().foldersWithUnreadItems()
    fun getFoldersWithFavorites() = getSubscriptionCategoriesDAO().foldersWithFavorites()

    // Feed tag relation
    suspend fun syncFeedToTagRelationForFeed(feedId: String, tagIds: List<String>) = withContext(dispatchers.db) {
        getFeedToTagRelationDAO().syncForFeed(feedId, tagIds)
    }
}
