package fr.chenry.android.freshrss

import android.Manifest.permission.POST_NOTIFICATIONS
import android.app.Notification
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import android.util.TypedValue
import android.widget.Toast
import androidx.annotation.AttrRes
import androidx.annotation.StringRes
import androidx.core.app.ActivityCompat
import androidx.core.app.NotificationCompat
import androidx.core.content.PermissionChecker.PERMISSION_GRANTED
import androidx.work.Constraints
import androidx.work.CoroutineWorker
import androidx.work.Data
import androidx.work.ForegroundInfo
import androidx.work.NetworkType
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.PeriodicWorkRequestBuilder
import androidx.work.WorkerParameters
import androidx.work.workDataOf
import fr.chenry.android.freshrss.store.Store
import fr.chenry.android.freshrss.store.api.ALL_ITEMS_ID
import fr.chenry.android.freshrss.utils.NotificationChannels
import fr.chenry.android.freshrss.utils.NotificationHelper
import fr.chenry.android.freshrss.utils.RefreshErrorDetails
import fr.chenry.android.freshrss.utils.e
import fr.chenry.android.freshrss.utils.flatMap
import fr.chenry.android.freshrss.utils.getUserTimeZone
import fr.chenry.android.freshrss.utils.i
import fr.chenry.android.freshrss.utils.isConnectedToNetwork
import fr.chenry.android.freshrss.utils.map
import fr.chenry.android.freshrss.utils.onFailure
import fr.chenry.android.freshrss.utils.onSuccess
import fr.chenry.android.freshrss.utils.requireF
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.acra.ACRA
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormatterBuilder
import org.joda.time.format.ISODateTimeFormat
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import org.koin.java.KoinJavaComponent.inject
import java.util.concurrent.TimeUnit

class RefreshWorker(
    appContext: Context,
    workerParams: WorkerParameters
) : CoroutineWorker(appContext, workerParams), KoinComponent {
    private val noNetworkMsg = applicationContext.getString(R.string.no_internet_connection_avaible)
    private val refreshNotification =
        createNotification(
            NotificationChannels.REFRESH,
            R.string.notification_refresh_title,
            R.string.notification_refresh_description,
            R.attr.colorPrimary
        )
            .setCategory(Notification.CATEGORY_PROGRESS)
            .setProgress(0, 0, true)
            .setOngoing(true)
            .setPriority(NotificationCompat.PRIORITY_LOW)
            .setVisibility(NotificationCompat.VISIBILITY_SECRET)
            .build()

    private val foregroundInfo = ForegroundInfo(NotificationHelper.ONGOING_REFRESH_NOTIFICATION, refreshNotification)
    private val flags
        get() =
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) PendingIntent.FLAG_UPDATE_CURRENT
            else PendingIntent.FLAG_IMMUTABLE or PendingIntent.FLAG_UPDATE_CURRENT

    private val store by inject<Store>()

    override suspend fun doWork(): Result {
        cancellAllNotifications()

        val manual = inputData.getBoolean(MANUAL_WORK_TAG, false)
        if (!applicationContext.isConnectedToNetwork()) {
            if (manual) withContext(Dispatchers.Main) {
                Toast.makeText(applicationContext, noNetworkMsg, Toast.LENGTH_LONG).show()
            }

            return Result.failure()
        }

        setForeground(foregroundInfo)

        val result = store.getTags().flatMap {
            store.getSubscriptions()
        }.map {
            // We don't really care about the result of this operation
            requireF().applicationScope.launch { store.fetchImages() }
        }.flatMap {
            store.getUnreadCount()
        }.flatMap {
            store.getStreamContents(ALL_ITEMS_ID)
        }.flatMap {
            store.getUnreadItemIds()
        }.flatMap {
            store.getFavoriteItemIds()
        }

        cancellAllNotifications()

        val now = DateTime.now(getUserTimeZone())

        result
            .onSuccess { requireF().db.updateLastFetchDate(store.account.id, now) }
            .onFailure {
                when (it) {
                    is CancellationException -> this.i("Refreshing job was canceled")
                    else -> {
                        this.e(it)
                        if (
                            ActivityCompat.checkSelfPermission(
                                applicationContext,
                                POST_NOTIFICATIONS
                            ) == PERMISSION_GRANTED &&
                            ACRA.isInitialised
                        ) requireF().notificationManager.notify(
                            NotificationHelper.FAIL_REFRESH_NOTIFICATION,
                            failNotification(it)
                        )
                    }
                }
            }

        val dtFormatter = DateTimeFormatterBuilder()
            .append(ISODateTimeFormat.date())
            .appendLiteral(' ')
            .append(ISODateTimeFormat.hourMinute())
            .toFormatter()

        val outDataBuilder = Data.Builder().putString("Ran at", now.toString(dtFormatter))
        if (result.isFailure) outDataBuilder.putString(ERRROR, result.exceptionOrNull()!!.toString())

        return when {
            result.isSuccess -> Result.success(outDataBuilder.build())
            else -> Result.failure(outDataBuilder.build())
        }
    }

    private fun cancelRefreshNotification() =
        requireF().notificationManager.cancel(NotificationHelper.ONGOING_REFRESH_NOTIFICATION)

    private fun cancellAllNotifications() {
        cancelRefreshNotification()
        requireF().notificationManager.cancel(NotificationHelper.FAIL_REFRESH_NOTIFICATION)
    }

    private fun failNotification(exception: Throwable): Notification {
        val notificationBuilder = createNotification(
            NotificationChannels.ERRORS,
            R.string.notification_refresh_failed_title,
            R.string.notification_refresh_failed_description,
            R.attr.colorError
        ).setSmallIcon(R.drawable.rss_feed_red)

        val debugMode = requireF().preferences.debugMode

        if (debugMode) {
            val intent = Intent(applicationContext, RefreshErrorDetails::class.java).apply {
                flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                putExtra(RefreshErrorDetails.EXEPTION_EXTRA_KEY, exception)
            }
            val pendingIntent: PendingIntent = PendingIntent.getBroadcast(requireF(), REQUEST_CODE, intent, flags)

            val sendReportButtonTxt = applicationContext.getString(R.string.send_report)
            val detailMessage = applicationContext.getString(R.string.detail_error_message, sendReportButtonTxt)
            notificationBuilder
                .setContentText(detailMessage)
                .addAction(R.drawable.ic_send_24dp, sendReportButtonTxt, pendingIntent)
        }

        return notificationBuilder
            .setCategory(Notification.CATEGORY_ERROR)
            .setPriority(NotificationCompat.PRIORITY_HIGH)
            .setAutoCancel(true)
            .build()
    }

    private fun createNotification(
        chan: NotificationChannels,
        @StringRes title: Int,
        @StringRes text: Int,
        @AttrRes colorAttr: Int
    ) = NotificationCompat.Builder(applicationContext, chan.channelId).apply {
        setSmallIcon(R.drawable.rss_feed)

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
            val typedValue = TypedValue()
            val attrs = requireF().theme.obtainStyledAttributes(typedValue.data, intArrayOf(colorAttr))
            color = attrs.getColor(0, requireF().getColor(R.color.color_primary))
            // TODO: Activate when themes are supported
            // setColorized(true)
            attrs.recycle()
        }

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.N) {
            setContentTitle(applicationContext.getString(text))
            setSubText(applicationContext.getString(title))
        } else {
            setContentTitle(applicationContext.getString(title))
            setContentInfo(applicationContext.getString(text))
        }
    }

    companion object {
        const val REQUEST_CODE = 668360593
        const val REFRESH_WORK_TAG = "REFRESH_WORK"
        const val ONE_TIME_WORK_NAME = "ONE_TIME_REFRESH_WORK"
        const val PERIODIC_WORK_NAME = "PERIODIC_REFRESH_WORK"
        const val MANUAL_WORK_TAG = "MANUAL_WORK"
        const val PERIODIC_WORK_TAG = "PERIODIC_WORK"
        const val ERRROR = "error"

        private val constraints = Constraints.Builder()
            .setRequiresBatteryNotLow(true)
            .setRequiredNetworkType(NetworkType.CONNECTED)
            .build()

        val manualWorkRequest = OneTimeWorkRequestBuilder<RefreshWorker>()
            .setInputData(workDataOf(MANUAL_WORK_TAG to true))
            .setInitialDelay(0L, TimeUnit.SECONDS)
            .addTag(REFRESH_WORK_TAG)
            .addTag(MANUAL_WORK_TAG)
            .build()

        fun periodicWorkRequest(
            repeatInterval: Int,
            repeatIntervalTimeUnit: TimeUnit,
            initialDelay: Int = 0,
            initialDelayTimeUnit: TimeUnit = TimeUnit.MINUTES
        ) = PeriodicWorkRequestBuilder<RefreshWorker>(repeatInterval.toLong(), repeatIntervalTimeUnit)
            .setConstraints(constraints)
            .setInitialDelay(initialDelay.toLong(), initialDelayTimeUnit)
            .setInputData(workDataOf())
            .addTag(REFRESH_WORK_TAG)
            .addTag(PERIODIC_WORK_TAG)
            .build()
    }
}
