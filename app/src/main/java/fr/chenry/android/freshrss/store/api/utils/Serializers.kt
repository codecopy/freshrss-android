package fr.chenry.android.freshrss.store.api.utils

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.core.TreeNode
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.JsonDeserializer
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.deser.std.StringDeserializer
import com.fasterxml.jackson.databind.node.ArrayNode
import com.fasterxml.jackson.datatype.joda.JodaModule
import com.fasterxml.jackson.datatype.joda.deser.LocalDateTimeDeserializer
import com.fasterxml.jackson.module.kotlin.readValue
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import fr.chenry.android.freshrss.store.api.models.ContentItem
import fr.chenry.android.freshrss.store.api.models.ContentItems
import fr.chenry.android.freshrss.store.api.models.FeedTagItem
import fr.chenry.android.freshrss.store.api.models.SubscriptionApiItem
import fr.chenry.android.freshrss.store.database.models.Article
import fr.chenry.android.freshrss.store.database.models.TokenResponse
import fr.chenry.android.freshrss.utils.e
import fr.chenry.android.freshrss.utils.unescape
import okhttp3.ResponseBody
import org.joda.time.DateTimeZone
import org.joda.time.LocalDateTime
import org.json.JSONArray
import org.json.JSONObject
import retrofit2.Converter
import java.util.Properties

class HtmlEntitiesDeserializer : StringDeserializer() {
    override fun deserialize(p: JsonParser?, ctxt: DeserializationContext?) = super.deserialize(p, ctxt).unescape()
}

class ContentItemsDeserializer : JsonDeserializer<ContentItems>() {
    override fun deserialize(p: JsonParser, ctxt: DeserializationContext): ContentItems = runCatching {
        JACKSON_OBJECT_MAPPER.readTree<ArrayNode>(p).mapNotNull {
            runCatching { JACKSON_OBJECT_MAPPER.convertValue(it, ContentItem::class.java) }.onFailure(this::e)
                .getOrNull()
        }
    }.onFailure(this::e).getOrDefault(listOf())
}

class MicroSecTimestampDeserializer : LocalDateTimeDeserializer() {
    override fun deserialize(p: JsonParser, ctxt: DeserializationContext): LocalDateTime {
        val tz = if (_format.isTimezoneExplicit) _format.timeZone else DateTimeZone.forTimeZone(ctxt.timeZone)
        return LocalDateTime(p.valueAsString.toLongOrNull()?.div(1000) ?: 0, tz)
    }
}

class MilliSecTimestampDeserializer : LocalDateTimeDeserializer() {
    override fun deserialize(p: JsonParser, ctxt: DeserializationContext): LocalDateTime {
        val tz = if (_format.isTimezoneExplicit) _format.timeZone else DateTimeZone.forTimeZone(ctxt.timeZone)
        return LocalDateTime(p.valueAsString.toLongOrNull() ?: 0, tz)
    }
}

class PublishedDateDeserializer : LocalDateTimeDeserializer() {
    override fun deserialize(p: JsonParser, ctxt: DeserializationContext): LocalDateTime {
        val tz = if (_format.isTimezoneExplicit) _format.timeZone else DateTimeZone.forTimeZone(ctxt.timeZone)
        return LocalDateTime(p.valueAsLong * 1000L, tz)
    }
}

class FeedCategoriesDeserializer : JsonDeserializer<List<String>>() {
    override fun deserialize(p: JsonParser, ctxt: DeserializationContext): List<String> = runCatching {
        val jsonArray = JSONArray(p.codec.readTree<TreeNode>(p).toString())
        (0 until jsonArray.length()).mapNotNull {
            runCatching { jsonArray.optJSONObject(it).optString("id") }.onFailure(this::e).getOrNull()
        }
    }.onFailure(this::e).getOrDefault(listOf())
}

object TagsConverter : Converter<ResponseBody, List<FeedTagItem>> {
    override fun convert(value: ResponseBody): List<FeedTagItem> = kotlin.runCatching {
        value.use {
            JSONObject(value.string()).optJSONArray("tags")?.let { jsonArray ->
                (0 until (jsonArray.length())).mapNotNull {
                    val result = JACKSON_OBJECT_MAPPER.readValue<FeedTagItem>(jsonArray.optJSONObject(it).toString())
                    if (result.type == null) null else result
                }
            } ?: listOf()
        }
    }.onFailure(this::e).getOrDefault(listOf())
}

object SubscriptionApiItemsConverter : Converter<ResponseBody, List<SubscriptionApiItem>> {
    override fun convert(value: ResponseBody): List<SubscriptionApiItem> = runCatching {
        value.use {
            val jsonArray = JSONObject(value.string()).optJSONArray("subscriptions")
            JACKSON_OBJECT_MAPPER.readValue<List<SubscriptionApiItem>>(jsonArray?.toString() ?: "[]")
        }
    }.onFailure(this::e).getOrDefault(listOf())
}

object UnreadItemIdsConverter : Converter<ResponseBody, List<String>> {
    override fun convert(value: ResponseBody): List<String> = runCatching {
        value.use {
            val jsonArray: JSONArray? = JSONObject(value.string()).optJSONArray("itemRefs")
            jsonArray?.let { array ->
                (0 until array.length()).mapNotNull {
                    runCatching {
                        val id = jsonArray.optJSONObject(it).optString("id")
                        // Ids returned by this endpoint are in long form.
                        // They must be converted in hex (short form) before being used.
                        // See https://feedhq.readthedocs.io/en/latest/api/terminology.html#items
                        "${Article.ARTICLE_ID_PREFIX}${id.toLong().toString(16).padStart(id.length, '0')}"
                    }.onFailure(this::e).getOrNull()
                }
            } ?: listOf()
        }
    }.onFailure(this::e).getOrDefault(listOf())
}

object TokenResponseConverter : Converter<ResponseBody, TokenResponse> {
    override fun convert(value: ResponseBody): TokenResponse = value.use {
        val properties = Properties().apply { load(value.byteStream()) }
        TokenResponse(
            SID = properties.getProperty("SID", ""),
            Auth = properties.getProperty("Auth", "")
        )
    }
}

val JACKSON_OBJECT_MAPPER: ObjectMapper = ObjectMapper()
    .registerKotlinModule()
    .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
    .registerModule(JodaModule())
