package fr.chenry.android.freshrss.components.login

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Transformations
import androidx.lifecycle.lifecycleScope
import fr.chenry.android.freshrss.R
import fr.chenry.android.freshrss.activities.LoginActivity
import fr.chenry.android.freshrss.activities.MainActivity
import fr.chenry.android.freshrss.databinding.FragmentLoginLoaderBinding
import fr.chenry.android.freshrss.store.api.ConnectionWrapper.LoginResult
import fr.chenry.android.freshrss.store.api.GOOGLE_API_ENDPOINT
import fr.chenry.android.freshrss.store.viewmodels.LoginVM
import fr.chenry.android.freshrss.testutils.EspressoIdlingResource.networkIdlingResource
import fr.chenry.android.freshrss.testutils.EspressoIdlingResource.unaryMinus
import fr.chenry.android.freshrss.testutils.EspressoIdlingResource.unaryPlus
import fr.chenry.android.freshrss.utils.requireF
import fr.chenry.android.freshrss.utils.startNextActivity
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.delay
import kotlinx.coroutines.withContext

class LoginLoaderFragment : Fragment(), LoginActivity.ChildFragment {
    private val viewModel by activityViewModels<LoginVM>()
    private val loginActivity get() = requireActivity() as LoginActivity

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View =
        FragmentLoginLoaderBinding.inflate(inflater, container, false).apply {
            lifecycleOwner = viewLifecycleOwner
            progressText = Transformations.map(viewModel.completeApiUrlWithStyle) {
                getString(R.string.login_progress_text, it.toString().replace("/$GOOGLE_API_ENDPOINT", ""))
            }
        }.root

    override fun onStart() {
        super.onStart()
        lifecycleScope.launchWhenStarted {
            +networkIdlingResource
            val resultAsync = async(Dispatchers.IO) { loginActivity.connectionWrapper.attemptLogin() }
            val delayAsync = async { delay(400) }

            listOf(resultAsync, delayAsync).awaitAll()
            val result = resultAsync.await()

            if (result is LoginResult.Ok) {
                withContext(Dispatchers.IO) { requireF().db.insertAccount(result.account) }
                requireF().refresh()
                delay(400)
                -networkIdlingResource
                requireActivity().startNextActivity(MainActivity::class.java)
            } else {
                viewModel.loginResult.push(result)
                -networkIdlingResource
                loginActivity.navigation.navigateUp()
            }
        }
    }

    override fun canGoBack(): Boolean = false
    override fun onSupportNavigateUp(): Boolean = false
}
