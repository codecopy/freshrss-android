package fr.chenry.android.freshrss.utils

import android.content.Context
import android.util.AttributeSet
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.TextView
import androidx.databinding.BindingAdapter
import fr.chenry.android.freshrss.R

class Loader @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : LinearLayout(context, attrs, defStyle) {
    var text: CharSequence
        get() = textView.text
        set(value) {
            textView.text = value
        }

    private val progressBar: ProgressBar
    private val textView: TextView

    init {
        val a = context.obtainStyledAttributes(attrs, R.styleable.Loader, defStyle, 0)
        val loaderText = a.getString(R.styleable.Loader_loaderText)
        val padding = a.getDimensionPixelSize(R.styleable.Loader_loaderPadding, 0)
        a.recycle()

        orientation = VERTICAL
        gravity = Gravity.CENTER
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        inflater.inflate(R.layout.loader, this, true)

        progressBar = getChildAt(0) as ProgressBar
        textView = getChildAt(1) as TextView

        textView.visibility = if (loaderText.isNullOrBlank()) View.GONE else View.VISIBLE
        textView.text = loaderText

        setPadding(padding, padding, padding, padding)
    }

    companion object {
        @JvmStatic
        @BindingAdapter("loaderText")
        fun setLoaderText(loader: Loader, resource: CharSequence?) = resource?.let(loader::text.setter)
    }
}
