package fr.chenry.android.freshrss.components.login

import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import com.google.android.material.textfield.TextInputLayout
import fr.chenry.android.freshrss.activities.LoginActivity
import fr.chenry.android.freshrss.store.viewmodels.LoginVM
import fr.chenry.android.freshrss.utils.unit
import fr.chenry.android.freshrss.utils.windowToken

abstract class LoginWrappedFragment<T : ViewDataBinding> : Fragment(), View.OnClickListener {
    protected lateinit var binding: T

    protected abstract val focusField: EditText
    protected abstract val imeNextField: EditText
    protected abstract val nextButton: Button
    protected abstract val resetFields: Array<out TextInputLayout>

    protected val loginActivity inline get() = requireActivity() as LoginActivity
    protected val navigation get() = requireView().findNavController()
    protected val viewModel by activityViewModels<LoginVM>()

    abstract fun nextAction()
    abstract fun inflateBinding(inflater: LayoutInflater, container: ViewGroup?): T

    private val inputManager by lazy {
        ContextCompat.getSystemService(
            requireContext(),
            InputMethodManager::class.java
        )
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = inflateBinding(inflater, container).apply { lifecycleOwner = viewLifecycleOwner }
        imeNextField.setOnEditorActionListener(ImeActionDone(::nextAction))
        nextButton.setOnClickListener(this)
        return binding.root
    }

    override fun onResume() {
        super.onResume()
        (requireActivity().currentFocus ?: focusField).requestFocus()
        showKeyboard()
    }

    override fun onClick(v: View) = nextAction()

    protected fun hideKeyboard() = windowToken?.let { token ->
        inputManager?.hideSoftInputFromWindow(token, 0, null)
    }.unit()

    protected fun showKeyboard() = inputManager?.showSoftInput(focusField, 0, null)

    protected fun displayError(textInputLayout: TextInputLayout, @StringRes res: Int) = textInputLayout.run {
        error = getString(res)
        requestFocus().unit()
    }

    protected fun resetErrors() = resetFields.forEach {
        it.error = null
        it.clearFocus()
    }

    internal class ImeActionDone(private inline val cb: () -> Unit) : TextView.OnEditorActionListener {
        override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean =
            if (actionId != EditorInfo.IME_ACTION_DONE && actionId != EditorInfo.IME_NULL) false
            else cb().let { true }
    }
}
