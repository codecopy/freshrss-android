package fr.chenry.android.freshrss.store.database.models

import android.net.Uri
import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.PrimaryKey
import androidx.room.Query
import androidx.room.Transaction
import androidx.room.Update
import fr.chenry.android.freshrss.BR
import fr.chenry.android.freshrss.store.api.FAVORITE_ITEMS_ID
import fr.chenry.android.freshrss.store.api.READ_ITEMS_ID
import fr.chenry.android.freshrss.store.api.models.ContentItem
import fr.chenry.android.freshrss.store.api.models.Href
import fr.chenry.android.freshrss.store.database.DbSerializable
import fr.chenry.android.freshrss.utils.firstOrElse
import org.joda.time.LocalDateTime
import java.util.concurrent.atomic.AtomicBoolean

/*
 * NOTE: the managment of read/unread and favorite/not favorite here is utterly
 * complex. This is because, when the number of unread article is long, it is
 * not possible to sync their status in a single SQL query.
 * See: https://git.feneas.org/christophehenry/freshrss-android/-/issues/100
 */

typealias Articles = List<Article>

@Entity(tableName = "unread_article_ids")
data class UnreadArticleId(@PrimaryKey val id: String)

@Entity(tableName = "favorite_article_ids")
data class FavoriteArticleId(@PrimaryKey val id: String)

@Entity(tableName = "articles")
data class Article(
    @PrimaryKey
    val id: String,
    val title: String,
    val href: String,
    val author: String,
    val content: String,
    val streamId: String,
    val readStatus: ReadStatus = ReadStatus.READ,
    val favorite: FavoriteStatus = FavoriteStatus.NOT_FAVORITE,
    val crawled: LocalDateTime = LocalDateTime(0),
    val published: LocalDateTime = LocalDateTime(0),
    val scrollPosition: Int = 0
) : BaseObservable() {

    @Ignore
    val url: Uri = Uri.parse(href)

    @Ignore
    private val mReadStatusRequestOnGoing = AtomicBoolean(false)

    @get:Bindable
    var readStatusRequestOnGoing
        get() = mReadStatusRequestOnGoing.get()
        set(value) {
            mReadStatusRequestOnGoing.set(value)
            notifyPropertyChanged(BR.readStatusRequestOnGoing)
        }

    @Ignore
    private val mFavoriteRequestOnGoing = AtomicBoolean(false)

    @get:Bindable
    var favoriteRequestOnGoing
        get() = mFavoriteRequestOnGoing.get()
        set(value) {
            mFavoriteRequestOnGoing.set(value)
            notifyPropertyChanged(BR.favoriteRequestOnGoing)
        }

    companion object {
        /** Prefix to append to article ids when dealing with ids returned by
         * [fr.chenry.android.freshrss.store.Store.getUnreadItemIds] */
        const val ARTICLE_ID_PREFIX = "tag:google.com,2005:reader/item/"

        fun fromContentItem(item: ContentItem) = Article(
            item.id,
            item.title,
            item.alternate.firstOrElse(Href("")).href,
            item.author,
            item.summary.content,
            item.origin.streamId,
            crawled = item.crawled,
            published = item.published
        )
    }
}

data class ArticleReadStatusUpdate(
    val id: String,
    val readStatus: ReadStatus
)

data class ArticleFavoriteStatusUpdate(
    val id: String,
    val favorite: FavoriteStatus
)

data class ArticleScrollPositionUpdate(
    val id: String,
    val scrollPosition: Int
)

const val READ_DB_NAME = "READ"
const val UNREAD_DB_NAME = "UNREAD"

enum class ReadStatus(override val dbName: String) : DbSerializable {
    READ(READ_DB_NAME), UNREAD(UNREAD_DB_NAME);

    fun toggle() = when (this) {
        READ -> UNREAD
        UNREAD -> READ
    }

    fun toPostFormData(articleId: String) = when (this) {
        READ -> mapOf("i" to articleId, "a" to READ_ITEMS_ID)
        UNREAD -> mapOf("i" to articleId, "r" to READ_ITEMS_ID)
    }

    companion object {

        fun dbValueOf(value: String?) = when (value) {
            READ_DB_NAME -> READ
            UNREAD_DB_NAME -> UNREAD
            else -> READ
        }
    }
}

const val FAVORITE_DB_NAME = "FAVORITE"
const val NOT_FAVORITE_DB_NAME = "NOT_FAVORITE"

enum class FavoriteStatus(override val dbName: String) : DbSerializable {
    FAVORITE(FAVORITE_DB_NAME), NOT_FAVORITE(NOT_FAVORITE_DB_NAME);

    fun toggle() = when (this) {
        FAVORITE -> NOT_FAVORITE
        NOT_FAVORITE -> FAVORITE
    }

    fun toPostFormData(articleId: String) = when (this) {
        FAVORITE -> mapOf("i" to articleId, "a" to FAVORITE_ITEMS_ID)
        NOT_FAVORITE -> mapOf("i" to articleId, "r" to FAVORITE_ITEMS_ID)
    }

    companion object {

        fun dbValueOf(value: String?) = when (value) {
            FAVORITE_DB_NAME -> FAVORITE
            NOT_FAVORITE_DB_NAME -> NOT_FAVORITE
            else -> NOT_FAVORITE
        }
    }
}

@Dao
interface ArticlesDAO {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertAll(articles: Articles)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun forceInsertAll(articles: Articles)

    @Update(entity = Article::class)
    suspend fun updateReadStatuses(readStatuses: List<ArticleReadStatusUpdate>)

    @Update(entity = Article::class)
    suspend fun updateFavoriteStatuses(readStatuses: List<ArticleFavoriteStatusUpdate>)

    @Update(entity = Article::class)
    suspend fun updateScrollPosition(articleScrollPositionUpdate: ArticleScrollPositionUpdate)

    @Query("UPDATE articles SET readStatus = '$READ_DB_NAME'")
    suspend fun markAllArticlesRead(): Int

    @Query("UPDATE articles SET favorite = '$FAVORITE_DB_NAME' WHERE id IN (:ids)")
    suspend fun markArticlesFavoriteForIds(ids: List<String>): Int

    @Query("UPDATE articles SET favorite = '$NOT_FAVORITE_DB_NAME' WHERE id NOT IN (:ids)")
    suspend fun markArtclesNotFavoriteExcludingIds(ids: List<String>): Int

    @Query("UPDATE articles SET favorite = '$NOT_FAVORITE_DB_NAME'")
    suspend fun markArtclesNotFavorite(): Int

    suspend fun syncAllReadStatuses(ids: List<String>) {
        val unreadIds = ids.map(::UnreadArticleId)
        emptyUnreadTableAndReload(unreadIds)
        syncReadAndUnreadArticles()
    }

    suspend fun syncAllFavoriteStatus(ids: List<String>) {
        val unreadIds = ids.map(::FavoriteArticleId)
        emptyFavoriteTableAndReload(unreadIds)
        syncFavoriteAndNotFavoriteArticles()
    }

    @Query("SELECT * FROM articles WHERE streamId = :streamId")
    fun getByStreamId(streamId: String): LiveData<Articles>

    @Query("SELECT * FROM articles WHERE id = :id LIMIT 1")
    fun getById(id: String): LiveData<Article>

    @Query("SELECT * FROM articles WHERE streamId = :streamId AND readStatus = '$UNREAD_DB_NAME'")
    fun getByStreamIdAndUnread(streamId: String): LiveData<Articles>

    @Query("SELECT * FROM articles WHERE streamId = :streamId AND favorite = '$FAVORITE_DB_NAME'")
    fun getByStreamIdAndFavorite(streamId: String): LiveData<Articles>

    @Query("SELECT max(crawled) FROM articles")
    suspend fun mostRecentCrawledDate(): LocalDateTime?

    //region Favorites and unread support
    @Query("DELETE FROM unread_article_ids")
    suspend fun nukeUnreadTable()

    @Insert
    suspend fun insertUneadArticleIds(ids: List<UnreadArticleId>)

    @Query(
        """
        UPDATE articles SET readStatus = '$UNREAD_DB_NAME'
        WHERE id IN (
            SELECT id FROM unread_article_ids
        )
        """
    )
    suspend fun syncUneadArticles(): Int

    @Query(
        """
        UPDATE articles SET readStatus = '$READ_DB_NAME'
        WHERE id NOT IN (
            SELECT id FROM unread_article_ids
        )
        """
    )
    suspend fun syncReadArticles(): Int

    @Transaction
    suspend fun emptyUnreadTableAndReload(ids: List<UnreadArticleId>) {
        nukeUnreadTable()
        insertUneadArticleIds(ids)
    }

    @Transaction
    suspend fun syncReadAndUnreadArticles() {
        syncUneadArticles()
        syncReadArticles()
    }

    @Query("DELETE FROM favorite_article_ids")
    suspend fun nukeFavoriteTable()

    @Insert
    suspend fun insertFavoriteArticleIds(ids: List<FavoriteArticleId>)

    @Query(
        """
        UPDATE articles SET favorite = '$FAVORITE_DB_NAME'
        WHERE id IN (
            SELECT id FROM favorite_article_ids
        )
        """
    )
    suspend fun syncFavoriteArticles(): Int

    @Query(
        """
        UPDATE articles SET favorite = '$NOT_FAVORITE_DB_NAME'
        WHERE id NOT IN (
            SELECT id FROM favorite_article_ids
        )
        """
    )
    suspend fun syncNotFavoriteArticles(): Int

    @Transaction
    suspend fun emptyFavoriteTableAndReload(ids: List<FavoriteArticleId>) {
        nukeFavoriteTable()
        insertFavoriteArticleIds(ids)
    }

    @Transaction
    suspend fun syncFavoriteAndNotFavoriteArticles() {
        syncFavoriteArticles()
        syncNotFavoriteArticles()
    }
    //endregion
}
