@file:Suppress("unused")

package fr.chenry.android.freshrss.utils

import android.app.Activity
import android.app.ActivityOptions
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.net.Uri
import android.os.Build
import android.os.IBinder
import android.util.Log
import android.util.Patterns
import android.view.View
import android.webkit.URLUtil
import androidx.annotation.StringRes
import androidx.core.app.ActivityCompat
import androidx.core.text.HtmlCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.map
import androidx.navigation.NavController
import androidx.work.WorkInfo
import androidx.work.WorkManager
import fr.chenry.android.freshrss.FreshRSSApplication
import fr.chenry.android.freshrss.R
import fr.chenry.android.freshrss.RefreshWorker
import org.apache.commons.text.StringEscapeUtils
import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import org.joda.time.Duration
import org.joda.time.LocalDateTime
import org.joda.time.Period
import java.net.URL
import java.util.Locale
import java.util.TimeZone

@Suppress("NOTHING_TO_INLINE", "FunctionName")
inline fun NOOP(): Unit = Unit

@Suppress("NOTHING_TO_INLINE")
inline fun Any?.unit() = NOOP()

fun Any.v(message: String) = Log.v(this::class.qualifiedName, message).unit()
fun Any.v(message: Throwable) = Log.v(this::class.qualifiedName, "VERBOSE", message).unit()
fun Any.d(message: String) = Log.d(this::class.qualifiedName, message).unit()
fun Any.d(message: Throwable) = Log.d(this::class.qualifiedName, "DEBUG", message).unit()
fun Any.i(message: String) = Log.i(this::class.qualifiedName, message).unit()
fun Any.i(message: Throwable) = Log.i(this::class.qualifiedName, "ERROR", message).unit()
fun Any.w(message: String) = Log.w(this::class.qualifiedName, message).unit()
fun Any.w(message: Throwable) = Log.w(this::class.qualifiedName, "WARNNG", message).unit()
fun Any.e(message: String) = Log.e(this::class.qualifiedName, message).unit()
fun Any.e(message: Throwable) = Log.e(this::class.qualifiedName, "ERROR", message).unit()
fun Any.wtf(message: String) = Log.wtf(this::class.qualifiedName, message).unit()
fun Any.wtf(message: Throwable) = Log.wtf(this::class.qualifiedName, "WTF", message).unit()

fun String.addTrailingSlash() = if (this.endsWith("/")) this else "$this/"
fun String.escape() = StringEscapeUtils.escapeHtml4(this).orEmpty()

// See https://github.com/FreshRSS/FreshRSS/issues/2770#issuecomment-575208803
fun String?.unescape() = StringEscapeUtils.unescapeHtml4(this).orEmpty().let { s ->
    var res = s
    listOf('’' to '\'', '＂' to '"', '＾' to '^', '？' to '?', '＼' to '\\', '／' to '/', '，' to ',', '；' to ';')
        .forEach { res = res.replace(it.first, it.second) }
    res
}

fun String?.extractURLs(): List<Uri> = if (this.isNullOrBlank()) listOf() else {
    this.split("\\s+").mapNotNull {
        if (it.isWebUrl()) it.asUri() else null
    }
}

fun Context.isConnectedToNetwork(): Boolean {
    val connectivityManager = this.getSystemService(Context.CONNECTIVITY_SERVICE) as? ConnectivityManager
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        val nw = connectivityManager?.activeNetwork ?: return false
        val actNw = connectivityManager.getNetworkCapabilities(nw) ?: return false
        return when {
            actNw.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
            actNw.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
            actNw.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
            actNw.hasTransport(NetworkCapabilities.TRANSPORT_BLUETOOTH) -> true
            else -> false
        }
    } else {
        @Suppress("DEPRECATION")
        val nwInfo = connectivityManager?.activeNetworkInfo ?: return false
        @Suppress("DEPRECATION")
        return nwInfo.isConnected
    }
}

fun URL.queryParameters(): Map<String, String> = this.query
    ?.split("&")
    ?.map { it.split("=").let { split -> split[0] to split[1] } }
    ?.toMap() ?: mapOf()

fun <T> List<T>.firstOrElse(other: T): T {
    return if (isEmpty()) other else this[0]
}

fun NavController.isTopLevelDestination() = this.currentDestination?.id == this.graph.startDestinationId

fun getUserTimeZone(): DateTimeZone = DateTimeZone.forTimeZone(TimeZone.getDefault())

@Suppress("DEPRECATION")
val Context.userLocale: Locale
    get() =
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) this.resources.configuration.locales[0]
        else this.resources.configuration.locale

fun Activity.requireF() = this.application as FreshRSSApplication
fun Fragment.requireF() = this.requireActivity().application as FreshRSSApplication
fun AndroidViewModel.requireF() = this.getApplication<FreshRSSApplication>()
fun RefreshWorker.requireF() = this.applicationContext as FreshRSSApplication
fun Context.requireF() = this.applicationContext as FreshRSSApplication

val Activity.windowToken: IBinder?
    inline get() = this.let {
        it.currentFocus ?: it.findViewById<View>(android.R.id.content).rootView
    }.windowToken

fun Activity.startNextActivity(nextActivity: Class<out Activity>) = runOnUiThread {
    val anim = ActivityOptions
        .makeCustomAnimation(this, R.anim.slide_in_right, R.anim.slide_out_left)
        .toBundle()

    ActivityCompat.startActivity(this, Intent(this, nextActivity), anim)
    finish()
}

val Fragment.windowToken: IBinder? inline get() = requireActivity().windowToken

fun Fragment.getHtmlString(
    @StringRes resId: Int,
    vararg formatArgs: Any?,
) = HtmlCompat.fromHtml(this.getString(resId, *formatArgs), HtmlCompat.FROM_HTML_MODE_LEGACY)

fun WorkManager.getRefreshWorksLiveData() = this.getWorkInfosByTagLiveData(RefreshWorker.REFRESH_WORK_TAG)
fun WorkManager.getPeriodicRefreshWorksLiveData() = this.getWorkInfosByTagLiveData(RefreshWorker.PERIODIC_WORK_TAG)
fun WorkManager.getFinishedWorkLiveData() = this.getWorkInfosByTagLiveData(RefreshWorker.REFRESH_WORK_TAG).map {
    it.filter(WorkInfo::isFinished)
}

fun WorkManager.getRefreshWork(): List<WorkInfo> = this.getWorkInfosByTag(RefreshWorker.REFRESH_WORK_TAG).get()

fun WorkManager.getRunningRefreshWork(): List<WorkInfo> = this.getRefreshWork().filter {
    it.state == WorkInfo.State.RUNNING
}

fun WorkManager.getPeriodicRefreshWorks(): List<WorkInfo> = this.getWorkInfosByTag(
    RefreshWorker.PERIODIC_WORK_TAG
).get()

val WorkInfo.isFinished inline get() = this.state == WorkInfo.State.FAILED || this.state == WorkInfo.State.SUCCEEDED

fun DateTime.period(other: DateTime): Period =
    Period(this, other).withMillis(0).let {
        when {
            it.days > 0 -> return@let it.withMinutes(0).withSeconds(0)
            it.hours > 0 -> return@let it.withSeconds(0)
            it.minutes > 0 -> return@let it.withSeconds(0)
            else -> return@let it
        }
    }

val LocalDateTime.unixEpochSeconds get() = Duration.millis(this.toDateTime(DateTimeZone.UTC).millis).standardSeconds
val LocalDateTime.unixEpochMillis get() = this.toDateTime(DateTimeZone.UTC).millis

fun String?.isWebUrl(): Boolean {
    if (this == null) return false

    val trimmed = this.trim()
    val isWebUrl = Patterns.WEB_URL.matcher(trimmed).matches()
    val isHttp = URLUtil.isHttpUrl(trimmed) || URLUtil.isHttpsUrl(trimmed)

    if (!isWebUrl) return runCatching {
        trimmed.asURL().host == "localhost"
    }.getOrDefault(false)

    return isHttp
}

fun String.asUri(): Uri = Uri.parse(this.trim())
fun String.asURL(): URL = URL(this.trim())
