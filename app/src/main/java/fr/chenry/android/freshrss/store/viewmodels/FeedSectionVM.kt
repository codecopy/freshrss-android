package fr.chenry.android.freshrss.store.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import fr.chenry.android.freshrss.components.feeds.FeedSection

class FeedSectionVM : ViewModel() {
    val section = MutableLiveData(FeedSection.ALL)
}
