package fr.chenry.android.freshrss.store.api

import fr.chenry.android.freshrss.store.api.models.ContentItemsHandler
import fr.chenry.android.freshrss.store.api.models.FeedTagItem
import fr.chenry.android.freshrss.store.api.models.SubscriptionApiItem
import fr.chenry.android.freshrss.store.api.models.UnreadCountsHandler
import fr.chenry.android.freshrss.store.api.utils.APIConverter
import fr.chenry.android.freshrss.store.api.utils.APIInterceptor
import fr.chenry.android.freshrss.store.api.utils.AuthorisationRequired
import fr.chenry.android.freshrss.store.api.utils.JACKSON_OBJECT_MAPPER
import fr.chenry.android.freshrss.store.api.utils.JsonOutput
import fr.chenry.android.freshrss.store.api.utils.SubscriptionApiItemsConverter
import fr.chenry.android.freshrss.store.api.utils.TagsConverter
import fr.chenry.android.freshrss.store.api.utils.TokenRequired
import fr.chenry.android.freshrss.store.api.utils.TokenResponseConverter
import fr.chenry.android.freshrss.store.api.utils.UnreadItemIdsConverter
import fr.chenry.android.freshrss.store.api.utils.UseDeserializer
import fr.chenry.android.freshrss.store.api.utils.loggingInterceptor
import fr.chenry.android.freshrss.store.database.models.Account
import fr.chenry.android.freshrss.store.database.models.TokenResponse
import fr.chenry.android.freshrss.utils.addTrailingSlash
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.jackson.JacksonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import retrofit2.create
import retrofit2.http.FieldMap
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part
import retrofit2.http.Path
import retrofit2.http.Query
import retrofit2.http.QueryMap
import java.util.concurrent.TimeUnit

const val GOOGLE_API_ENDPOINT = "api/greader.php"
const val LOGIN_ENPOINT = "accounts/ClientLogin"
const val API_URL_OK_RESPONSE = "OK"

/** Key to use with [fr.chenry.android.freshrss.store.Store.getStreamContents] to retrieve every article from server */
const val ALL_ITEMS_ID = "user/-/state/com.google/reading-list"

/** Key to use with [fr.chenry.android.freshrss.store.Store.getStreamContents] to retrieve favorites from server */
const val FAVORITE_ITEMS_ID = "user/-/state/com.google/starred"

/** Key to use with [fr.chenry.android.freshrss.store.api.API.getItemIds] to retrieve favorites from server */
const val READ_ITEMS_ID = "user/-/state/com.google/read"

const val FEED_EDIT_ROUTE = "reader/api/0/subscription/edit"

interface API {
    @AuthorisationRequired
    @POST("reader/api/0/token")
    fun getWriteToken(): Call<String>

    @JsonOutput
    @AuthorisationRequired
    @UseDeserializer(SubscriptionApiItemsConverter::class)
    @GET("reader/api/0/subscription/list")
    fun getSubscriptions(): Call<List<SubscriptionApiItem>>

    @JsonOutput
    @AuthorisationRequired
    @GET("reader/api/0/unread-count")
    fun getUnreadCount(): Call<UnreadCountsHandler>

    @JsonOutput
    @AuthorisationRequired
    @GET("reader/api/0/stream/contents/{id}?n=1000000")
    fun getStreamContents(
        @Path("id", encoded = true) id: String,
        @Query("ot") ot: String
    ): Call<ContentItemsHandler>

    @JsonOutput
    @AuthorisationRequired
    @UseDeserializer(UnreadItemIdsConverter::class)
    @GET("reader/api/0/stream/items/ids?s=$ALL_ITEMS_ID&n=1000000")
    fun getItemIds(@QueryMap(encoded = true) queryMap: Map<String, String>): Call<List<String>>

    @JsonOutput
    @AuthorisationRequired
    @UseDeserializer(TagsConverter::class)
    @GET("reader/api/0/tag/list")
    fun getTags(): Call<List<FeedTagItem>>

    @FormUrlEncoded
    @TokenRequired
    @AuthorisationRequired
    @POST("reader/api/0/edit-tag")
    fun postEditTag(@FieldMap(encoded = true) contents: Map<String, String>): Call<Unit>

    @TokenRequired
    @AuthorisationRequired
    @POST(FEED_EDIT_ROUTE)
    fun postAddSubscription(
        @Query("s") url: String,
        @Query("a") feedCategory: String? = null,
        @Query("ac") __s__: String = "subscribe"
    ): Call<String>

    companion object {
        const val TOKEN_RENEWAL_FAILED_MESSAGE = "Token renewal failed"

        fun get(account: Account): API {
            val interceptor = APIInterceptor(account)

            val okHttpClient = OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .addInterceptor(loggingInterceptor)
                .build()

            val service: API = Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl(account.serverInstance.addTrailingSlash())
                .addConverterFactory(APIConverter())
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(JacksonConverterFactory.create(JACKSON_OBJECT_MAPPER))
                .build()
                .create()

            interceptor.service = service
            return service
        }
    }
}

interface ConnectionAPI {
    @GET(GOOGLE_API_ENDPOINT)
    suspend fun getApiValidation(): String

    @POST("$GOOGLE_API_ENDPOINT/$LOGIN_ENPOINT")
    @UseDeserializer(TokenResponseConverter::class)
    @Multipart
    suspend fun login(
        @Part("Email") user: String,
        @Part("Passwd") password: String,
        // Don't change these
        @Part("service") service: String = "reader",
        @Part("source") source: String = "FreshRSS",
        @Part("accountType") accountType: String = "GOOGLE"
    ): TokenResponse

    companion object {
        fun get(apiUrl: String): ConnectionAPI {
            val okHttpClient = OkHttpClient.Builder()
                .addInterceptor(loggingInterceptor)
                .connectTimeout(5, TimeUnit.SECONDS)
                .build()

            return Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl(apiUrl.replace("${GOOGLE_API_ENDPOINT}\$".toRegex(), ""))
                .addConverterFactory(APIConverter())
                .addConverterFactory(ScalarsConverterFactory.create())
                .build()
                .create()
        }
    }
}
