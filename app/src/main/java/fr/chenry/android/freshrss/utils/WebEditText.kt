package fr.chenry.android.freshrss.utils

import android.content.Context
import android.os.Build
import android.util.AttributeSet
import android.view.ViewStructure
import androidx.databinding.BindingAdapter
import androidx.lifecycle.MutableLiveData
import com.google.android.material.textfield.TextInputEditText

class WebEditText @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleRes: Int = com.google.android.material.R.attr.editTextStyle
) : TextInputEditText(context, attrs, defStyleRes) {
    private val instanceDomain: MutableLiveData<CharSequence> = MutableLiveData("")

    override fun onProvideAutofillStructure(structure: ViewStructure?, flags: Int) {
        structure?.let(::setWebDomain)
        super.onProvideAutofillStructure(structure, flags)
    }

    override fun onProvideAutofillVirtualStructure(structure: ViewStructure?, flags: Int) {
        structure?.let(::setWebDomain)
        super.onProvideAutofillVirtualStructure(structure, flags)
    }

    override fun onProvideStructure(structure: ViewStructure?) {
        structure?.let(::setWebDomain)
        super.onProvideStructure(structure)
    }

    override fun onProvideVirtualStructure(structure: ViewStructure?) {
        structure?.let(::setWebDomain)
        super.onProvideVirtualStructure(structure)
    }

    private fun setWebDomain(structure: ViewStructure) {
        val domain = instanceDomain.value?.toString()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O && domain.isWebUrl())
            structure.setWebDomain(domain)
    }

    companion object {
        @JvmStatic
        @BindingAdapter("instanceDomain")
        fun setInstanceDomain(editText: WebEditText, resource: CharSequence?) =
            resource?.let(editText.instanceDomain::postValue)
    }
}
