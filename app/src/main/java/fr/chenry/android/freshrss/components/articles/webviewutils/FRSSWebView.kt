package fr.chenry.android.freshrss.components.articles.webviewutils

import android.content.Context
import android.util.AttributeSet
import android.webkit.WebView
import androidx.fragment.app.Fragment
import fr.chenry.android.freshrss.R
import fr.chenry.android.freshrss.store.database.models.Article
import org.jsoup.Jsoup

class FRSSWebView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0,
    defStyleRes: Int = 0
) : WebView(context, attrs, defStyleAttr, defStyleRes) {

    init {
        isFocusable = true
        isFocusableInTouchMode = true
        isScrollbarFadingEnabled = false
        settings.javaScriptEnabled = false
        settings.defaultTextEncodingName = "UTF-8"
    }

    fun load(fragment: Fragment, article: Article) {
        """
            |<!DOCTYPE html>
            |<html>
            |  <head>
            |    <title>${article.title}</title>
            |    <meta charset="UTF-8" />
            |    <link rel="stylesheet" type="text/css" href="base-style.css" />
            |  </head>
            |  <body>
            |    <h1 id="article-title">${article.title}</h1>
            |    ${if (article.author.isNotEmpty()) "<h3 id='article-authors'>${article.author}</h3>" else ""}
            |    <div id="main-content">${article.content}</div>
            |  </body>
            |</html>
        """.trimMargin().let {
            loadDataWithBaseURL("file:///android_asset/", processHTML(it), "text/html; charset=utf-8", "UTF-8", null)
            webViewClient = FRSSWebViewClient(fragment, article.url)
        }
    }

    private fun processHTML(html: String): String {
        val doc = Jsoup.parse(html)
        val imagesInLinks = doc.select("a > img")

        imagesInLinks.forEach {
            it.parent()?.let { link ->
                val linkToOriginalImage = link.attr("abs:href")
                val type = link.attr("type")

                it.attr("data-original-href", linkToOriginalImage)
                if (type.isNotBlank()) it.attr("data-original-type", type)

                runCatching {
                    val newImage = it.clone()
                    link.after(newImage).remove()
                    newImage.after(
                        """<a href="$linkToOriginalImage" class="link-to-original-image">
                    |   ${rootView.context.getString(R.string.link_to_original_image)}
                    |</a>
                        """.trimMargin()
                    )
                }
            }
        }

        return doc.html()
    }
}
