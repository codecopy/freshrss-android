/**
 * This is a 1:1 copy of Kotlin's Resul't monad.
 * This mainly exist to prevent usage functions returning Result in coroutines
 * to unexpectedly fail.
 */
@file:Suppress("UNCHECKED_CAST", "RedundantVisibilityModifier", "unused", "NOTHING_TO_INLINE")

package fr.chenry.android.freshrss.utils

import java.io.Serializable

@JvmInline
@Suppress("NON_PUBLIC_PRIMARY_CONSTRUCTOR_OF_INLINE_CLASS")
value class SafeResult<out T> @PublishedApi internal constructor(
    @PublishedApi
    internal val value: Any?
) : Serializable {
    val isSuccess: Boolean get() = value !is Failure
    val isFailure: Boolean get() = value is Failure
    inline fun getOrNull(): T? =
        when {
            isFailure -> null
            else -> value as T
        }

    public fun exceptionOrNull(): Throwable? =
        when (value) {
            is Failure -> value.exception
            else -> null
        }

    public override fun toString(): String =
        when (value) {
            is Failure -> value.toString() // "Failure($exception)"
            else -> "Success($value)"
        }

    public companion object {

        @Suppress("INAPPLICABLE_JVM_NAME")
        @JvmName("success")
        public inline fun <T> success(value: T): SafeResult<T> =
            SafeResult(value)

        /**
         * Returns an instance that encapsulates the given [Throwable] [exception] as failure.
         */
        @Suppress("INAPPLICABLE_JVM_NAME")
        @JvmName("failure")
        public inline fun <T> failure(exception: Throwable): SafeResult<T> =
            SafeResult(createFailure(exception))
    }

    internal class Failure(
        @JvmField
        val exception: Throwable
    ) : Serializable {
        override fun equals(other: Any?): Boolean = other is Failure && exception == other.exception
        override fun hashCode(): Int = exception.hashCode()
        override fun toString(): String = "Failure($exception)"
    }
}

@PublishedApi
internal fun createFailure(exception: Throwable): Any =
    SafeResult.Failure(exception)

@PublishedApi
internal fun SafeResult<*>.throwOnFailure() {
    if (value is SafeResult.Failure) throw value.exception
}

public inline fun <R> safeResult(block: () -> R): SafeResult<R> {
    return try {
        SafeResult.success(block())
    } catch (e: Throwable) {
        SafeResult.failure(e)
    }
}

@SinceKotlin("1.3")
public inline fun <T, R> T.safeResult(block: T.() -> R): SafeResult<R> {
    return try {
        SafeResult.success(block())
    } catch (e: Throwable) {
        SafeResult.failure(e)
    }
}

public inline fun <T> SafeResult<T>.getOrThrow(): T {
    throwOnFailure()
    return value as T
}

public inline fun <R, T : R> SafeResult<T>.getOrElse(onFailure: (exception: Throwable) -> R): R {
    return when (val exception = exceptionOrNull()) {
        null -> value as T
        else -> onFailure(exception)
    }
}

public inline fun <R, T : R> SafeResult<T>.getOrDefault(defaultValue: R): R {
    if (isFailure) return defaultValue
    return value as T
}

public inline fun <R, T> SafeResult<T>.fold(
    onSuccess: (value: T) -> R,
    onFailure: (exception: Throwable) -> R
): R {
    return when (val exception = exceptionOrNull()) {
        null -> onSuccess(value as T)
        else -> onFailure(exception)
    }
}

public inline fun <R, T> SafeResult<T>.map(transform: (value: T) -> R): SafeResult<R> {
    return when {
        isSuccess -> SafeResult.success(transform(value as T))
        else -> SafeResult(value)
    }
}

public inline fun <R, T> SafeResult<T>.flatMap(transform: (value: T) -> SafeResult<R>): SafeResult<R> {
    return when {
        isSuccess -> transform(value as T)
        else -> SafeResult(value)
    }
}

public inline fun <R, T> SafeResult<T>.mapCatching(transform: (value: T) -> R): SafeResult<R> {
    return when {
        isSuccess -> safeResult { transform(value as T) }
        else -> SafeResult(value)
    }
}

public inline fun <R, T : R> SafeResult<T>.recover(transform: (exception: Throwable) -> R): SafeResult<R> {
    return when (val exception = exceptionOrNull()) {
        null -> this
        else -> SafeResult.success(transform(exception))
    }
}

public inline fun <R, T : R> SafeResult<T>.recoverCatching(transform: (exception: Throwable) -> R): SafeResult<R> {
    return when (val exception = exceptionOrNull()) {
        null -> this
        else -> safeResult { transform(exception) }
    }
}

public inline fun <T> SafeResult<T>.onFailure(action: (exception: Throwable) -> Unit): SafeResult<T> {
    exceptionOrNull()?.let { action(it) }
    return this
}

public inline fun <T> SafeResult<T>.onSuccess(action: (value: T) -> Unit): SafeResult<T> {
    if (isSuccess) action(value as T)
    return this
}
