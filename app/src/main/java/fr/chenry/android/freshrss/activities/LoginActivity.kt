package fr.chenry.android.freshrss.activities

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.os.ResultReceiver
import android.view.inputmethod.InputMethodManager
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import fr.chenry.android.freshrss.R
import fr.chenry.android.freshrss.store.api.ConnectionWrapper
import fr.chenry.android.freshrss.store.viewmodels.LoginVM
import fr.chenry.android.freshrss.utils.asURL
import fr.chenry.android.freshrss.utils.unit
import fr.chenry.android.freshrss.utils.windowToken

class LoginActivity : AppCompatActivity(R.layout.activity_login) {
    val navigation: NavController get() = findNavController(R.id.login_activity_host_fragment)
    val viewModel by viewModels<LoginVM>()
    val connectionWrapper by lazy { ConnectionWrapper(this, viewModel) }

    private val inputManager by lazy { ContextCompat.getSystemService(this, InputMethodManager::class.java) }

    private val navHostFragment inline get() = supportFragmentManager.primaryNavigationFragment as? NavHostFragment
    private val currentFragment: ChildFragment?
        get() {
            val fragments = navHostFragment?.childFragmentManager?.fragments ?: listOf()
            if (fragments.isEmpty()) return null
            return fragments.last() as? ChildFragment
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        loadFromConfig()
    }

    override fun onStart() {
        super.onStart()
        navHostFragment?.childFragmentManager?.addOnBackStackChangedListener {
            currentFragment?.let {
                supportActionBar?.setDisplayHomeAsUpEnabled(it.canGoBack())
            }
        }
    }

    override fun onPause() {
        super.onPause()
        hideKeyboard()
    }

    override fun onSupportNavigateUp(): Boolean = currentFragment?.let {
        if (it.canGoBack()) it.onSupportNavigateUp() else false
    } ?: navigation.navigateUp() || super.onSupportNavigateUp()

    private fun loadFromConfig() {
        runCatching {
            val instanceFromConfig = getString(R.string.debug_hostname).asURL()
            val protocolFromConfig = "${instanceFromConfig.protocol}://"
            if (protocolFromConfig in viewModel.authorizedProtocols) {
                viewModel.protocol.postValue(protocolFromConfig)
                viewModel.instanceDomain.postValue(instanceFromConfig.toString().replace(protocolFromConfig, ""))
            }
        }

        viewModel.username.postValue(getString(R.string.debug_username).trim())
        viewModel.password.postValue(getString(R.string.debug_password).trim())
    }

    private fun hideKeyboard() = windowToken?.let { token ->
        val resultReceiver = ResultReceiver(Handler(Looper.getMainLooper()))
        inputManager?.hideSoftInputFromWindow(token, 0, resultReceiver)
    }.unit()

    interface ChildFragment {
        fun canGoBack(): Boolean
        fun onSupportNavigateUp(): Boolean
    }
}
