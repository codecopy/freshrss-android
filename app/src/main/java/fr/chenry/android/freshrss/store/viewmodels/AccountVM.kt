package fr.chenry.android.freshrss.store.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.toLiveData
import fr.chenry.android.freshrss.R
import fr.chenry.android.freshrss.store.database.models.Account
import fr.chenry.android.freshrss.store.database.models.VoidAccount
import fr.chenry.android.freshrss.utils.IResourceResolver
import fr.chenry.android.freshrss.utils.emptyStringRes
import fr.chenry.android.freshrss.utils.firstOrElse
import fr.chenry.android.freshrss.utils.functionRes
import fr.chenry.android.freshrss.utils.getUserTimeZone
import fr.chenry.android.freshrss.utils.idResRes
import fr.chenry.android.freshrss.utils.period
import fr.chenry.android.freshrss.utils.requireF
import fr.chenry.android.freshrss.utils.userLocale
import io.reactivex.Flowable
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import org.joda.time.DateTime
import org.joda.time.Duration
import org.joda.time.format.DateTimeFormat
import org.joda.time.format.PeriodFormat
import java.util.concurrent.TimeUnit

class AccountVM(application: Application) : AndroidViewModel(application) {
    private val ticker = Flowable
        .interval(0, 30, TimeUnit.SECONDS)
        .timeInterval()
        .observeOn(AndroidSchedulers.mainThread())

    private val source = requireF().db.getAccountFlowable().map { it.firstOrElse(VoidAccount) }
    val account: LiveData<Account> = source.toLiveData()

    val unreadCount: Int inline get() = account.value?.unreadCount ?: 0

    val lastFetchText: LiveData<IResourceResolver> by lazy {
        Flowable.combineLatest(listOf(ticker, source)) { (_, it) ->
            runBlocking {
                withContext(Dispatchers.IO) {
                    it as Account

                    if (it.lastFetchDate.millis == 0L) return@withContext emptyStringRes()

                    val sevenDaysAgo = DateTime.now(getUserTimeZone()).minusDays(7)

                    if (it.lastFetchDate.isAfter(sevenDaysAgo)) {
                        val period = it.lastFetchDate.period(DateTime.now())

                        if (period.toStandardDuration().isShorterThan(Duration(20_000)))
                            idResRes(R.string.last_sync_a_few_seconds_ago)
                        else
                            idResRes(R.string.last_sync_relative_date_text) {
                                +functionRes {
                                    val res = PeriodFormat.wordBased(userLocale).print(period)
                                    res
                                }
                            }
                    } else {
                        idResRes(R.string.last_sync_absolute_date_text) {
                            +functionRes {
                                it.lastFetchDate.toString(DateTimeFormat.patternForStyle("M-", userLocale))
                            }
                        }
                    }
                }
            }
        }.distinctUntilChanged().toLiveData()
    }
}
