package fr.chenry.android.freshrss.components.feeds

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.ComponentActivity
import androidx.annotation.VisibleForTesting
import androidx.compose.animation.core.RepeatMode
import androidx.compose.animation.core.Spring
import androidx.compose.animation.core.VisibilityThreshold
import androidx.compose.animation.core.infiniteRepeatable
import androidx.compose.animation.core.spring
import androidx.compose.animation.core.tween
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.heightIn
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyItemScope
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.composed
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.graphics.painter.BitmapPainter
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.semantics.contentDescription
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.IntOffset
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.fragment.app.Fragment
import androidx.lifecycle.compose.ExperimentalLifecycleComposeApi
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.google.accompanist.placeholder.PlaceholderHighlight
import com.google.accompanist.placeholder.placeholder
import com.google.accompanist.placeholder.shimmer
import com.google.accompanist.swiperefresh.SwipeRefresh
import com.google.accompanist.swiperefresh.SwipeRefreshState
import com.google.android.material.composethemeadapter.MdcTheme
import fr.chenry.android.freshrss.R
import fr.chenry.android.freshrss.store.database.models.Feed
import fr.chenry.android.freshrss.store.database.models.FeedCategory
import fr.chenry.android.freshrss.store.database.models.FeedLikeDisplayable
import fr.chenry.android.freshrss.store.database.models.FeedTags
import fr.chenry.android.freshrss.store.database.models.Feeds
import fr.chenry.android.freshrss.store.database.models.VoidCategory
import fr.chenry.android.freshrss.store.viewmodels.FeedVM
import fr.chenry.android.freshrss.store.viewmodels.FeedVMFactory
import fr.chenry.android.freshrss.store.viewmodels.RefreshWorkInfosVM
import fr.chenry.android.freshrss.utils.requireF
import java.util.Random
import java.util.UUID

class FeedFragment : Fragment() {
    private val args: FeedFragmentArgs by navArgs()
    private val section: FeedSection by lazy { args.section }
    private val category: FeedCategory by lazy { args.category }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = ComposeView(requireContext()).apply {
        setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)
        setContent {
            MainContent(section = section, category = category, onClick = ::onClick)
        }
    }

    @VisibleForTesting
    fun onClick(virtualFeed: FeedLikeDisplayable): Boolean {
        if (virtualFeed is Feed) when (category) {
            VoidCategory -> MainFeedFragmentDirections.mainFeedsToArticles(virtualFeed, section)
            else -> FeedFragmentDirections.articlesCategoryToArticles(virtualFeed, section)
        }.let(findNavController()::navigate)

        if (virtualFeed is FeedCategory) findNavController().navigate(
            MainFeedFragmentDirections.mainFeedsToArticlesCategory(virtualFeed, section)
        )
        return true
    }
}

@Composable
@OptIn(ExperimentalLifecycleComposeApi::class)
fun MainContent(
    section: FeedSection,
    category: FeedCategory,
    onClick: (virtualFeed: FeedLikeDisplayable) -> Boolean
) {
    val activity = LocalContext.current as ComponentActivity
    val workInfos = viewModel<RefreshWorkInfosVM>(viewModelStoreOwner = activity)
    val viewModel = viewModel<FeedVM>(
        viewModelStoreOwner = activity,
        factory = FeedVMFactory(activity.requireF(), category, section),
        key = "section=${section.name}${if (category != VoidCategory) ":category=${category.id}" else ""}"
    )
    val categories by viewModel.categories.collectAsStateWithLifecycle()
    val feeds by viewModel.feeds.collectAsStateWithLifecycle()
    val isLoading by workInfos.isWorkerRunningLiveData.observeAsState(false)

    MdcTheme {
        SwipeRefresh(state = SwipeRefreshState(isLoading), onRefresh = activity.requireF()::refresh) {
            if (feeds.isEmpty() && isLoading) FeedPlaceholder()
            else if (feeds.isEmpty()) EmptySubscriptionImage(section, category)
            else FeedList(categories, feeds, onClick = onClick)
        }
    }
}

@Composable
@Preview
fun FeedPlaceholder() {
    val random = Random()
    val categories by rememberSaveable {
        mutableStateOf(
            random.rangeInt(1, 3).map {
                FeedCategory("${UUID.randomUUID()}", " ".random(20, 40), "", 11)
            }
        )
    }

    val feeds by rememberSaveable {
        mutableStateOf(
            random.rangeInt(1, 3).associate {
                " ".random(1, 10) to random.rangeInt(1, 4).map {
                    Feed("${UUID.randomUUID()}", " ".random(20, 40), "", 11)
                }
            }
        )
    }

    FeedList(categories, feeds, placeholder = true)
}

@Composable
@OptIn(ExperimentalFoundationApi::class)
private fun FeedList(
    categories: FeedTags,
    feeds: Map<String, Feeds>,
    placeholder: Boolean = false,
    onClick: (virtualFeed: FeedLikeDisplayable) -> Boolean = { false }
) {
    val listState = rememberLazyListState()
    val headerContent = stringResource(id = R.string.subscription_categories)
    val contentModifier = if (!placeholder) Modifier else Modifier.placeholder(
        visible = true,
        shape = RoundedCornerShape(4.dp),
        color = Color.Transparent,
        highlight = PlaceholderHighlight.shimmer(
            highlightColor = Color.Gray,
            animationSpec = infiniteRepeatable(
                animation = tween(durationMillis = 1000),
                repeatMode = RepeatMode.Restart
            )
        ),
    )

    LazyColumn(state = listState) {
        if (categories.isNotEmpty()) {
            stickyHeader(key = headerContent, contentType = String::class) {
                Header(
                    content = headerContent,
                    modifier = Modifier.conditionnalAnimateItemPlacement(listState),
                    contentModifier = contentModifier
                )
            }

            items(categories, key = { c -> c.id }, contentType = { FeedCategory::class }) {
                FeedItem(
                    item = it,
                    modifier = Modifier.conditionnalAnimateItemPlacement(listState),
                    contentModifier = contentModifier,
                    onClick = onClick
                )
            }
        }

        feeds.entries.forEach { (k, v) ->
            stickyHeader(k) {
                Header(
                    content = k,
                    modifier = Modifier.conditionnalAnimateItemPlacement(listState),
                    contentModifier = contentModifier
                )
            }

            items(v, key = { c -> c.id }, contentType = { Feed::class }) {
                FeedItem(
                    item = it,
                    modifier = Modifier.conditionnalAnimateItemPlacement(listState),
                    contentModifier = contentModifier,
                    onClick = onClick
                )
            }
        }
    }
}

@Composable
private fun Header(content: String, modifier: Modifier = Modifier, contentModifier: Modifier = Modifier) {
    Row(
        modifier = modifier
            .fillMaxWidth()
            .background(color = colorResource(R.color.color_accent)),
    ) {
        Box(
            modifier = Modifier.padding(
                top = dimensionResource(R.dimen.padding_4dp),
                bottom = dimensionResource(R.dimen.padding_4dp),
                start = dimensionResource(R.dimen.padding_8dp),
                end = dimensionResource(R.dimen.padding_8dp),
            )
        ) {
            Text(
                modifier = contentModifier.semantics { contentDescription = "Header content" },
                text = content,
                style = TextStyle(
                    fontWeight = FontWeight.Bold,
                    color = colorResource(R.color.design_default_color_on_primary),
                ),
            )
        }
    }
}

@Composable
private fun FeedItem(
    item: FeedLikeDisplayable,
    modifier: Modifier = Modifier,
    contentModifier: Modifier = Modifier,
    onClick: (virtualFeed: FeedLikeDisplayable) -> Boolean,
) {
    Row(
        verticalAlignment = Alignment.CenterVertically,
        modifier = modifier
            .semantics { contentDescription = "Feed item" }
            .clickable { onClick(item) }
            .background(colorResource(id = R.color.design_default_color_background))
            .fillMaxWidth()
            .padding(
                top = dimensionResource(R.dimen.padding_4dp),
                bottom = dimensionResource(R.dimen.padding_4dp),
                start = dimensionResource(R.dimen.padding_8dp),
                end = dimensionResource(R.dimen.padding_8dp),
            )
    ) {
        Box(modifier = Modifier.padding(dimensionResource(R.dimen.padding_6dp))) {
            Image(
                painter = item.imageBitmap?.asImageBitmap()?.let(::BitmapPainter) ?: painterResource(
                    R.drawable.ic_rss_feed_black_24dp_no_padding
                ),
                contentDescription = null,
                modifier = contentModifier.heightIn(0.dp, 20.dp),
            )
        }
        Box(
            modifier = Modifier
                .align(alignment = Alignment.CenterVertically)
                .weight(1f)
        ) {
            Text(
                text = item.title,
                modifier = contentModifier,
                style = TextStyle(fontWeight = FontWeight.Bold),
            )
        }
        if (item.unreadCount >= 0 && item != VoidCategory) {
            Box(modifier = Modifier.padding(start = dimensionResource(id = R.dimen.padding_4dp))) {
                Text(
                    text = "${item.unreadCount}",
                    modifier = contentModifier,
                    style = TextStyle(fontWeight = FontWeight.Bold),
                )
            }
        }
        Image(
            painter = painterResource(id = R.drawable.ic_keyboard_arrow_next_black_24dp),
            contentDescription = null,
            modifier = Modifier
                .align(alignment = Alignment.CenterVertically)
                .heightIn(0.dp, 20.dp)
        )
    }
}

@Composable
private fun EmptySubscriptionImage(
    section: FeedSection,
    category: FeedCategory
) {
    val painter = when (section) {
        FeedSection.ALL -> R.drawable.ic_undraw_empty
        FeedSection.UNREAD -> R.drawable.ic_undraw_joyride
        FeedSection.FAVORITES -> R.drawable.ic_undraw_loving_it
    }.let { painterResource(it) }

    val contentDescription = if (category != VoidCategory) {
        when (section) {
            FeedSection.ALL -> R.string.empty_subscriptions_section_all_category
            FeedSection.UNREAD -> R.string.empty_subscriptions_section_unread_category
            FeedSection.FAVORITES -> R.string.empty_subscriptions_section_favorites_category
        }
    } else {
        when (section) {
            FeedSection.ALL -> R.string.empty_subscriptions_section_all
            FeedSection.UNREAD -> R.string.empty_subscriptions_section_unread
            FeedSection.FAVORITES -> R.string.empty_subscriptions_section_favorites
        }
    }.let { stringResource(it, category.label) }

    Column(
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier
            .fillMaxSize()
            .padding(16.dp)
    ) {
        Image(
            painter = painter,
            contentDescription = contentDescription,
            modifier = Modifier
                .height(140.dp)
                .padding(bottom = 16.dp),
        )
        Text(
            text = contentDescription,
            style = TextStyle(
                color = colorResource(id = R.color.light_grey),
                fontSize = 24.sp,
                fontWeight = FontWeight.Bold,
                textAlign = TextAlign.Center,
            )
        )
    }
}

@Suppress("NOTHING_TO_INLINE")
private inline fun String.random(min: Int, max: Int) =
    if (min <= 0 || max <= 0 || min >= max) this
    else Random().rangeInt(min, max).joinToString("") { this }

@Suppress("NOTHING_TO_INLINE")
private inline fun Random.rangeInt(min: Int, max: Int) =
    if (min <= 0 || max <= 0 || min >= max) (0..0)
    else (0..(this.nextInt(max - min) + min))

context(LazyItemScope)
@OptIn(ExperimentalFoundationApi::class)
private fun Modifier.conditionnalAnimateItemPlacement(lazyListState: LazyListState): Modifier = composed {
    val enabled by remember {
        derivedStateOf { lazyListState.isScrollInProgress }
    }

    if (!enabled) this.animateItemPlacement(
        spring(stiffness = Spring.StiffnessHigh, visibilityThreshold = IntOffset.VisibilityThreshold)
    ) else this
}
