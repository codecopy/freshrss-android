@file:Suppress("unused")

package fr.chenry.android.freshrss.utils

import android.content.Context
import androidx.annotation.StringRes

interface IResourceResolver {
    operator fun invoke(context: Context): String
}

class FunctionResourceResolver internal constructor(private val cb: Context.() -> String) : IResourceResolver {
    override operator fun invoke(context: Context): String = cb(context)
}

object EmptyStringResourceResolver : IResourceResolver {
    override operator fun invoke(context: Context): String = ""
}

class IdResourceResolver @JvmOverloads internal constructor(
    @StringRes private val resId: Int,
    vararg args: Any = arrayOf()
) : IResourceResolver {
    private val formatArgs: MutableList<Any> = args.toMutableList()

    fun add(any: Any) = this@IdResourceResolver.formatArgs.add(any)
    operator fun Any.unaryPlus() = add(this)

    fun addAll(all: Collection<Any>) = this@IdResourceResolver.formatArgs.addAll(all)
    fun addAll(all: Iterable<Any>) = this@IdResourceResolver.formatArgs.addAll(all)
    fun addAll(all: Sequence<Any>) = this@IdResourceResolver.formatArgs.addAll(all)
    fun addAll(vararg all: Any) = this@IdResourceResolver.formatArgs.addAll(all)

    override operator fun invoke(context: Context): String {
        val fargs = formatArgs.map {
            if (it is IResourceResolver) it(context) else it
        }

        return if (fargs.isEmpty()) context.getString(resId) else context.getString(resId, *fargs.toTypedArray())
    }
}

fun emptyStringRes() = EmptyStringResourceResolver

@JvmOverloads
fun idResRes(
    @StringRes resId: Int,
    vararg args: Any = arrayOf(),
    cb: IdResourceResolver.() -> Unit = {}
) = IdResourceResolver(resId, *args).apply(cb)

fun functionRes(cb: Context.() -> String) = FunctionResourceResolver(cb)
