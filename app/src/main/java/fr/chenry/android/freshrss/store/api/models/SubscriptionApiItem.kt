package fr.chenry.android.freshrss.store.api.models

import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import fr.chenry.android.freshrss.store.api.utils.FeedCategoriesDeserializer
import fr.chenry.android.freshrss.store.api.utils.HtmlEntitiesDeserializer

data class SubscriptionApiItem(
    val id: String,
    @JsonDeserialize(using = HtmlEntitiesDeserializer::class)
    val title: String,
    @JsonDeserialize(using = FeedCategoriesDeserializer::class)
    val categories: List<String> = listOf(),
    val url: String,
    val htmlUrl: String,
    val iconUrl: String
)
