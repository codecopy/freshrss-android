package fr.chenry.android.freshrss.utils

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import org.acra.ACRA

class RefreshErrorDetails : BroadcastReceiver() {
    @Suppress("DEPRECATION")
    override fun onReceive(context: Context, intent: Intent) = intent.getSerializableExtra(EXEPTION_EXTRA_KEY)?.let {
        if (it !is Throwable) return@let
        ACRA.errorReporter.handleSilentException(RefreshFailedException(it))
        context.requireF().notificationManager.cancel(NotificationHelper.FAIL_REFRESH_NOTIFICATION)
    }.unit()

    companion object {
        const val EXEPTION_EXTRA_KEY = "EXEPTION_EXTRA_KEY"
    }

    private class RefreshFailedException(cause: Throwable) : Exception(cause)
}
