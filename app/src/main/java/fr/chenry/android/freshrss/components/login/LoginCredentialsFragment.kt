package fr.chenry.android.freshrss.components.login

import android.os.Bundle
import android.text.method.LinkMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.VisibleForTesting
import androidx.core.text.parseAsHtml
import fr.chenry.android.freshrss.R
import fr.chenry.android.freshrss.components.utils.errorDetailDialog
import fr.chenry.android.freshrss.components.utils.simpleErrorDialog
import fr.chenry.android.freshrss.components.utils.simpleWarningDialog
import fr.chenry.android.freshrss.databinding.FragmentLoginCredentialsBinding
import fr.chenry.android.freshrss.store.api.ConnectionWrapper
import fr.chenry.android.freshrss.store.api.GOOGLE_API_ENDPOINT
import fr.chenry.android.freshrss.utils.NOOP
import fr.chenry.android.freshrss.utils.getHtmlString

class LoginCredentialsFragment : LoginWrappedFragment<FragmentLoginCredentialsBinding>() {
    override val focusField get() = binding.usernameField
    override val imeNextField get() = binding.passwordField
    override val nextButton get() = binding.loginButton
    override val resetFields by lazy { arrayOf(binding.usernameContainer, binding.passwordContainer) }

    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    var badCredentialsCount = 2

    override fun inflateBinding(inflater: LayoutInflater, container: ViewGroup?): FragmentLoginCredentialsBinding =
        FragmentLoginCredentialsBinding.inflate(inflater, container, false)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val view = super.onCreateView(inflater, container, savedInstanceState)

        binding.actualUrl = viewModel.completeApiUrlWithStyle
        binding.username = viewModel.username
        binding.password = viewModel.password

        binding.connectionPasswordWarning.apply {
            val config = getString(R.string.http_api_password_config_section)
            text = getHtmlString(R.string.connection_password_warning, config)
            movementMethod = LinkMovementMethod.getInstance()
        }

        resetErrors()

        return view
    }

    override fun onStart() {
        super.onStart()

        when (val result = viewModel.loginResult.pop()) {
            ConnectionWrapper.LoginResult.ApiAccessDisabled -> apiAccessDisabledWarning()
            ConnectionWrapper.LoginResult.BadCredentials -> badCredentials()
            ConnectionWrapper.LoginResult.NoPriorOperation -> NOOP()
            is ConnectionWrapper.LoginResult.Ok -> throw Exception("Shouldn't be redirected back when login was ok")
            is ConnectionWrapper.LoginResult.UnknownError -> errorDetailDialog(result.exception)
        }
    }

    override fun nextAction() {
        resetErrors()

        if (viewModel.username.value.isNullOrBlank()) return displayError(
            binding.usernameContainer,
            R.string.error_field_required
        )

        if (viewModel.password.value.isNullOrBlank()) return displayError(
            binding.passwordContainer,
            R.string.error_invalid_password
        )

        hideKeyboard()

        loginActivity.navigation.navigate(LoginWrapperFragmentDirections.toLoaderFragment())
    }

    private fun apiAccessDisabledWarning() = simpleErrorDialog {
        textContainsLinks = true
        text = getHtmlString(
            R.string.api_access_disabled_warning,
            getClickableInstanceText(),
            "<b>${getString(R.string.http_api_access_option_section)}</b>",
            "<b>${getString(R.string.http_api_access_option)}</b>",
        )
    }

    private fun badCredentials() {
        if (++badCredentialsCount % BAD_CREDENTIALS_COUNT_TRIGGER == 0) simpleWarningDialog {
            val message = getString(R.string.api_password_misconfiguration_hint)
            val longMessage = getString(R.string.api_password_misconfiguration_long_message)
            text = "<p>$message</p>$longMessage".parseAsHtml()
            textContainsLinks = true
        } else {
            binding.passwordContainer.error = getString(R.string.bad_credentials_form_error)
            binding.passwordField.requestFocus()
        }
    }

    private fun getClickableInstanceText(): String {
        val url = viewModel.completeApiUrl.replace("/$GOOGLE_API_ENDPOINT", "/i/")
        return """<a href="$url">$url</a>"""
    }

    companion object {
        const val BAD_CREDENTIALS_COUNT_TRIGGER = 3
    }
}
