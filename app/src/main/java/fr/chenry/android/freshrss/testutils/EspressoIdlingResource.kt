package fr.chenry.android.freshrss.testutils

import androidx.test.espresso.idling.CountingIdlingResource

object EspressoIdlingResource {
    private const val NETWORK_RESOURCE = "NETWORK_RESOURCE"
    private const val DISPLAY_RESOURCE = "DISPLAY_RESOURCE"

    @JvmField
    val networkIdlingResource = CountingIdlingResource(NETWORK_RESOURCE)

    @JvmField
    val displayIdlingResource = CountingIdlingResource(DISPLAY_RESOURCE)

    operator fun CountingIdlingResource.unaryPlus(): Unit = this.increment()
    operator fun CountingIdlingResource.unaryMinus() {
        if (!this.isIdleNow) this.decrement()
    }
}
