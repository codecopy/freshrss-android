package fr.chenry.android.freshrss.store.database.models

import android.graphics.Bitmap
import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Dao
import androidx.room.Entity
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.PrimaryKey
import androidx.room.Query
import androidx.room.Update
import fr.chenry.android.freshrss.store.api.models.SubscriptionApiItem
import kotlinx.coroutines.flow.Flow
import kotlinx.parcelize.IgnoredOnParcel
import kotlinx.parcelize.Parcelize
import org.joda.time.LocalDateTime

typealias Feeds = List<Feed>

@Parcelize
@Entity(tableName = "subscriptions")
data class Feed(
    @PrimaryKey
    override val id: String,
    override val title: String,
    val iconUrl: String,
    override val unreadCount: Int = 0,
    val newestArticleDate: LocalDateTime = LocalDateTime(0)
) : Parcelable, FeedLikeDisplayable {
    @IgnoredOnParcel
    var iconUrlFlag: String = ""

    @IgnoredOnParcel
    @ColumnInfo(typeAffinity = ColumnInfo.BLOB)
    override var imageBitmap: Bitmap? = null

    fun toFeedUpdate() = FeedUpdate(id, title, iconUrl)

    companion object {
        fun fromFeedApiItem(feedApiItem: SubscriptionApiItem) = Feed(
            feedApiItem.id,
            feedApiItem.title.trim(),
            feedApiItem.iconUrl.trim()
        )
    }
}

data class FeedUpdate(
    val id: String,
    val title: String,
    val iconUrl: String
)

@Dao
interface FeedDAO {

    @Insert(onConflict = OnConflictStrategy.ABORT)
    suspend fun insertAll(feeds: Feeds)

    @Update(entity = Feed::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun updateValues(feeds: List<FeedUpdate>)

    @Query("SELECT id FROM subscriptions")
    suspend fun getAllIds(): List<String>

    @Query("SELECT * FROM subscriptions ORDER BY title COLLATE NOCASE ASC")
    fun getAll(): Flow<Feeds>

    @Query("SELECT * FROM subscriptions WHERE unreadCount > 0 ORDER BY date(newestArticleDate) ASC")
    fun getAllUnread(): Flow<Feeds>

    @Query(
        """
        SELECT DISTINCT s.* FROM subscriptions s
            INNER JOIN articles a
            ON a.streamId = s.id
        WHERE a.favorite = '$FAVORITE_DB_NAME'
        ORDER BY s.title COLLATE NOCASE ASC
        """
    )
    fun getAllWithFavorites(): Flow<Feeds>

    @Query("SELECT * FROM subscriptions WHERE id = :id")
    fun byId(id: String): Flow<Feeds>

    @Query("SELECT title FROM subscriptions WHERE id = :id LIMIT 1")
    suspend fun titleById(id: String): String

    @Query(
        """
        SELECT s.* FROM subscriptions s
            INNER JOIN feed_to_tag_relation f
            ON f.feedId = s.id
        WHERE f.tagId = :tagId
        ORDER BY s.title COLLATE NOCASE ASC
        """
    )
    fun byCategory(tagId: String): Flow<Feeds>

    @Query(
        """
        SELECT s.* FROM subscriptions s
            INNER JOIN feed_to_tag_relation f
            ON f.feedId = s.id
        WHERE f.tagId = :tagId
        AND s.unreadCount > 0
        ORDER BY date(s.newestArticleDate) ASC
        """
    )
    fun byCategoryAndUnreadCount(tagId: String): Flow<Feeds>

    @Query(
        """
        SELECT DISTINCT s.* FROM subscriptions s
            INNER JOIN articles a
            ON a.streamId = s.id
            INNER JOIN feed_to_tag_relation f
            ON f.feedId = s.id
        WHERE f.tagId = :tagId
        AND a.favorite = '$FAVORITE_DB_NAME'
        ORDER BY s.title COLLATE NOCASE ASC
        """
    )
    fun bySubscriptionAndFavorites(tagId: String): Flow<Feeds>

    @Query(
        """
        SELECT * FROM subscriptions
        WHERE imageBitmap IS NULL
            OR LENGTH(imageBitmap) = 0
            OR iconUrl != iconUrlFlag
        """
    )
    suspend fun withImageToUpdate(): Feeds

    @Query("DELETE FROM subscriptions WHERE id NOT IN (:ids)")
    suspend fun deleteAllAbsentsById(ids: Collection<String>)

    @Query("UPDATE subscriptions SET unreadCount = :count WHERE id = :id")
    suspend fun updateCount(id: String, count: Int)

    @Query("UPDATE subscriptions SET newestArticleDate = :d WHERE id = :id AND newestArticleDate < :d")
    suspend fun updateNewestArticleDate(id: String, d: LocalDateTime)

    @Query("UPDATE subscriptions SET unreadCount = unreadCount + 1 WHERE id = :id")
    fun incrementCount(id: String)

    @Query("UPDATE subscriptions SET unreadCount = unreadCount - 1 WHERE id = :id AND unreadCount > 0")
    suspend fun decrementCount(id: String)

    @Update
    suspend fun update(feed: Feed)

    @Query("UPDATE subscriptions SET imageBitmap = NULL WHERE id = :id")
    fun deleteImage(id: String)
}
