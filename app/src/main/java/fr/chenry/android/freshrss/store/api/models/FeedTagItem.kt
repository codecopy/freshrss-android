package fr.chenry.android.freshrss.store.api.models

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonProperty

data class FeedTagItem(
    val id: String,
    val type: String?,
    @JsonProperty("unread_count")
    val unreadCount: Int = 0
) {

    @JsonIgnore
    val label: String = id.replace("user/-/label/", "")
}
