package fr.chenry.android.freshrss.components.navigationdrawer

import android.os.Bundle
import androidx.annotation.VisibleForTesting
import androidx.preference.ListPreference
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import androidx.preference.SwitchPreferenceCompat
import fr.chenry.android.freshrss.R
import fr.chenry.android.freshrss.utils.requireF

class SettingsFragment : PreferenceFragmentCompat(), Preference.OnPreferenceChangeListener {
    @VisibleForTesting
    val refreshFrequencyPreference by lazy {
        findPreference<ListPreference>(requireF().preferences.refreshFrequencyKey)!!
    }

    @VisibleForTesting
    val retainScrollPositionPreference by lazy {
        findPreference<SwitchPreferenceCompat>(requireF().preferences.retainScrollPositionKey)!!
    }

    @VisibleForTesting
    val debugModePreference by lazy {
        findPreference<SwitchPreferenceCompat>(requireF().preferences.debugModeKey)!!
    }

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        addPreferencesFromResource(R.xml.preference_screen)
        refreshFrequencyPreference.onPreferenceChangeListener = this
        retainScrollPositionPreference.onPreferenceChangeListener = this
        debugModePreference.onPreferenceChangeListener = this
    }

    override fun onResume() {
        super.onResume()
        setRefreshFrequencyPreference(requireF().preferences.refreshFrequency.toString())
        setRetainScrollPositionPreference(requireF().preferences.retainScrollPosition)
    }

    override fun onPreferenceChange(preference: Preference, newValue: Any): Boolean = when (preference.key) {
        refreshFrequencyPreference.key -> setRefreshFrequencyPreference(newValue.toString())
        retainScrollPositionPreference.key -> setRetainScrollPositionPreference(newValue as Boolean)
        debugModePreference.key -> setDebugModePreference(newValue as Boolean)
        else -> false
    }

    private fun setRefreshFrequencyPreference(newValue: String): Boolean {
        requireF().preferences.refreshFrequency = newValue.toInt()

        refreshFrequencyPreference.findIndexOfValue(newValue).let {
            val defaultValue = requireF().getString(R.string.refresh_frequency_30m)
            val newValueStr = refreshFrequencyPreference.entries.getOrNull(it) ?: defaultValue
            refreshFrequencyPreference.title = requireF().getString(R.string.refresh_frequency_title, newValueStr)
        }
        return true
    }

    private fun setRetainScrollPositionPreference(newValue: Boolean): Boolean {
        requireF().preferences.retainScrollPosition = newValue
        retainScrollPositionPreference.isChecked = newValue
        return true
    }

    private fun setDebugModePreference(newValue: Boolean): Boolean {
        requireF().preferences.debugMode = newValue
        debugModePreference.isChecked = newValue
        return true
    }
}
