package fr.chenry.android.freshrss.components.articles.webviewutils.chunkfilters

import com.x5.template.Chunk
import com.x5.template.filters.FilterArgs
import com.x5.template.filters.LetterCaseFilter
import com.x5.template.filters.ObjectFilter
import java.util.Locale
import kotlin.text.RegexOption.IGNORE_CASE

object ChunkFilters {
    private val excludedChars = listOf("'", ":")

    // Will match any separator token, i.e, any word not containing at least a letter or a number
    val sepToken = "[\\S&&[^\\p{L}]&&[^\\d]${excludedChars.joinToString(separator = "") { "&&[^$it]" }}]"
}

class SentenceCapFilter : ObjectFilter() {
    override fun getFilterName() = "sentence_cap"

    override fun transformObject(chunk: Chunk, `object`: Any, args: FilterArgs) =
        `object`.toString().let(::sentenceCap)

    companion object {
        fun sentenceCap(token: String): String {
            var result = token
            token.split("\\s+${ChunkFilters.sepToken}+\\s+".toRegex())
                .map { t ->
                    when {
                        t.isBlank() -> t
                        t.length == 1 -> t[0].toString().uppercase(Locale.ROOT)
                        else -> "${t[0].toString().uppercase(Locale.ROOT)}${t.substring(1).lowercase(Locale.ROOT)}"
                    }
                }.forEach { t -> result = result.replace(t, t, ignoreCase = true) }
            return result.trim()
        }
    }
}

class RemoveFeedTitleFilter : ObjectFilter() {
    override fun getFilterName() = "remove_feed_title"

    override fun transformObject(chunk: Chunk, `object`: Any, args: FilterArgs) =
        stripFragment(`object`.toString(), args.getFilterArgs(chunk)?.getOrNull(0) ?: "")

    companion object {
        /**
         * Will remove the author from the title and any separator character with it.
         * For instance:
         *    >>>> stripFragment("My title - Author", "Author")
         *    "My title"
         *    >>>> stripFragment("Author | My title", "Author"))
         *    "My title"
         *    >>>> stripFragment("Series | Author | My title", "Author")
         *    "Series | My title"
         */
        fun stripFragment(title: String, author: String): String {
            val sep = ChunkFilters.sepToken
            return when {
                // Case where author is in the middle of the title, for instance: "Video series #1 | Author | Title"
                title.contains("\\s*$sep+\\s*$author\\s*$sep+\\s*".toRegex(IGNORE_CASE)) ->
                    "\\s*$sep*\\s*$author".toRegex(IGNORE_CASE).replace(title, "")
                else ->
                    "\\s*$sep*\\s*$author\\s*$sep*\\s*".toRegex(IGNORE_CASE).replace(title, "")
            }.trim()
        }
    }
}

open class LetterCaseFilter2 : LetterCaseFilter() {
    override fun transformText(chunk: Chunk, text: String, args: FilterArgs): String {
        var op = OP_UPPER
        val filterName = args.filterName

        if (filterName == "lower" || filterName == "lc") op = OP_LOWER
        else if (filterName == "capitalize" || filterName == "cap") op = OP_CAPITALIZE
        else if (filterName == "title") op = OP_TITLE

        val locale = chunk.locale?.javaLocale ?: Locale.ROOT

        return when (op) {
            OP_UPPER -> text.uppercase(locale)
            OP_LOWER -> text.lowercase(locale)
            OP_CAPITALIZE -> capitalize(text, locale, false)
            OP_TITLE -> capitalize(text, locale, true)
            else -> ""
        }
    }

    private fun capitalize(text: String, locale: Locale, lcFirst: Boolean): String {
        val chars = if (lcFirst) text.lowercase(locale).toCharArray() else text.toCharArray()
        var found = false

        chars.indices.forEach { i ->
            if (!found && chars[i].isLetter()) {
                chars[i] = chars[i].toString().uppercase(locale)[0]
                found = true
            } else if (chars[i].isWhitespace() || chars[i] == '.') {
                found = false
            }
        }

        return String(chars)
    }

    companion object {
        private const val OP_UPPER = 0
        private const val OP_LOWER = 1
        private const val OP_CAPITALIZE = 2
        private const val OP_TITLE = 4
    }
}
