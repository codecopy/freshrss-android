package fr.chenry.android.freshrss.store.viewmodels

import android.app.Application
import android.text.SpannableStringBuilder
import androidx.annotation.VisibleForTesting
import androidx.core.text.bold
import androidx.core.text.italic
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.map
import fr.chenry.android.freshrss.R
import fr.chenry.android.freshrss.store.api.ConnectionWrapper
import fr.chenry.android.freshrss.store.api.GOOGLE_API_ENDPOINT
import fr.chenry.android.freshrss.utils.requireF
import java.net.IDN
import java.net.URLEncoder

class LoginVM(app: Application) : AndroidViewModel(app) {
    /*
     * Using `resources.getStringArray` and `getString` is most of the time
     * an absolute no-go as this could cause problems with localization changes.
     * See https://medium.com/androiddevelopers/locale-changes-and-the-androidviewmodel-antipattern-84eb677660d9
     * Here, these strings aren't localized at all so it is ok to do so.
     */
    val authorizedProtocols: Array<String> = requireF().resources.getStringArray(R.array.connection_options)

    // *** Field values *** //
    val protocol = MutableLiveData(authorizedProtocols[0])
    val instanceDomain = MutableLiveData("")
    val username = MutableLiveData("")
    val password = MutableLiveData("")

    private val computedEndPoint = instanceDomain.map(::instanceDomainTransform)

    val completeApiUrlWithStyle = MediatorLiveData<CharSequence>().apply {
        fun addSourceCb(@Suppress("UNUSED_PARAMETER") s: String) = postValue(recomputeSpan())
        addSource(protocol, ::addSourceCb)
        addSource(instanceDomain, ::addSourceCb)
        addSource(computedEndPoint, ::addSourceCb)
    }

    val loginResult = ConnectionWrapper.LoginResultHandler()

    val completeApiUrl inline get() = completeApiUrlWithStyle.value?.toString() ?: ""

    @VisibleForTesting
    fun instanceDomainTransform(it: String) = when {
        it.endsWith("/api/greader.php") -> ""
        it.endsWith("/api/") -> "greader.php"
        it.endsWith("/api") -> "/greader.php"
        it.endsWith("/") -> GOOGLE_API_ENDPOINT
        else -> "/$GOOGLE_API_ENDPOINT"
    }

    /**
     * This applies a a bold style to parts of the a URL to let
     * the user be aware of the pat that were auto-computed.
     */
    @VisibleForTesting
    fun recomputeSpan() = SpannableStringBuilder().apply {
        append(protocol.value!!)

        if (instanceDomain.value.isNullOrBlank()) italic { append(requireF().getString(R.string.instance_exemple)) }
        else bold {
            val chunks = instanceDomain.value!!.replace("\\s+".toRegex(), "").split("/")
            val sanitizedDomain = runCatching { IDN.toASCII(chunks[0]) }.getOrElse { "???" }
            append(sanitizedDomain)
            if (chunks.size > 1) (1 until chunks.size).forEach { append("/${URLEncoder.encode(chunks[it], "UTF-8")}") }
        }

        if (!computedEndPoint.value.isNullOrBlank()) append(computedEndPoint.value!!)
    }
}
