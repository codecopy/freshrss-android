package fr.chenry.android.freshrss.store.api.models

import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import fr.chenry.android.freshrss.store.api.utils.ContentItemsDeserializer
import fr.chenry.android.freshrss.store.api.utils.HtmlEntitiesDeserializer
import fr.chenry.android.freshrss.store.api.utils.MicroSecTimestampDeserializer
import fr.chenry.android.freshrss.store.api.utils.MilliSecTimestampDeserializer
import fr.chenry.android.freshrss.store.api.utils.PublishedDateDeserializer
import org.joda.time.LocalDateTime

data class ContentItemsHandler(
    val id: String,
    val updated: Long,
    @JsonDeserialize(using = ContentItemsDeserializer::class)
    val items: ContentItems,
    val continuation: String = ""
)

typealias ContentItems = List<ContentItem>

data class ContentItem(
    val id: String,
    @JsonProperty("crawlTimeMsec")
    @JsonDeserialize(using = MilliSecTimestampDeserializer::class)
    val crawled: LocalDateTime,
    @JsonProperty("timestampUsec")
    @JsonDeserialize(using = MicroSecTimestampDeserializer::class)
    val timestamp: LocalDateTime,
    @JsonDeserialize(using = PublishedDateDeserializer::class)
    val published: LocalDateTime,
    @JsonDeserialize(using = HtmlEntitiesDeserializer::class)
    val title: String,
    val alternate: List<Href>,
    val categories: List<String>,
    val origin: ContentItemOrigin,
    val summary: Summary,
    @JsonDeserialize(using = HtmlEntitiesDeserializer::class)
    val author: String = ""
)

data class ContentItemOrigin(
    val streamId: String,
    val title: String
)

data class Summary(val content: String)

data class Href(val href: String)
