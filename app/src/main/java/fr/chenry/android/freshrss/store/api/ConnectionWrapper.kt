package fr.chenry.android.freshrss.store.api

import android.content.Context
import fr.chenry.android.freshrss.store.api.utils.NoNetworkException
import fr.chenry.android.freshrss.store.database.models.Account
import fr.chenry.android.freshrss.store.database.models.TokenResponse
import fr.chenry.android.freshrss.store.viewmodels.LoginVM
import fr.chenry.android.freshrss.utils.SafeResult
import fr.chenry.android.freshrss.utils.e
import fr.chenry.android.freshrss.utils.fold
import fr.chenry.android.freshrss.utils.getOrThrow
import fr.chenry.android.freshrss.utils.isConnectedToNetwork
import fr.chenry.android.freshrss.utils.safeResult
import retrofit2.HttpException
import java.util.concurrent.locks.ReentrantLock
import kotlin.concurrent.withLock

/**
 * This class wraps the connection API and manages the corner cases
 * of connection.
 */
class ConnectionWrapper(
    private val context: Context,
    private val viewModel: LoginVM,
) {
    private lateinit var connectionApi: ConnectionAPI

    suspend fun checkApiUrl(): ApiUrlValidationResult {
        val instanceUrl = viewModel.completeApiUrl
        try {
            connectionApi = ConnectionAPI.get(instanceUrl)
        } catch (e: Throwable) {
            return ApiUrlValidationResult.InvalidUrlError
        }

        val result = safeNetworkCall(connectionApi::getApiValidation)

        if (result.isSuccess) {
            val response = result.getOrThrow()
            return if (response == API_URL_OK_RESPONSE) ApiUrlValidationResult.Validated
            else ApiUrlValidationResult.InvalidResponseError
        }

        val exception = result.exceptionOrNull()!!

        if (exception !is HttpException) return ApiUrlValidationResult.UnknownError(exception)

        /*
         * Corner case: a common error among self-hosted FreshRSS users  is to expose
         * the FreshRSS root directory to the web instead of only exposing the /p/
         * folder. In this case, FreshRSS endup being exposed on the `/p/` route instead of `/`.
         * As this is a common error, it need to be tested directly here.
         */
        val instanceDomainWithP = withP()
        val connectionApiWithP = ConnectionAPI.get(instanceDomainWithP)
        val resultP = safeNetworkCall(connectionApiWithP::getApiValidation)

        if (resultP.isSuccess) {
            connectionApi = connectionApiWithP
            val oldApiUrl = viewModel.completeApiUrl
            viewModel.instanceDomain.postValue(instanceDomainWithP.substring(viewModel.protocol.value!!.length))

            val response = resultP.getOrThrow()
            return if (response == API_URL_OK_RESPONSE) ApiUrlValidationResult.RootExposedWarning(oldApiUrl)
            else ApiUrlValidationResult.InvalidResponseError
        }

        // Finally, if testing /p/ also failed, we return the original exception
        return ApiUrlValidationResult.UnknownError(exception)
    }

    suspend fun attemptLogin(): LoginResult {
        // That should not happen but in the very weird cas it happens I pefer the report to be very clear
        if (!::connectionApi.isInitialized) throw NoConnectionApiException()

        fun onFailure(it: Throwable): LoginResult {
            this.e(it)
            if (it !is HttpException) return LoginResult.error(it)

            return when (it.code()) {
                401 -> LoginResult.badCredentials()
                503 -> LoginResult.apiAccessDisabled()
                else -> LoginResult.error(it)
            }
        }

        fun onSuccess(it: TokenResponse) = LoginResult.ok(
            SID = it.SID,
            Auth = it.Auth,
            login = viewModel.username.value!!,
            serverInstance = viewModel.completeApiUrl
        )

        return safeNetworkCall {
            val result = connectionApi.login(viewModel.username.value!!, viewModel.password.value!!)
            result
        }.fold(::onSuccess, ::onFailure)
    }

    /**
     * This function adds /p/ to the instance path.
     * This is a common error among users installing FreshRSS:
     * they fail to expose the correct FreshRSS directory (/p/).
     * Instead they expose its parent resulting FreshRSS being
     * accessible on /p/ instead of /. This is a very tricky error
     * to diagnose and the app should auto-detect this kind of misconfigurations.
     */
    private fun withP(): String {
        val base = viewModel.completeApiUrl.replace("/${GOOGLE_API_ENDPOINT}\$".toRegex(), "")
        return "$base/p/"
    }

    private suspend inline fun <T : Any> safeNetworkCall(
        crossinline block: suspend () -> T
    ): SafeResult<T> = safeResult {
        if (!context.isConnectedToNetwork()) throw NoNetworkException()
        block()
    }

    sealed /* TODO 1.5 interface */ class ApiUrlValidationResult {
        object Validated : ApiUrlValidationResult()
        object InvalidUrlError : ApiUrlValidationResult()
        class RootExposedWarning(val previousApiUrl: String) : ApiUrlValidationResult()
        object InvalidResponseError : ApiUrlValidationResult()
        class UnknownError(val exception: Throwable) : ApiUrlValidationResult()
    }

    sealed class LoginResult {
        class Ok(val account: Account) : LoginResult()
        object NoPriorOperation : LoginResult()
        object ApiAccessDisabled : LoginResult()
        object BadCredentials : LoginResult()
        class UnknownError(val exception: Throwable) : LoginResult()

        companion object {
            fun ok(
                SID: String,
                Auth: String,
                login: String,
                serverInstance: String
            ) = Ok(Account(SID, Auth, login, serverInstance))

            fun error(exception: Throwable) = UnknownError(exception)
            fun apiAccessDisabled() = ApiAccessDisabled
            fun badCredentials() = BadCredentials
        }
    }

    class NoConnectionApiException : Exception(
        "${ConnectionWrapper::class.simpleName}.${ConnectionWrapper::checkApiUrl.name} was not called " +
            "before ${ConnectionWrapper::class.simpleName}.${ConnectionWrapper::attemptLogin.name}"
    )

    class LoginResultHandler {
        private var element: LoginResult? = null
        private val lock = ReentrantLock()

        fun push(result: LoginResult) = lock.withLock {
            element = result
        }

        fun pop(): LoginResult = lock.withLock {
            val result = element
            element = null
            result ?: LoginResult.NoPriorOperation
        }
    }
}
