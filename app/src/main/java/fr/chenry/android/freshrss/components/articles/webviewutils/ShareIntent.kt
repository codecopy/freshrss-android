package fr.chenry.android.freshrss.components.articles.webviewutils

import android.content.Intent
import android.content.Intent.ACTION_SEND
import android.content.Intent.EXTRA_TEXT
import androidx.annotation.VisibleForTesting
import com.x5.template.Theme
import fr.chenry.android.freshrss.components.articles.webviewutils.chunkfilters.LetterCaseFilter2
import fr.chenry.android.freshrss.components.articles.webviewutils.chunkfilters.RemoveFeedTitleFilter
import fr.chenry.android.freshrss.components.articles.webviewutils.chunkfilters.SentenceCapFilter
import fr.chenry.android.freshrss.store.database.models.Article

object ShareIntent {
    private val defaultTemplate = """
                |{${'$'}title|remove_feed_title(${'$'}subscription)|sentence_cap} — {${'$'}subscription|capitalize}

                |{${'$'}href}
    """.trimMargin("|")

    private val template = Theme().let {
        it.registerFilter(SentenceCapFilter())
        it.registerFilter(RemoveFeedTitleFilter())
        it.registerFilter(LetterCaseFilter2())
        it.makeChunk().apply { append(defaultTemplate) }
    }

    @VisibleForTesting
    fun getTemplateAttributes(feedTitle: String, article: Article) = mapOf(
        "subscription" to feedTitle,
        "author" to article.author,
        "title" to article.title,
        "content" to article.content,
        "href" to article.href
    )

    fun format(attributes: Map<String, String>) = template.apply {
        attributes.entries.forEach { (t, u) -> set(t, u) }
    }.toString()

    fun create(feedTitle: String, article: Article) = Intent(ACTION_SEND).apply {
        type = "text/plain"
        putExtra(EXTRA_TEXT, format(getTemplateAttributes(feedTitle, article)))
    }
}
