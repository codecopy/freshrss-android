package fr.chenry.android.freshrss.store.viewmodels

import android.content.Context
import fr.chenry.android.freshrss.R
import fr.chenry.android.freshrss.store.database.models.Article
import fr.chenry.android.freshrss.store.database.models.Feed
import fr.chenry.android.freshrss.store.database.models.FeedCategory
import org.joda.time.LocalDateTime
import java.util.Locale

object LabelComparator : Comparator<FeedCategory> {
    override fun compare(o1: FeedCategory, o2: FeedCategory): Int = o1.label.compareTo(o2.label, true)
}

object TitleComparator : Comparator<Feed> {
    override fun compare(o1: Feed, o2: Feed): Int = o1.title.compareTo(o2.title, true)
}

object DateComparator : Comparator<Feed> {
    override fun compare(o1: Feed, o2: Feed): Int =
        if (o1.newestArticleDate.isEqual(o2.newestArticleDate)) o1.title.compareTo(o2.title, true)
        else o2.newestArticleDate.compareTo(o1.newestArticleDate)
}

object ArticleComparator : Comparator<Article> {
    override fun compare(o1: Article, o2: Article): Int =
        if (o1.published.isEqual(o2.published)) o1.title.compareTo(o2.title, ignoreCase = true)
        else o2.published.compareTo(o1.published)
}

interface Grouper {
    operator fun invoke(context: Context, feed: Feed): String
}

object TitleGrouper : Grouper {
    override operator fun invoke(
        context: Context,
        feed: Feed
    ): String = feed.title.getOrElse(0) { '#' }.toString().uppercase(Locale.getDefault())
}

object DateGrouper : Grouper {
    override operator fun invoke(context: Context, feed: Feed): String {
        val today = LocalDateTime.now().withTime(0, 0, 0, 0)
        return when {
            feed.newestArticleDate.isAfter(today.minusDays(1)) ->
                R.string.human_time_grouping_today
            feed.newestArticleDate.isAfter(today.minusDays(2)) ->
                R.string.human_time_grouping_yesterday
            feed.newestArticleDate.isAfter(today.withDayOfWeek(1)) ->
                R.string.human_time_grouping_this_week
            feed.newestArticleDate.isAfter(today.withDayOfWeek(1).minusWeeks(1)) ->
                R.string.human_time_grouping_last_week
            feed.newestArticleDate.isAfter(today.withDayOfMonth(1)) ->
                R.string.human_time_grouping_this_month
            feed.newestArticleDate.isAfter(today.withDayOfMonth(1).minusMonths(1)) ->
                R.string.human_time_grouping_last_month
            feed.newestArticleDate.isAfter(today.withDayOfYear(1)) ->
                R.string.human_time_grouping_this_year
            else -> R.string.human_time_grouping_old_articles
        }.let(context::getString)
    }
}
