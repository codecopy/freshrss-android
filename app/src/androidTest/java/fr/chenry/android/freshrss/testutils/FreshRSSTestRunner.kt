package fr.chenry.android.freshrss.testutils

import android.app.Application
import android.content.Context
import androidx.room.Room
import androidx.test.runner.AndroidJUnitRunner
import fr.chenry.android.freshrss.FreshRSSApplication
import fr.chenry.android.freshrss.store.database.FreshRSSDabatabase
import fr.chenry.android.freshrss.testutils.factories.ArticleFactory
import fr.chenry.android.freshrss.testutils.factories.FeedFactory
import fr.chenry.android.freshrss.utils.CustomDispatchers
import fr.chenry.android.freshrss.utils.NOOP
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import org.koin.core.module.Module
import org.koin.dsl.module

class FreshRSSTestRunner : AndroidJUnitRunner() {
    override fun newApplication(cl: ClassLoader, className: String, context: Context): Application =
        super.newApplication(cl, FreshRSSTestApplication::class.java.name, context)

    class FreshRSSTestApplication : FreshRSSApplication() {
        val feed = FeedFactory.createFeeds(1)[0]
        val article = ArticleFactory.createArticle(streamId = feed.id)

        @OptIn(ExperimentalCoroutinesApi::class)
        override val applicationScope: CoroutineScope = CoroutineScope(SupervisorJob() + UnconfinedTestDispatcher())

        override val db: FreshRSSDabatabase by lazy {
            this.deleteDatabase(TEST_DB_NAME)
            Room.databaseBuilder(this, FreshRSSDabatabase::class.java, TEST_DB_NAME).build().apply {
                runBlocking {
                    syncSubscriptions(mapOf(feed.id to feed))
                    forceInsertArticles(listOf(article))
                }
            }
        }

        override fun acraInit() = NOOP()

        @OptIn(ExperimentalCoroutinesApi::class)
        override fun koinInit(vararg modules: Module) {
            super.koinInit(
                module {
                    single {
                        CustomDispatchers(UnconfinedTestDispatcher(), UnconfinedTestDispatcher())
                    }
                }
            )
        }

        companion object {
            const val TEST_DB_NAME = "freshrss-test.db"
        }
    }
}
