package fr.chenry.android.freshrss.store.database

import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import org.junit.Test
import org.junit.runner.RunWith
import kotlin.test.assertContentEquals
import kotlin.test.assertEquals

@RunWith(AndroidJUnit4::class)
@SmallTest
class FreshRSSDabatabaseMigrationsTest : FreshRSSDabatabaseBaseTest() {
    @Test
    fun allMigrations() {
        createDatabaseFromScratch()
    }

    @Test
    fun testMigrate0to2() {
        createDatabaseFromScratch(2)
        database.rawQuery("SELECT * FROM subscriptions", arrayOf()).use { query ->
            assertEquals(5, query.count)
            assertEquals(3, query.columnCount)

            assertEquals(true, query.moveToFirst())

            assertEquals("2c21b3b1-8297-445f-a522-2fe8d3ff5083", query.getString(0))
            assertEquals("Subscription 1", query.getString(1))
            assertEquals(2, query.getInt(2))

            assertEquals(true, query.moveToLast())

            assertEquals("54f869cf-a764-4cb5-a77e-c46d8ca798e4", query.getString(0))
            assertEquals("Subscription 5", query.getString(1))
            assertEquals(0, query.getInt(2))
        }

        database.close()
    }

    @Test
    fun testMigrate2to3() {
        createDatabaseFromScratch(3)

        database.rawQuery("SELECT * FROM subscriptions", arrayOf()).use {
            assertEquals(5, it.count)
            assertContentEquals(
                arrayOf("id", "title", "unreadCount", "imageBitmap", "iconUrl"),
                it.columnNames
            )

            assertEquals(true, it.moveToFirst())

            assertEquals(null, it.getBlob(3))
            assertEquals("", it.getString(4))
        }

        val bindArgs = arrayOf("54f869cf-a764-4cb5-a77e-c46d8ca798e4")
        database.execSQL(
            "UPDATE subscriptions SET iconUrl = 'https://example.com/icon' WHERE id = ?",
            bindArgs
        )
        database.rawQuery("SELECT * FROM subscriptions WHERE id = ?", bindArgs).use {
            it.moveToFirst()
            assertEquals("https://example.com/icon", it.getString(4))
        }

        database.close()
    }

    @Test
    fun testMigrate6to7() {
        createDatabaseFromScratch(6)

        val migration = FreshRSSDabatabase_Migrations.build()[5]
        database.rawQuery("SELECT * FROM accounts", arrayOf()).use {
            assertEquals(4, it.count)
            it.moveToFirst()
            assertEquals("rss.user.tld", it.getString(it.getColumnIndex("serverInstance")))
            it.moveToNext()
            assertEquals("http://rss.user.tld", it.getString(it.getColumnIndex("serverInstance")))
            it.moveToNext()
            assertEquals(
                "https://rss.user.tld/api/greader.php",
                it.getString(it.getColumnIndex("serverInstance"))
            )
            it.moveToNext()
            assertEquals(
                "rss.user.tld/p/api/greader.php",
                it.getString(it.getColumnIndex("serverInstance"))
            )
        }

        helper.runMigrationsAndValidate(testDB, migration.endVersion, true, migration)

        database.rawQuery("SELECT * FROM accounts", arrayOf()).use {
            assertEquals(4, it.count)
            it.moveToFirst()
            assertEquals(
                "https://rss.user.tld/api/greader.php",
                it.getString(it.getColumnIndex("serverInstance"))
            )
            it.moveToNext()
            assertEquals(
                "http://rss.user.tld/api/greader.php",
                it.getString(it.getColumnIndex("serverInstance"))
            )
            it.moveToNext()
            assertEquals(
                "https://rss.user.tld/api/greader.php",
                it.getString(it.getColumnIndex("serverInstance"))
            )
            it.moveToNext()
            assertEquals(
                "https://rss.user.tld/p/api/greader.php",
                it.getString(it.getColumnIndex("serverInstance"))
            )
        }
    }
}
