package fr.chenry.android.freshrss.components.navigationdrawer

import androidx.core.content.edit
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.recyclerview.widget.RecyclerView
import androidx.test.core.app.launchActivity
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.DrawerActions
import androidx.test.espresso.contrib.DrawerMatchers
import androidx.test.espresso.contrib.RecyclerViewActions.actionOnItem
import androidx.test.espresso.matcher.ViewMatchers.assertThat
import androidx.test.espresso.matcher.ViewMatchers.hasDescendant
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import fr.chenry.android.freshrss.R
import fr.chenry.android.freshrss.activities.MainActivity
import fr.chenry.android.freshrss.testutils.FreshRSSBaseTest
import fr.chenry.android.freshrss.utils.requireF
import org.hamcrest.Matchers.equalTo
import org.hamcrest.Matchers.matchesPattern
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import kotlin.test.assertEquals
import kotlin.test.assertFalse

@RunWith(AndroidJUnit4::class)
@LargeTest
class SettingsFragmentTest : FreshRSSBaseTest() {
    @Before
    fun setUp() = application.preferences.sharedPreferences.edit { clear() }

    @Test
    fun `should-navigate-to-settings-fragment`() {
        val scenario = launchActivity<MainActivity>()

        onView(withId(R.id.activity_main_navigation_drawer)).perform(DrawerActions.open())
        onView(withId(R.id.activity_main_navigation_drawer)).check(matches(DrawerMatchers.isOpen()))
        onView(withText(R.string.title_settings)).perform(click())

        scenario.onActivity {
            assertEquals(
                it.navigation.currentDestination!!,
                it.navigation.graph.findNode(R.id.settingsFragment)
            )
        }
    }

    @Test
    fun `should-correctly-initialize-default-values`() {
        launchFragmentInContainer<SettingsFragment>(themeResId = R.style.AppTheme).onFragment { fragment ->
            val expected = fragment.getString(
                R.string.refresh_frequency_title,
                fragment.getString(R.string.refresh_frequency_30m)
            ) as CharSequence

            val expected2 = fragment.refreshFrequencyPreference.entries
                .indexOfFirst { it == fragment.getString(R.string.refresh_frequency_30m) }
                .let { fragment.refreshFrequencyPreference.entryValues.getOrNull(it) }

            assertThat(fragment.refreshFrequencyPreference.title, equalTo(expected))
            assertThat(fragment.refreshFrequencyPreference.value, equalTo(expected2))

            assertThat(fragment.retainScrollPositionPreference.isChecked, equalTo(true))
        }
    }

    @Test
    fun `should-correctly-modify-settings`() {
        val scenario = launchFragmentInContainer<SettingsFragment>(themeResId = R.style.AppTheme)

        // https://stackoverflow.com/a/57342588
        onView(withId(androidx.preference.R.id.recycler_view)).perform(
            actionOnItem<RecyclerView.ViewHolder>(
                hasDescendant(withText(matchesPattern(context.getString(R.string.refresh_frequency_title, ".*")))),
                click()
            )
        )

        onView(withText(R.string.refresh_frequency_5h)).perform(click())

        scenario.onFragment { fragment ->
            val expected = fragment.getString(
                R.string.refresh_frequency_title,
                fragment.getString(R.string.refresh_frequency_5h)
            ) as CharSequence

            val expected2 = fragment.refreshFrequencyPreference.entries
                .indexOfFirst { it == fragment.getString(R.string.refresh_frequency_5h) }
                .let { fragment.refreshFrequencyPreference.entryValues.getOrNull(it) }

            assertEquals(fragment.refreshFrequencyPreference.title, expected)
            assertEquals(fragment.refreshFrequencyPreference.value, expected2)
            assertEquals(fragment.requireF().preferences.refreshFrequency, 300)
        }

        onView(withText(context.getString(R.string.retain_scroll_position_preference_title))).perform(click())

        scenario.onFragment { fragment ->
            assertFalse(fragment.retainScrollPositionPreference.isChecked)
            assertFalse(fragment.requireF().preferences.retainScrollPosition)
        }
    }
}
