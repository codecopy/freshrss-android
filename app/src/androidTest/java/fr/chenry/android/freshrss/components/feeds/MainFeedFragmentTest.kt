package fr.chenry.android.freshrss.components.feeds

import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.swipeLeft
import androidx.test.espresso.action.ViewActions.swipeRight
import androidx.test.espresso.matcher.ViewMatchers.isDescendantOfA
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.bottomnavigation.BottomNavigationView
import fr.chenry.android.freshrss.R
import fr.chenry.android.freshrss.testutils.FreshRSSBaseTest
import fr.chenry.android.freshrss.testutils.factories.FeedFactory
import fr.chenry.android.freshrss.testutils.matchers.check
import fr.chenry.android.freshrss.utils.ViewPagerIdlingResource
import io.kotest.matchers.ints.shouldBeExactly
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.hamcrest.Matchers.allOf
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@OptIn(ExperimentalCoroutinesApi::class)
@RunWith(AndroidJUnit4::class)
@SmallTest
class MainFeedFragmentTest : FreshRSSBaseTest() {
    private val feeds = FeedFactory.createFeeds(3)
    private lateinit var viewPagerIdlingResource: ViewPagerIdlingResource

    @Before
    fun setUp() = runTest {
        application.db.syncSubscriptions(feeds.associateBy { it.id })
    }

    @Test
    fun `test-bottom-navigation`() {
        clearSharedPrefs()

        launchFragmentInContainer<MainFeedFragment>(themeResId = R.style.AppTheme).onFragment {
            viewPagerIdlingResource = ViewPagerIdlingResource(it.activity!!.findViewById(R.id.feed_view_pager))
            IdlingRegistry.getInstance().register(viewPagerIdlingResource)
        }

        // / Testing bottom bar buttons
        onView(withId(R.id.feed_bottom_navigation)).check<BottomNavigationView> {
            it.selectedItemId shouldBeExactly FeedSection.ALL.navigationButtonId
        }

        onView(withId(R.id.feed_view_pager)).check<ViewPager2> {
            it.currentItem shouldBeExactly FeedSection.ALL.ordinal
        }

        onView(
            allOf(
                withText(R.string.title_favorites),
                isDescendantOfA(withId(R.id.feed_bottom_navigation)),
                isDisplayed()
            )
        ).perform(click())

        onView(withId(R.id.feed_bottom_navigation)).check<BottomNavigationView> {
            it.selectedItemId shouldBeExactly FeedSection.FAVORITES.navigationButtonId
        }

        onView(withId(R.id.feed_view_pager)).check<ViewPager2> {
            it.currentItem shouldBeExactly FeedSection.FAVORITES.ordinal
        }

        onView(
            allOf(
                withText(R.string.title_unread),
                isDescendantOfA(withId(R.id.feed_bottom_navigation)),
                isDisplayed()
            )
        ).perform(click())

        onView(withId(R.id.feed_bottom_navigation)).check<BottomNavigationView> {
            it.selectedItemId shouldBeExactly FeedSection.UNREAD.navigationButtonId
        }

        onView(withId(R.id.feed_view_pager)).check<ViewPager2> {
            it.currentItem shouldBeExactly FeedSection.UNREAD.ordinal
        }

        // / Testing swipe
        onView(withId(R.id.feed_view_pager)).perform(swipeRight()).check<ViewPager2> {
            it.currentItem shouldBeExactly FeedSection.ALL.ordinal
        }

        onView(withId(R.id.feed_bottom_navigation)).check<BottomNavigationView> {
            it.selectedItemId shouldBeExactly FeedSection.ALL.navigationButtonId
        }

        onView(withId(R.id.feed_view_pager)).perform(swipeRight()).check<ViewPager2> {
            it.currentItem shouldBeExactly FeedSection.FAVORITES.ordinal
        }

        onView(withId(R.id.feed_bottom_navigation)).check<BottomNavigationView> {
            it.selectedItemId shouldBeExactly FeedSection.FAVORITES.navigationButtonId
        }

        onView(withId(R.id.feed_view_pager)).perform(swipeLeft()).check<ViewPager2> {
            it.currentItem shouldBeExactly FeedSection.ALL.ordinal
        }

        onView(withId(R.id.feed_bottom_navigation)).check<BottomNavigationView> {
            it.selectedItemId shouldBeExactly FeedSection.ALL.navigationButtonId
        }

        onView(withId(R.id.feed_view_pager)).perform(swipeLeft()).check<ViewPager2> {
            it.currentItem shouldBeExactly FeedSection.UNREAD.ordinal
        }

        onView(withId(R.id.feed_bottom_navigation)).check<BottomNavigationView> {
            it.selectedItemId shouldBeExactly FeedSection.UNREAD.navigationButtonId
        }

        IdlingRegistry.getInstance().unregister(viewPagerIdlingResource)
    }

    @Test
    fun `test-section-is-remembered-across-restart`() {
        clearSharedPrefs()

        val fragmentScenario = launchFragmentInContainer<MainFeedFragment>(themeResId = R.style.AppTheme)

        onView(withId(R.id.feed_bottom_navigation)).check<BottomNavigationView> {
            it.selectedItemId shouldBeExactly FeedSection.ALL.navigationButtonId
        }

        onView(withId(R.id.feed_view_pager)).check<ViewPager2> {
            it.currentItem shouldBeExactly FeedSection.ALL.ordinal
        }

        onView(
            allOf(
                withText(R.string.title_favorites),
                isDescendantOfA(withId(R.id.feed_bottom_navigation)),
                isDisplayed()
            )
        ).perform(click())

        onView(withId(R.id.feed_bottom_navigation)).check<BottomNavigationView> {
            it.selectedItemId shouldBeExactly FeedSection.FAVORITES.navigationButtonId
        }

        onView(withId(R.id.feed_view_pager)).check<ViewPager2> {
            it.currentItem shouldBeExactly FeedSection.FAVORITES.ordinal
        }

        fragmentScenario.recreate()

        onView(withId(R.id.feed_bottom_navigation)).check<BottomNavigationView> {
            it.selectedItemId shouldBeExactly FeedSection.FAVORITES.navigationButtonId
        }

        onView(withId(R.id.feed_view_pager)).check<ViewPager2> {
            it.currentItem shouldBeExactly FeedSection.FAVORITES.ordinal
        }
    }
}
