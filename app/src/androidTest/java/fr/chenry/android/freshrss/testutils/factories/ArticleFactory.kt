package fr.chenry.android.freshrss.testutils.factories

import fr.chenry.android.freshrss.store.database.models.Article
import fr.chenry.android.freshrss.store.database.models.FavoriteStatus
import fr.chenry.android.freshrss.store.database.models.ReadStatus
import fr.chenry.android.freshrss.testutils.TestUtils
import org.joda.time.LocalDateTime
import java.util.concurrent.TimeUnit

object ArticleFactory {
    fun createArticle(
        id: String = TestUtils.idNumber().valid(),
        title: String = TestUtils.starTrek().character(),
        href: String =
            TestUtils.internet().url().let {
                if (!"https?://.*".toRegex().matches(it)) "https://$it" else it
            },
        author: String = TestUtils.book().author(),
        content: String = TestUtils.lorem().paragraph(5),
        streamId: String = TestUtils.idNumber().valid(),
        readStatus: ReadStatus =
            ReadStatus.values()[TestUtils.random().nextInt(ReadStatus.values().size)],
        favorite: FavoriteStatus =
            FavoriteStatus.values()[TestUtils.random().nextInt(FavoriteStatus.values().size)],
        crawled: LocalDateTime = TestUtils.jodaDate().future(10, TimeUnit.DAYS).toLocalDateTime(),
        published: LocalDateTime = TestUtils.jodaDate().future(10, TimeUnit.DAYS).toLocalDateTime(),
        scrollPosition: Int = TestUtils.random().nextInt(10_000)
    ) = Article(
        id,
        title,
        href,
        author,
        content,
        streamId,
        readStatus,
        favorite,
        crawled,
        published,
        scrollPosition
    )

    fun createArticles(number: Int) = (1..number).map { createArticle() }
}
