package fr.chenry.android.freshrss.testutils

import android.app.Activity
import android.app.Instrumentation
import android.content.Context
import android.content.res.AssetManager
import androidx.core.content.edit
import androidx.preference.PreferenceManager
import androidx.test.core.app.ApplicationProvider
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.runner.lifecycle.ActivityLifecycleMonitorRegistry
import androidx.test.runner.lifecycle.Stage

abstract class FreshRSSBaseTest {
    val application: FreshRSSTestRunner.FreshRSSTestApplication
        inline get() = ApplicationProvider.getApplicationContext()
    val instrumentation: Instrumentation
        inline get() = InstrumentationRegistry.getInstrumentation()
    val context: Context
        inline get() = instrumentation.targetContext
    val assets: AssetManager
        inline get() = instrumentation.context.assets

    // See https://stackoverflow.com/a/60341723/3459089
    fun <T : Activity> getActivityInstance(): T {
        lateinit var activity: T
        InstrumentationRegistry.getInstrumentation().runOnMainSync {
            @Suppress("UNCHECKED_CAST")
            activity = ActivityLifecycleMonitorRegistry.getInstance()
                .getActivitiesInStage(Stage.RESUMED)
                .last() as T
        }
        return activity
    }

    fun clearSharedPrefs() = PreferenceManager.getDefaultSharedPreferences(application).edit {
        clear()
    }
}
