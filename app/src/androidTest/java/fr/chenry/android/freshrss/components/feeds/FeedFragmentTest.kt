package fr.chenry.android.freshrss.components.feeds

import androidx.activity.ComponentActivity
import androidx.compose.ui.test.assertCountEquals
import androidx.compose.ui.test.hasContentDescription
import androidx.compose.ui.test.hasText
import androidx.compose.ui.test.junit4.createAndroidComposeRule
import androidx.compose.ui.test.performClick
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.navigation.Navigation
import androidx.navigation.testing.TestNavHostController
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import com.google.android.material.composethemeadapter.MdcTheme
import fr.chenry.android.freshrss.R
import fr.chenry.android.freshrss.store.database.models.FOLDER_TYPE
import fr.chenry.android.freshrss.store.database.models.FavoriteStatus
import fr.chenry.android.freshrss.store.database.models.FeedCategory
import fr.chenry.android.freshrss.store.database.models.FeedLikeDisplayable
import fr.chenry.android.freshrss.store.database.models.ReadStatus
import fr.chenry.android.freshrss.store.database.models.VoidCategory
import fr.chenry.android.freshrss.testutils.FreshRSSBaseTest
import fr.chenry.android.freshrss.testutils.TestUtils
import fr.chenry.android.freshrss.testutils.factories.ArticleFactory
import fr.chenry.android.freshrss.testutils.factories.FeedFactory
import io.kotest.matchers.shouldBe
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.joda.time.LocalDateTime
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@OptIn(ExperimentalCoroutinesApi::class)
@RunWith(AndroidJUnit4::class)
@SmallTest
class FeedFragmentTest : FreshRSSBaseTest() {
    @get:Rule
    val composeTestRule = createAndroidComposeRule<ComponentActivity>()

    private val feeds = listOf(
        FeedFactory.createFeed(
            title = "${TestUtils.lebowski().character()} (Feed 1)",
            newestArticleDate = LocalDateTime.now().minusDays(2)
        ),
        FeedFactory.createFeed(
            title = "${TestUtils.lebowski().character()} (Feed 2)",
            newestArticleDate = LocalDateTime.now().minusDays(1)
        ),
        FeedFactory.createFeed(
            title = "${TestUtils.lebowski().character()} (Feed 3)",
            newestArticleDate = LocalDateTime.now().minusYears(1)
        ),
    )

    private val categories = listOf(
        FeedCategory(
            id = TestUtils.idNumber().valid(),
            label = "Test category 1",
            type = FOLDER_TYPE
        ),
        FeedCategory(
            id = TestUtils.idNumber().valid(),
            label = "Test category 2",
            type = FOLDER_TYPE
        ),
    )

    private val articles = listOf(
        ArticleFactory.createArticle(
            streamId = feeds[0].id,
            favorite = FavoriteStatus.NOT_FAVORITE,
            readStatus = ReadStatus.READ
        ),
        ArticleFactory.createArticle(
            streamId = feeds[0].id,
            favorite = FavoriteStatus.FAVORITE,
            readStatus = ReadStatus.READ
        ),

        ArticleFactory.createArticle(
            streamId = feeds[1].id,
            favorite = FavoriteStatus.NOT_FAVORITE,
            readStatus = ReadStatus.UNREAD,
            published = LocalDateTime.now().minusDays(2)
        ),
        ArticleFactory.createArticle(
            streamId = feeds[1].id,
            favorite = FavoriteStatus.NOT_FAVORITE,
            readStatus = ReadStatus.READ
        ),

        ArticleFactory.createArticle(
            streamId = feeds[2].id,
            favorite = FavoriteStatus.FAVORITE,
            readStatus = ReadStatus.UNREAD,
            published = LocalDateTime.now().minusDays(1)
        ),
        ArticleFactory.createArticle(
            streamId = feeds[2].id,
            favorite = FavoriteStatus.NOT_FAVORITE,
            readStatus = ReadStatus.UNREAD,
            published = LocalDateTime.now().minusYears(1)
        ),
    )

    @Before
    fun setUp() = runTest {
        application.db.syncAllFeedCategories(categories)
        application.db.syncSubscriptions(feeds.associateBy { it.id })
        application.db.forceInsertArticles(articles)
        application.db.syncFeedToTagRelationForFeed(feeds[0].id, listOf(categories[0].id))
        application.db.syncFeedToTagRelationForFeed(feeds[1].id, listOf(categories[0].id))
        application.db.syncFeedToTagRelationForFeed(feeds[2].id, listOf(categories[1].id))
    }

    @Test
    fun `test-unread-content`() {
        val mock = mockk<(virtualFeed: FeedLikeDisplayable) -> Boolean>()
        every { mock(any()) } returns true

        composeTestRule.setContent {
            MdcTheme {
                MainContent(FeedSection.UNREAD, VoidCategory, mock)
            }
        }

        composeTestRule.onAllNodes(hasContentDescription("Header content")).assertCountEquals(4)
        composeTestRule.onNode(
            hasText(composeTestRule.activity.getString(R.string.subscription_categories))
        ).assertExists("No feed category")
        composeTestRule.onNode(hasText(feeds[1].title)).performClick()
        verify { mock(feeds[1]) }
    }

    @Test
    fun `test-all-content`() {
        val mock = mockk<(virtualFeed: FeedLikeDisplayable) -> Boolean>()
        every { mock(any()) } returns true

        composeTestRule.setContent {
            MdcTheme {
                MainContent(FeedSection.ALL, VoidCategory, mock)
            }
        }

        val grouped = feeds.sortedWith { o1, o2 ->
            o1.title.compareTo(o2.title, ignoreCase = true)
        }.groupBy { it.title[0] }

        composeTestRule.onAllNodes(
            hasContentDescription("Header content")
        ).assertCountEquals(grouped.keys.size + 1)

        composeTestRule.onNode(
            hasText(composeTestRule.activity.getString(R.string.subscription_categories))
        ).assertExists("No feed category")

        composeTestRule.onNode(hasText(feeds[1].title)).performClick()
        verify { mock(feeds[1]) }
    }

    @Test
    fun `test-favorite-content`() {
        val mock = mockk<(virtualFeed: FeedLikeDisplayable) -> Boolean>()
        every { mock(any()) } returns true

        composeTestRule.setContent {
            MdcTheme {
                MainContent(FeedSection.FAVORITES, VoidCategory, mock)
            }
        }

        val grouped = listOf(feeds[0], feeds[2]).sortedWith { o1, o2 ->
            o1.title.compareTo(o2.title, ignoreCase = true)
        }.groupBy { it.title[0] }

        composeTestRule.onAllNodes(
            hasContentDescription("Header content")
        ).assertCountEquals(grouped.keys.size + 1)

        composeTestRule.onNode(
            hasText(composeTestRule.activity.getString(R.string.subscription_categories))
        ).assertExists("No feed category")

        composeTestRule.onNode(hasText(feeds[2].title)).performClick()
        verify { mock(feeds[2]) }
    }

    @Test
    fun `test-category-content`() {
        val mock = mockk<(virtualFeed: FeedLikeDisplayable) -> Boolean>()
        every { mock(any()) } returns true

        composeTestRule.setContent {
            MdcTheme {
                MainContent(FeedSection.ALL, categories[0], mock)
            }
        }

        val grouped = listOf(feeds[0], feeds[1]).sortedWith { o1, o2 ->
            o1.title.compareTo(o2.title, ignoreCase = true)
        }.groupBy { it.title[0] }

        composeTestRule.onAllNodes(
            hasContentDescription("Header content")
        ).assertCountEquals(grouped.keys.size)

        composeTestRule.onNode(
            hasText(composeTestRule.activity.getString(R.string.subscription_categories))
        ).assertDoesNotExist()

        composeTestRule.onNode(hasText(feeds[1].title)).performClick()
        verify { mock(feeds[1]) }
    }

    @Test
    fun `test-navigation`() {
        val navController = TestNavHostController(ApplicationProvider.getApplicationContext())
        launchFragmentInContainer<FeedFragment>(
            fragmentArgs = MainFeedFragmentDirections.mainFeedsToArticlesCategory(
                VoidCategory, FeedSection.ALL
            ).arguments,
            themeResId = R.style.AppTheme,
        ).onFragment {
            navController.setGraph(R.navigation.main_nav_graph)
            Navigation.setViewNavController(it.requireView(), navController)

            it.onClick(feeds[0])

            navController.currentDestination?.id shouldBe R.id.articlesFragment
        }

        launchFragmentInContainer<FeedFragment>(
            fragmentArgs = MainFeedFragmentDirections.mainFeedsToArticlesCategory(
                VoidCategory, FeedSection.ALL
            ).arguments,
            themeResId = R.style.AppTheme,
        ).onFragment {
            navController.setGraph(R.navigation.main_nav_graph)
            Navigation.setViewNavController(it.requireView(), navController)

            it.onClick(categories[0])

            navController.currentDestination?.id shouldBe R.id.feedsFragment
        }

        launchFragmentInContainer<FeedFragment>(
            fragmentArgs = MainFeedFragmentDirections.mainFeedsToArticlesCategory(
                categories[0], FeedSection.ALL
            ).arguments,
            themeResId = R.style.AppTheme,
        ).onFragment {
            navController.setGraph(R.navigation.main_nav_graph)
            navController.setCurrentDestination(
                R.id.feedsFragment,
                MainFeedFragmentDirections.mainFeedsToArticlesCategory(VoidCategory, FeedSection.ALL).arguments
            )
            Navigation.setViewNavController(it.requireView(), navController)

            it.onClick(feeds[0])

            navController.currentDestination?.id shouldBe R.id.articlesFragment
        }
    }
}
