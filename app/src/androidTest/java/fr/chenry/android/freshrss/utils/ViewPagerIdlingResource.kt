package fr.chenry.android.freshrss.utils

import androidx.test.espresso.IdlingResource
import androidx.viewpager2.widget.ViewPager2

class ViewPagerIdlingResource(viewPager: ViewPager2) : IdlingResource {
    private var isIdle = true
    private var resourceCallback: IdlingResource.ResourceCallback? = null

    init {
        viewPager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageScrollStateChanged(state: Int) {
                isIdle = (state == ViewPager2.SCROLL_STATE_IDLE || state == ViewPager2.SCROLL_STATE_DRAGGING)
                if (isIdle && resourceCallback != null) {
                    resourceCallback!!.onTransitionToIdle()
                }
            }
        })
    }

    override fun getName(): String {
        return ViewPagerIdlingResource::class.java.canonicalName!!
    }

    override fun isIdleNow(): Boolean {
        return isIdle
    }

    override fun registerIdleTransitionCallback(resourceCallback: IdlingResource.ResourceCallback) {
        this.resourceCallback = resourceCallback
    }
}
