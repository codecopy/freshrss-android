package fr.chenry.android.freshrss.components.articles

import android.os.Build
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.swipeRight
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions.actionOnItem
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.hasDescendant
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withEffectiveVisibility
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import fr.chenry.android.freshrss.R
import fr.chenry.android.freshrss.components.feeds.FeedSection
import fr.chenry.android.freshrss.store.Store
import fr.chenry.android.freshrss.store.database.models.ArticleReadStatusUpdate
import fr.chenry.android.freshrss.store.database.models.FavoriteStatus
import fr.chenry.android.freshrss.store.database.models.ReadStatus
import fr.chenry.android.freshrss.testutils.FreshRSSBaseTest
import fr.chenry.android.freshrss.testutils.factories.ArticleFactory
import fr.chenry.android.freshrss.testutils.factories.FeedFactory
import fr.chenry.android.freshrss.testutils.matchers.check
import fr.chenry.android.freshrss.utils.SafeResult
import fr.chenry.android.freshrss.utils.loadKoinModules
import io.mockk.coEvery
import io.mockk.mockk
import io.mockk.unmockkAll
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.supervisorScope
import kotlinx.coroutines.test.runTest
import org.junit.Assume.assumeTrue
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.dsl.module
import kotlin.test.assertEquals

@OptIn(ExperimentalCoroutinesApi::class)
@RunWith(AndroidJUnit4::class)
@LargeTest
class ArticlesFragmentTest : FreshRSSBaseTest() {
    private val feed = FeedFactory.createFeed()
    private val articles = listOf(
        ArticleFactory.createArticle(
            id = "1",
            streamId = feed.id,
            favorite = FavoriteStatus.FAVORITE,
            readStatus = ReadStatus.READ
        ),
        ArticleFactory.createArticle(
            id = "2",
            streamId = feed.id,
            favorite = FavoriteStatus.NOT_FAVORITE,
            readStatus = ReadStatus.READ
        ),
        ArticleFactory.createArticle(
            id = "3",
            streamId = feed.id,
            favorite = FavoriteStatus.FAVORITE,
            readStatus = ReadStatus.UNREAD
        ),
        ArticleFactory.createArticle(
            id = "4",
            streamId = feed.id,
            favorite = FavoriteStatus.NOT_FAVORITE,
            readStatus = ReadStatus.UNREAD
        ),
    )

    @Before
    fun setUp() = runTest {
        application.db.syncSubscriptions(mapOf(feed.id to feed))
        application.db.forceInsertArticles(articles)
    }

    @Test
    fun `test-displays-all-items`() {
        launchFragmentInContainer<ArticlesFragment>(
            themeResId = R.style.AppTheme,
            fragmentArgs = ArticlesFragmentArgs(feed, FeedSection.ALL).toBundle()
        )

        onView(withId(R.id.fragment_feed_articles)).check<RecyclerView> {
            assertEquals(it.adapter!!.itemCount, articles.size)
        }
    }

    @Test
    fun `test-displays-favorites-items`() {
        launchFragmentInContainer<ArticlesFragment>(
            themeResId = R.style.AppTheme,
            fragmentArgs = ArticlesFragmentArgs(feed, FeedSection.FAVORITES).toBundle()
        )

        onView(withId(R.id.fragment_feed_articles)).check<RecyclerView> { view ->
            assertEquals(view.adapter!!.itemCount, articles.count { it.favorite == FavoriteStatus.FAVORITE })
        }
    }

    @Test
    fun `test-displays-unread-items`() {
        launchFragmentInContainer<ArticlesFragment>(
            themeResId = R.style.AppTheme,
            fragmentArgs = ArticlesFragmentArgs(feed, FeedSection.UNREAD).toBundle()
        )

        onView(withId(R.id.fragment_feed_articles)).check<RecyclerView> { view ->
            assertEquals(view.adapter!!.itemCount, articles.count { it.readStatus == ReadStatus.UNREAD })
        }
    }

    @Test
    fun `test-swipe-marks-item-as-read`() = runTest {
        assumeTrue(
            "Tests skipped on API 21 as Mockk deosn't seem to work correctly on versions prior 28",
            Build.VERSION.SDK_INT > Build.VERSION_CODES.P
        )

        val storeMock = mockk<Store>()

        loadKoinModules(
            module {
                single(createdAtStart = true) { storeMock }
            }
        )

        val unread = articles.filter { it.readStatus == ReadStatus.UNREAD }

        coEvery {
            storeMock.postReadStatus(unread[0], ReadStatus.READ)
        } coAnswers {
            application.db.updateArticleReadStatuses(listOf(ArticleReadStatusUpdate(unread[0].id, ReadStatus.READ)))
            SafeResult.success(Unit)
        }

        launchFragmentInContainer<ArticlesFragment>(
            themeResId = R.style.AppTheme,
            fragmentArgs = ArticlesFragmentArgs(feed, FeedSection.UNREAD).toBundle()
        )

        onView(withId(R.id.fragment_feed_articles)).perform(
            actionOnItem<RecyclerView.ViewHolder>(
                hasDescendant(withText(unread[0].title)),
                swipeRight()
            )
        ).check<RecyclerView> {
            assertEquals(unread.size - 1, it.adapter!!.itemCount)
        }

        unmockkAll()
    }

    @Test
    fun `test-mark-all-item-as-read-displays-the-read-all-screen`() {
        runTest {
            val unread = articles.filter { it.readStatus == ReadStatus.UNREAD }

            launchFragmentInContainer<ArticlesFragment>(
                themeResId = R.style.AppTheme,
                fragmentArgs = ArticlesFragmentArgs(feed, FeedSection.UNREAD).toBundle()
            )

            onView(withId(R.id.fragment_feed_articles)).perform().check<RecyclerView> {
                assertEquals(it.adapter!!.itemCount, unread.size)
            }

            supervisorScope {
                unread.forEach {
                    application.db.updateArticleReadStatuses(listOf(ArticleReadStatusUpdate(it.id, ReadStatus.READ)))
                }
            }
        }

        Thread.sleep(50)

        onView(withId(R.id.fragment_feed_empty_screen)).check(matches(isDisplayed()))
        onView(withId(R.id.fragment_feed_articles)).check(
            matches(withEffectiveVisibility(ViewMatchers.Visibility.GONE))
        )
    }
}
