package fr.chenry.android.freshrss.testutils.matchers

import android.view.View
import android.widget.TextView
import androidx.core.text.parseAsHtml
import androidx.test.espresso.matcher.BoundedMatcher
import com.google.android.material.textfield.TextInputLayout
import fr.chenry.android.freshrss.utils.IResourceResolver
import org.hamcrest.Description

class ErrorTextMatcher
internal constructor(private val resourceId: Int, vararg formatArgs: Any = arrayOf()) :
    BoundedMatcher<View?, TextInputLayout>(TextInputLayout::class.java) {
    private val formatArgs = arrayOf(*formatArgs)
    private lateinit var resourceName: String
    private lateinit var expectedText: String
    private lateinit var actualText: String

    override fun describeTo(description: Description) {
        description.appendText("with error text of ressource id: $resourceId")
        if (::resourceName.isInitialized) description.appendText(" [$resourceName]")
    }

    override fun describeMismatch(item: Any?, description: Description?) =
        equalToWithDiff(expectedText).describeMismatch(actualText, description)

    public override fun matchesSafely(textView: TextInputLayout): Boolean {
        val resolvedFormatArgs =
            formatArgs
                .map { if (it is IResourceResolver) it(textView.context) else it }
                .toTypedArray()

        if (!::expectedText.isInitialized)
            expectedText =
                textView
                    .resources
                    .getString(resourceId, *resolvedFormatArgs)
                    .parseAsHtml()
                    .toString()
                    .replace("\n", "")

        if (!::resourceName.isInitialized)
            resourceName = textView.resources.getResourceEntryName(resourceId)
        if (!::actualText.isInitialized) actualText = "${textView.error}"

        return expectedText == actualText
    }
}

fun errorTextMatches(error: Int, vararg formatArgs: Any = arrayOf()) =
    ErrorTextMatcher(error, *formatArgs)

class HtmlTextMatcher
internal constructor(
    private val resourceId: Int,
    private vararg val formatArgs: Any = arrayOf(),
    private val transformer: (String) -> String = { it }
) : BoundedMatcher<View?, TextView>(TextView::class.java) {
    private lateinit var resourceName: String
    private lateinit var expectedText: String
    private lateinit var actualText: String

    override fun describeTo(description: Description) {
        description.appendText("with error text of ressource id: $resourceId")
        if (::resourceName.isInitialized) description.appendText(" [$resourceName]")
    }

    override fun describeMismatch(item: Any?, description: Description?) =
        equalToWithDiff(expectedText).describeMismatch(actualText, description)

    public override fun matchesSafely(textView: TextView): Boolean {
        val resolvedFormatArgs =
            formatArgs
                .map { if (it is IResourceResolver) it(textView.context) else it }
                .toTypedArray()

        if (!::expectedText.isInitialized)
            expectedText =
                textView
                    .resources
                    .getString(resourceId, *resolvedFormatArgs)
                    .let(transformer)
                    .parseAsHtml()
                    .toString()
                    .replace("\n", "")

        if (!::resourceName.isInitialized)
            resourceName = textView.resources.getResourceEntryName(resourceId)
        if (!::actualText.isInitialized) actualText = "${textView.text}".replace("\n", "")

        return expectedText == actualText
    }
}

fun htmlTextMatches(
    resourceId: Int,
    vararg formatArgs: Any = arrayOf(),
    transformer: (String) -> String = { it }
) = HtmlTextMatcher(resourceId, *formatArgs, transformer = transformer)
