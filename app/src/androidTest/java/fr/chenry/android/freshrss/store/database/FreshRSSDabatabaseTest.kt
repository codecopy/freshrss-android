package fr.chenry.android.freshrss.store.database

import androidx.room.Room
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import fr.chenry.android.freshrss.store.database.models.Feed
import fr.chenry.android.freshrss.testutils.TestUtils
import fr.chenry.android.freshrss.testutils.factories.FeedFactory
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.firstOrNull
import kotlinx.coroutines.test.runTest
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import kotlin.test.assertContentEquals
import kotlin.test.assertEquals

@OptIn(ExperimentalCoroutinesApi::class)
@RunWith(AndroidJUnit4::class)
@SmallTest
class FreshRSSDabatabaseTest : FreshRSSDabatabaseBaseTest() {

    private val dbName = "${this::class.qualifiedName!!}.db"
    private lateinit var db: FreshRSSDabatabase

    @Before
    fun setUp() {
        db = Room.databaseBuilder(context, FreshRSSDabatabase::class.java, dbName)
            .addMigrations(*FreshRSSDabatabase_Migrations.build())
            .build()
    }

    @After
    fun tearDown() {
        context.deleteDatabase(dbName)
    }

    @Test
    fun syncSubscriptionsFromScratch() = runTest {
        val subscriptions = FeedFactory.createFeeds(10).sortedBy { it.id }.associateBy { it.id }
        db.syncSubscriptions(subscriptions)
        val result = db.getAllSubcriptionsIds().sorted().toTypedArray()
        assertContentEquals(subscriptions.keys.toTypedArray(), result)
    }

    @Test
    fun syncSubscriptionsWithUpdates() = runTest {
        val subscriptions = FeedFactory.createFeeds(10)
        val subscriptionsMap = subscriptions.sortedBy { it.id }.associateByTo(mutableMapOf()) { it.id }

        db.syncSubscriptions(subscriptionsMap)

        val oldSubscription = subscriptions[5]
        val newFeed = Feed(
            oldSubscription.id,
            TestUtils.harryPotter().character(),
            "${oldSubscription.iconUrl}/icon",
            oldSubscription.unreadCount + 20,
            oldSubscription.newestArticleDate.plusDays(3)
        )
        subscriptionsMap[newFeed.id] = newFeed

        db.syncSubscriptions(subscriptionsMap)

        val expected = oldSubscription.copy(
            title = newFeed.title,
            iconUrl = newFeed.iconUrl
        )
        val actual = db.getSubcriptionsById(oldSubscription.id).firstOrNull()?.firstOrNull()

        assertEquals(expected, actual)
    }

    @Test
    fun syncSubscriptionsWithDeletions() = runTest {
        val subscriptions = FeedFactory.createFeeds(10)
        val subscriptionsMap = subscriptions.sortedBy { it.id }.associateByTo(mutableMapOf()) { it.id }

        db.syncSubscriptions(subscriptionsMap)

        val subscriptionToRemove = subscriptions[5]
        subscriptionsMap.remove(subscriptionToRemove.id)

        assertEquals(9, subscriptionsMap.size)

        db.syncSubscriptions(subscriptionsMap)

        val actual = db.getAllSubcriptionsIds()
        val expected =
            subscriptions.filter { it.id != subscriptionToRemove.id }.map { it.id }.sorted()
        assertEquals(expected, actual)
    }
}
