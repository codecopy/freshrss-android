package fr.chenry.android.freshrss.testutils.factories

import fr.chenry.android.freshrss.store.database.models.Feed
import fr.chenry.android.freshrss.testutils.TestUtils
import org.joda.time.LocalDateTime
import java.util.concurrent.TimeUnit

object FeedFactory {
    fun createFeed(
        id: String = TestUtils.idNumber().valid(),
        title: String = TestUtils.lebowski().character(),
        iconUrl: String = TestUtils.internet().url(),
        unreadCount: Int = TestUtils.number().numberBetween(0, 100),
        newestArticleDate: LocalDateTime =
            LocalDateTime(TestUtils.jodaDate().past(15, TimeUnit.DAYS).millis)
    ) = Feed(id, title, iconUrl, unreadCount, newestArticleDate)

    fun createFeeds(number: Int) = (1..number).map { createFeed() }
}
