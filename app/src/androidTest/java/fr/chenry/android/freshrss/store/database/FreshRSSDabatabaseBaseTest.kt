package fr.chenry.android.freshrss.store.database

import android.database.sqlite.SQLiteDatabase
import androidx.room.testing.MigrationTestHelper
import androidx.sqlite.db.SupportSQLiteDatabase
import androidx.test.platform.app.InstrumentationRegistry
import fr.chenry.android.freshrss.testutils.FreshRSSBaseTest
import org.junit.Rule

abstract class FreshRSSDabatabaseBaseTest : FreshRSSBaseTest() {
    open val testDB
        get() = "migration-test"

    @get:Rule
    val helper: MigrationTestHelper = MigrationTestHelper(
        InstrumentationRegistry.getInstrumentation(),
        FreshRSSDabatabase::class.java,
    )

    val database: SQLiteDatabase by lazy {
        SQLiteDatabase.openDatabase(
            context.getDatabasePath(testDB).absolutePath,
            null,
            SQLiteDatabase.OPEN_READWRITE
        )
    }

    fun createDatabaseFromScratch(targetVersion: Int = FreshRSSDabatabase_Migrations.build().size) {
        helper.createDatabase(testDB, 1).apply {
            execSQLWithImports(this, "migration-test-1-before")
            migrateDatabase(this, targetVersion)
            close()
        }
    }

    private fun migrateDatabase(database: SupportSQLiteDatabase, to: Int) {
        val migrations = FreshRSSDabatabase_Migrations.build()

        (1 until to).forEach { version ->
            val migration = migrations[version - 1]
            val nextPreMigrationScript = "migration-test-${migration.endVersion}-before.sql"
            val nextPostMigrationScript = "migration-test-${migration.endVersion}-after.sql"

            assets.list("database")?.find { it == nextPreMigrationScript }?.let {
                execSQLWithImports(database, it)
            }
            helper.runMigrationsAndValidate(testDB, migration.endVersion, true, migration)
            assets.list("database")?.find { it == nextPostMigrationScript }?.let {
                execSQLWithImports(database, it)
            }
        }
    }

    private fun execSQLWithImports(sqlMgr: SupportSQLiteDatabase, name: String) {
        val importRegex = "^--\\s*import\\s*:\\s*(\\S+).*$".toRegex()
        assets.open("database/${name.replace(".sql", "")}.sql").reader().forEachLine { sql ->
            val matchResult = importRegex.find(sql)
            if (matchResult != null) {
                val (import) = matchResult.destructured
                execSQLWithImports(sqlMgr, import)
            } else if (!sql.startsWith("--", true)) {
                sqlMgr.execSQL(sql)
            }
        }
    }
}
