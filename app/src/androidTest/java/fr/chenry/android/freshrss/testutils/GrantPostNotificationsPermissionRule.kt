package fr.chenry.android.freshrss.testutils

import android.Manifest.permission.POST_NOTIFICATIONS
import android.os.Build
import androidx.test.rule.GrantPermissionRule

@Suppress("TestFunctionName")
fun GrantPostNotificationsPermissionRule(): GrantPermissionRule =
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) GrantPermissionRule.grant(POST_NOTIFICATIONS)
    else GrantPermissionRule.grant()
