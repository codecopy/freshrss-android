package fr.chenry.android.freshrss.activities

import android.app.Activity
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.test.core.app.launchActivity
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.NoActivityResumedException
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.pressBack
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.DrawerActions
import androidx.test.espresso.contrib.DrawerMatchers.isClosed
import androidx.test.espresso.contrib.DrawerMatchers.isOpen
import androidx.test.espresso.matcher.ViewMatchers.isRoot
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import androidx.test.rule.GrantPermissionRule
import fr.chenry.android.freshrss.R
import fr.chenry.android.freshrss.testutils.FreshRSSBaseTest
import fr.chenry.android.freshrss.testutils.GrantPostNotificationsPermissionRule
import junit.framework.TestCase.assertFalse
import org.junit.Assert.assertThrows
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@LargeTest
class MainActivityTest : FreshRSSBaseTest() {
    @get:Rule
    val postNotificationPermissionRule: GrantPermissionRule = GrantPostNotificationsPermissionRule()

    fun `drawer-is-closed-when-adding-a-new-subscription`() {
        launchActivity<MainActivity>()

        // Open drawer
        onView(withId(R.id.activity_main_navigation_drawer)).perform(DrawerActions.open())
        onView(withId(R.id.activity_main_navigation_drawer)).check(matches(isOpen()))
        onView(withText(R.string.title_add_subscription)).perform(click())

        val drawer = getActivityInstance<Activity>().findViewById<DrawerLayout>((R.id.activity_main_navigation_drawer))
        assertFalse(drawer.isDrawerOpen(GravityCompat.START))
    }

    @Test
    fun `drawer-is-closed-before-application-exits`() {
        launchActivity<MainActivity>()

        onView(withId(R.id.activity_main_navigation_drawer)).perform(DrawerActions.open())
        onView(withId(R.id.activity_main_navigation_drawer)).check(matches(isOpen()))
        onView(isRoot()).perform(pressBack())
        onView(withId(R.id.activity_main_navigation_drawer)).check(matches(isClosed()))

        // We assert that pressing back again exists the app
        assertThrows(NoActivityResumedException::class.java) {
            onView(isRoot()).perform(pressBack())
        }
    }
}
