@file:Suppress("unused")

package fr.chenry.android.freshrss.testutils

import com.github.javafaker.DateAndTime
import com.github.javafaker.Faker
import org.joda.time.DateTime
import java.util.Locale
import java.util.concurrent.TimeUnit

class JodaDateAndTime internal constructor(private val faker: Faker) {
    /**
     * Generates a future date from now. Note that there is a 1 second slack to avoid generating a
     * past date.
     *
     * @param atMost at most this amount of time ahead from now exclusive.
     * @param unit the time unit.
     * @return a future date from now.
     */
    fun future(atMost: Int, unit: TimeUnit): DateTime {
        val aBitLaterThanNow = DateTime(DateTime.now().plusSeconds(1))
        return future(atMost, unit, aBitLaterThanNow)
    }

    /**
     * Generates a future date from now, with a minimum time.
     *
     * @param atMost at most this amount of time ahead from now exclusive.
     * @param minimum the minimum amount of time in the future from now.
     * @param unit the time unit.
     * @return a future date from now.
     */
    @Suppress("UNUSED_VARIABLE")
    fun future(atMost: Int, minimum: Int, unit: TimeUnit): DateTime {
        val now = DateTime()
        val minimumDateTime = DateTime(DateTime.now().millis + unit.toMillis(minimum.toLong()))
        return future(atMost - minimum, unit, minimumDateTime)
    }

    /**
     * Generates a future date relative to the `referenceDateTime`.
     *
     * @param atMost at most this amount of time ahead to the `referenceDateTime` exclusive.
     * @param unit the time unit.
     * @param referenceDateTime the future date relative to this date.
     * @return a future date relative to `referenceDateTime`.
     */
    fun future(atMost: Int, unit: TimeUnit, referenceDateTime: DateTime): DateTime {
        val upperBound = unit.toMillis(atMost.toLong())
        var futureMillis = referenceDateTime.millis
        futureMillis += 1 + faker.random().nextLong(upperBound - 1)
        return DateTime(futureMillis)
    }

    /**
     * Generates a past date from now. Note that there is a 1 second slack added.
     *
     * @param atMost at most this amount of time earlier from now exclusive.
     * @param unit the time unit.
     * @return a past date from now.
     */
    fun past(atMost: Int, unit: TimeUnit): DateTime {
        val aBitEarlierThanNow = DateTime(DateTime.now().millis - 1000)
        return past(atMost, unit, aBitEarlierThanNow)
    }

    /**
     * Generates a past date from now, with a minimum time.
     *
     * @param atMost at most this amount of time earlier from now exclusive.
     * @param minimum the minimum amount of time in the past from now.
     * @param unit the time unit.
     * @return a past date from now.
     */
    @Suppress("UNUSED_VARIABLE")
    fun past(atMost: Int, minimum: Int, unit: TimeUnit): DateTime {
        val now = DateTime()
        val minimumDateTime = DateTime(DateTime.now().millis - unit.toMillis(minimum.toLong()))
        return past(atMost - minimum, unit, minimumDateTime)
    }

    /**
     * Generates a past date relative to the `referenceDateTime`.
     *
     * @param atMost at most this amount of time past to the `referenceDateTime` exclusive.
     * @param unit the time unit.
     * @param referenceDateTime the past date relative to this date.
     * @return a past date relative to `referenceDateTime`.
     */
    fun past(atMost: Int, unit: TimeUnit, referenceDateTime: DateTime): DateTime {
        val upperBound = unit.toMillis(atMost.toLong())
        var futureMillis = referenceDateTime.millis
        futureMillis -= 1 + faker.random().nextLong(upperBound - 1)
        return DateTime(futureMillis)
    }

    /**
     * Generates a random date between two dates.
     *
     * @param from the lower bound inclusive
     * @param to the upper bound exclusive
     * @return a random date between `from` and `to`.
     * @throws IllegalArgumentException if the `to` date represents an earlier date than `from`
     * date.
     */
    @Throws(IllegalArgumentException::class)
    fun between(from: DateTime, to: DateTime): DateTime {
        require(!to.isBefore(from)) {
            "Invalid date range, the upper bound date is before the lower bound."
        }
        if (from == to) {
            return from
        }
        val offsetMillis = faker.random().nextLong(to.millis - from.millis)
        return DateTime(from.millis + offsetMillis)
    }
    /**
     * Generates a random birthday between two ages.
     *
     * @param minAge the minimal age
     * @param maxAge the maximal age
     * @return a random birthday between `minAge` and `maxAge` years ago.
     * @throws IllegalArgumentException if the `maxAge` is lower than `minAge`.
     */
    /**
     * Generates a random birthday between 65 and 18 years ago.
     *
     * @return a random birthday between 65 and 18 years ago.
     */
    @JvmOverloads
    fun birthday(minAge: Int = DEFAULT_MIN_AGE, maxAge: Int = DEFAULT_MAX_AGE): DateTime {
        val now = DateTime.now()
        val from = DateTime(now.year - maxAge, now.monthOfYear, now.dayOfMonth, 0, 0)
        val to = DateTime(now.year - minAge, now.monthOfYear, now.dayOfMonth, 0, 0)
        return between(from, to)
    }

    companion object {
        private const val DEFAULT_MIN_AGE = 18
        private const val DEFAULT_MAX_AGE = 65
    }
}

object TestUtils : Faker(Locale.getDefault()) {
    private val jodaDate = JodaDateAndTime(this)

    @Deprecated(message = "Use jodaDate()")
    override fun date(): DateAndTime {
        throw NotImplementedError(
            "This modules makes use of Java 8's date and time API " +
                "which is not availables on some platforms. Please use `jodaDate` function instead"
        )
    }

    fun jodaDate() = jodaDate
}
