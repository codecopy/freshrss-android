package fr.chenry.android.freshrss.activities

import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.core.app.ActivityCompat
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.test.espresso.Espresso
import androidx.test.espresso.Espresso.onData
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.closeSoftKeyboard
import androidx.test.espresso.action.ViewActions.replaceText
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.RootMatchers.isDialog
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.isRoot
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withSpinnerText
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import androidx.test.rule.ActivityTestRule
import com.skydoves.expandablelayout.ExpandableLayout
import fr.chenry.android.freshrss.R
import fr.chenry.android.freshrss.components.login.LoginCredentialsFragment.Companion.BAD_CREDENTIALS_COUNT_TRIGGER
import fr.chenry.android.freshrss.components.login.LoginWrapperFragmentDirections
import fr.chenry.android.freshrss.store.api.API_URL_OK_RESPONSE
import fr.chenry.android.freshrss.store.api.GOOGLE_API_ENDPOINT
import fr.chenry.android.freshrss.store.api.LOGIN_ENPOINT
import fr.chenry.android.freshrss.testutils.EspressoIdlingResource
import fr.chenry.android.freshrss.testutils.EspressoIdlingResource.unaryMinus
import fr.chenry.android.freshrss.testutils.FreshRSSBaseTest
import fr.chenry.android.freshrss.testutils.matchers.check
import fr.chenry.android.freshrss.testutils.matchers.errorTextMatches
import fr.chenry.android.freshrss.testutils.matchers.htmlTextMatches
import fr.chenry.android.freshrss.testutils.matchers.that
import fr.chenry.android.freshrss.utils.idResRes
import io.mockk.every
import io.mockk.mockk
import io.mockk.unmockkAll
import okhttp3.HttpUrl
import okhttp3.mockwebserver.Dispatcher
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import okhttp3.mockwebserver.RecordedRequest
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.allOf
import org.hamcrest.Matchers.containsString
import org.hamcrest.Matchers.instanceOf
import org.hamcrest.Matchers.matchesPattern
import org.hamcrest.core.Is.`is`
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import java.util.regex.Pattern
import androidx.test.espresso.matcher.ViewMatchers.assertThat as assertThatView

@RunWith(AndroidJUnit4::class)
@LargeTest
class LoginActivityTest : FreshRSSBaseTest() {
    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    // Waiting for https://github.com/android/android-test/issues/143#issuecomment-856036493 to get
    // fixed
    @Suppress("DEPRECATION")
    @get:Rule
    val activityRule = ActivityTestRule(LoginActivity::class.java)

    @Before
    fun setUp() {
        while (!EspressoIdlingResource.networkIdlingResource.isIdleNow) -EspressoIdlingResource.networkIdlingResource
        IdlingRegistry.getInstance().register(EspressoIdlingResource.networkIdlingResource)
        Espresso.closeSoftKeyboard()
    }

    @After
    fun tearDown() {
        IdlingRegistry.getInstance().unregister(EspressoIdlingResource.networkIdlingResource)
    }

    // **************
    // * Login form *
    // **************
    @Test
    fun `tests-change-protocol`() {
        val activity = getActivityInstance<LoginActivity>()

        assertEquals(activity.getString(R.string.https_protocol), activity.viewModel.protocol.value)
        onView(withId(R.id.protocol_selection)).perform(click())
        onData(`is`("http://")).perform(click())
        onView(withId(R.id.protocol_selection)).check(matches(withSpinnerText(R.string.http_protocol)))
        assertEquals(activity.getString(R.string.http_protocol), activity.viewModel.protocol.value)
    }

    @Test
    fun `displays-a-form-error-when-url-is-empty`() {
        onView(withId(R.id.instance_field)).perform(replaceText(""))
        onView(withId(R.id.to_credentials_button)).perform(click())

        onView(withId(R.id.instance_container)).check(that(errorTextMatches(R.string.error_field_required)))

        onView(withId(R.id.instance_field)).perform(replaceText("   \t"))
        onView(withId(R.id.to_credentials_button)).perform(click())

        onView(withId(R.id.instance_container)).check(that(errorTextMatches(R.string.error_field_required)))
    }

    @Test
    fun `displays-a-warning-on-non-web-url`() {
        val activity = getActivityInstance<LoginActivity>()

        onView(withId(R.id.instance_field)).perform(replaceText("example?com"))
        onView(withId(R.id.to_credentials_button)).perform(click())

        onView(withId(R.id.simple_text_dialog_content)).check(
            that(
                htmlTextMatches(
                    R.string.url_nok,
                    activity.viewModel.instanceDomain.value!!,
                    idResRes(android.R.string.ok)
                )
            )
        )
    }

    @Test
    fun `displays-an-dialog-on-unknown-error`() {
        onView(withId(R.id.instance_field)).perform(replaceText("localhost"))
        onView(withId(R.id.to_credentials_button)).perform(click())

        onView(withId(R.id.error_detail_dialog_expandable)).perform(click())
        onView(withId(R.id.error_detail_dialog_expandable)).check<ExpandableLayout> {
            assertTrue(it.isExpanded)
        }

        val pattern = "java.net.ConnectException: Failed to connect to localhost/127.0.0.1.*"

        onView(withId(R.id.error_detail_dialog_expandable)).check<ExpandableLayout> {
            val text = it.secondLayout.findViewById<TextView>(R.id.error_detail_dialog_stacktrace).text
            assertThat(text.toString(), matchesPattern(pattern.toPattern(Pattern.DOTALL)))
        }

        // Close the dialog for the next test
        onView(isRoot()).inRoot(isDialog()).check<View> {
            it.findViewById<Button>(R.id.error_detail_dialog_close).callOnClick()
        }

        onView(withId(R.id.instance_field)).perform(replaceText("localhost.err"))
        onView(withId(R.id.to_credentials_button)).perform(click())

        onView(withId(R.id.instance_container)).check(that(errorTextMatches(R.string.unreachable_host)))
    }

    @Test
    fun `displays-an-error-dialog-when-api-does-not-respond-correctly`() {
        initMockWebServer { enqueue(MockResponse().setBody("OOPSIE")) }.use { server ->
            fillApiUrlForm(server.url("/"))

            onView(withId(R.id.to_credentials_button)).perform(click())

            onView(withId(R.id.simple_text_dialog_content)).inRoot(isDialog()).check(
                that(
                    htmlTextMatches(
                        R.string.api_url_invalid_response,
                        activityRule.activity.viewModel.completeApiUrl,
                        API_URL_OK_RESPONSE
                    )
                )
            )
        }
    }

    @Test
    fun `displays-a-warning-dialog-when-root-folder-is-exposed`() {
        initMockWebServer {
            dispatcher = object : Dispatcher() {
                override fun dispatch(request: RecordedRequest): MockResponse = when (request.path) {
                    "/p/$GOOGLE_API_ENDPOINT" -> MockResponse().setBody("OK")
                    else -> MockResponse().setResponseCode(404)
                }
            }
        }.use { server ->
            val serverUrl = server.url("/")
            fillApiUrlForm(serverUrl)
            onView(withId(R.id.to_credentials_button)).perform(click())

            onView(withId(R.id.simple_text_dialog_content)).inRoot(isDialog()).check(
                that(
                    htmlTextMatches(
                        R.string.root_exposed_warning,
                        idResRes(R.string.freshrss_documentation_url)
                    )
                )
            )
        }
    }

    @Test
    fun `navigates-when-API-responds-OK`() {
        initMockWebServer { enqueue(MockResponse().setBody("OK")) }.use {
            moveToCredentialForm(it.url("/"))
        }
    }

    // ********************
    // * Credentials form *
    // ********************

    @Test
    fun `shows-an-error-message-on-empty-fields`() {
        initMockWebServer { enqueue(MockResponse().setBody("OK")) }.use {
            moveToCredentialForm(it.url("/"))
        }

        onView(withId(R.id.username_field)).perform(replaceText(""))
        onView(withId(R.id.login_button)).perform(click())

        onView(withId(R.id.username_container)).check(matches(errorTextMatches(R.string.error_field_required)))

        onView(withId(R.id.username_field)).perform(replaceText("username"), closeSoftKeyboard())
        onView(withId(R.id.password_field)).perform(replaceText(""), closeSoftKeyboard())
        onView(withId(R.id.login_button)).perform(click())

        onView(withId(R.id.password_container)).check(matches(errorTextMatches(R.string.error_invalid_password)))
    }

    @Test
    fun `redirects-to-the-loading-page-on-valid-form`() {
        initMockWebServer {
            dispatcher = object : Dispatcher() {
                override fun dispatch(request: RecordedRequest): MockResponse = when (request.path) {
                    "/$GOOGLE_API_ENDPOINT" -> MockResponse().setBody("OK")
                    "/$GOOGLE_API_ENDPOINT/$LOGIN_ENPOINT" -> MockResponse().setResponseCode(503)
                    else -> MockResponse().setResponseCode(404)
                }
            }
        }.use { server ->
            moveToCredentialForm(server.url("/"))

            IdlingRegistry.getInstance().unregister(EspressoIdlingResource.networkIdlingResource)

            onView(withId(R.id.username_field)).perform(replaceText("username"))
            onView(withId(R.id.password_field)).perform(replaceText("password"))

            val oldNav = activityRule.activity.navigation
            val mockNavController = mockk<NavController>(relaxed = true)

            every {
                mockNavController.navigate(LoginWrapperFragmentDirections.toLoaderFragment())
            } answers { oldNav.navigate(LoginWrapperFragmentDirections.toLoaderFragment()) }

            every { mockNavController.navigateUp() } returns true

            Navigation.setViewNavController(
                ActivityCompat.requireViewById(
                    activityRule.activity,
                    R.id.login_activity_host_fragment
                ),
                mockNavController
            )

            onView(withId(R.id.login_button)).perform(click())
            onView(withId(R.id.login_progress)).check(matches(isDisplayed()))

            unmockkAll()
        }
    }

    @Test
    fun `displays-a-warning-dialog-when-API-acces-is-disabled`() {
        initMockWebServer {
            dispatcher = object : Dispatcher() {
                override fun dispatch(request: RecordedRequest): MockResponse =
                    when (request.path) {
                        "/$GOOGLE_API_ENDPOINT" -> MockResponse().setBody("OK")
                        "/$GOOGLE_API_ENDPOINT/$LOGIN_ENPOINT" ->
                            MockResponse().setResponseCode(503)
                        else -> MockResponse().setResponseCode(404)
                    }
            }
        }.use { server ->
            moveToCredentialForm(server.url("/"))

            onView(withId(R.id.username_field)).perform(replaceText("username"))
            onView(withId(R.id.password_field)).perform(replaceText("password"))

            onView(withId(R.id.login_button)).perform(click())

            onView(withId(R.id.simple_text_dialog_content)).inRoot(isDialog()).check(
                that(
                    htmlTextMatches(
                        R.string.api_access_disabled_warning,
                        server.url("/i/"),
                        idResRes(R.string.http_api_access_option_section),
                        idResRes(R.string.http_api_access_option)
                    )
                )
            )
        }
    }

    @Test
    fun `displays-login-warnings-correctly-on-bad-credentials`() {
        initMockWebServer {
            dispatcher = object : Dispatcher() {
                override fun dispatch(request: RecordedRequest): MockResponse = when (request.path) {
                    "/$GOOGLE_API_ENDPOINT" -> MockResponse().setBody("OK")
                    "/$GOOGLE_API_ENDPOINT/$LOGIN_ENPOINT" ->
                        MockResponse().setResponseCode(401)
                    else -> MockResponse().setResponseCode(404)
                }
            }
        }.use { server ->
            moveToCredentialForm(server.url("/"))

            onView(withId(R.id.username_field)).perform(replaceText("username"))
            onView(withId(R.id.password_field)).perform(replaceText("password"))

            // First attempt: displays a dialog
            onView(withId(R.id.login_button)).perform(closeSoftKeyboard(), click())
            onView(withId(R.id.simple_text_dialog_content)).inRoot(isDialog()).check<TextView> {
                assertThatView(
                    it.text.toString(),
                    containsString(
                        it.resources.getString(
                            R.string.api_password_misconfiguration_hint,
                            BAD_CREDENTIALS_COUNT_TRIGGER.toString()
                        )
                    )
                )
            }
            onView(withText(android.R.string.ok)).perform(click())

            // Second and third attempts: display a message in the password field
            onView(withId(R.id.login_button)).perform(closeSoftKeyboard(), click())
            onView(withId(R.id.password_container))
                .check(matches(errorTextMatches(R.string.bad_credentials_form_error)))

            onView(withId(R.id.login_button)).perform(closeSoftKeyboard(), click())
            onView(withId(R.id.password_container))
                .check(matches(errorTextMatches(R.string.bad_credentials_form_error)))

            // Fourth attempt: displays a dialog again
            onView(withId(R.id.login_button)).perform(closeSoftKeyboard(), click())
            onView(withId(R.id.simple_text_dialog_content)).inRoot(isDialog()).check<TextView> {
                assertThatView(
                    it.text.toString(),
                    containsString(
                        it.resources.getString(
                            R.string.api_password_misconfiguration_hint,
                            BAD_CREDENTIALS_COUNT_TRIGGER.toString()
                        )
                    )
                )
            }
        }
    }

    @Test
    fun `displays-an-error-dialog-on-unexpected-error`() {
        initMockWebServer {
            dispatcher = object : Dispatcher() {
                override fun dispatch(request: RecordedRequest): MockResponse = when (request.path) {
                    "/$GOOGLE_API_ENDPOINT" -> MockResponse().setBody("OK")
                    "/$GOOGLE_API_ENDPOINT/$LOGIN_ENPOINT" ->
                        MockResponse().setResponseCode(401)
                    else -> MockResponse().setResponseCode(404)
                }
            }
        }.use {
            // For instance, the server crashed in between
            moveToCredentialForm(it.url("/"))
        }

        onView(withId(R.id.username_field)).perform(replaceText("username"))
        onView(withId(R.id.password_field)).perform(replaceText("password"))

        onView(withId(R.id.login_button)).perform(click())

        onView(withId(R.id.error_detail_dialog_expandable)).perform(click())
        onView(withId(R.id.error_detail_dialog_expandable)).check<ExpandableLayout> {
            assertTrue(it.isExpanded)
        }

        val pattern = "java.net.ConnectException: Failed to connect to localhost/127.0.0.1:8888.*"

        onView(withId(R.id.error_detail_dialog_expandable)).check<ExpandableLayout> {
            val text = it.secondLayout.findViewById<TextView>(R.id.error_detail_dialog_stacktrace).text
            assertThatView(text.toString(), matchesPattern(pattern.toPattern(Pattern.DOTALL)))
        }
    }

    private fun initMockWebServer(cb: MockWebServer.() -> Unit = {}) =
        MockWebServer().apply {
            val port = System.getenv().getOrElse("MOCK_WEB_SERVER_PORT") { "8888" }.toIntOrNull() ?: 8888
            apply(cb)
            start(port)
        }

    private fun fillApiUrlForm(url: HttpUrl) {
        val protocol = "${url.scheme}://"
        val instance = url.toString().substring(protocol.length)
        onView(withId(R.id.protocol_selection)).perform(click())
        onData(allOf(`is`(instanceOf(String::class.java)), `is`(protocol))).perform(click())
        onView(withId(R.id.protocol_selection)).check(matches(withSpinnerText(containsString(protocol))))
        onView(withId(R.id.instance_field)).perform(replaceText(instance))
    }

    private fun moveToCredentialForm(url: HttpUrl) {
        fillApiUrlForm(url)
        onView(withId(R.id.to_credentials_button)).perform(click())
        onView(withId(R.id.login_credentials_form)).check(matches(isDisplayed()))
        Espresso.closeSoftKeyboard()
    }
}
