package fr.chenry.android.freshrss.testutils.matchers

import com.github.difflib.text.DiffRow
import com.github.difflib.text.DiffRowGenerator
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.TypeSafeMatcher

class IsEqualWithDiff(private val expected: String) : TypeSafeMatcher<String>() {
    public override fun matchesSafely(item: String): Boolean {
        return expected == item
    }

    public override fun describeMismatchSafely(item: String, mismatchDescription: Description) {
        mismatchDescription.appendText("got the following differences: \n\t")
        val expected = listOf(*expected.split("\n").toTypedArray())
        val actual = listOf(*item.split("\n").toTypedArray())
        val generator =
            DiffRowGenerator.create()
                .showInlineDiffs(true)
                .mergeOriginalRevised(true)
                .reportLinesUnchanged(false)
                .inlineDiffByWord(true)
                .oldTag { it -> if (it) "<+" else "+>" }
                .newTag { it -> if (it) "<+" else "+>" }
                .build()

        runCatching {
            val rows = generator.generateDiffRows(actual, expected)

            for (indices in rows.indices) {
                if (rows[indices].tag == DiffRow.Tag.CHANGE) {
                    mismatchDescription.appendText("\n\t\texpected: ${rows[indices].newLine}")
                    mismatchDescription.appendText("\n\t\tgot:      ${rows[indices].oldLine}")
                }
            }
        }
            .onFailure(Throwable::printStackTrace)
    }

    override fun describeTo(description: Description) {
        description
            .appendText("a string equal to ")
            .appendValue(expected)
            .appendText(" with diff when mismatch")
    }
}

fun equalToWithDiff(expectedString: String): Matcher<String> {
    return IsEqualWithDiff(expectedString)
}
