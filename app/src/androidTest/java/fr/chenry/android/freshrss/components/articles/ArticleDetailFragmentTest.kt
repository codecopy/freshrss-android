package fr.chenry.android.freshrss.components.articles

import android.app.Activity.RESULT_OK
import android.app.Instrumentation
import android.content.Intent
import android.os.Build
import android.view.View
import androidx.appcompat.view.menu.ActionMenuItemView
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.toBitmap
import androidx.lifecycle.Lifecycle
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.intent.Intents
import androidx.test.espresso.intent.Intents.intended
import androidx.test.espresso.intent.Intents.intending
import androidx.test.espresso.intent.matcher.IntentMatchers.hasAction
import androidx.test.espresso.intent.matcher.IntentMatchers.hasData
import androidx.test.espresso.intent.matcher.IntentMatchers.hasExtra
import androidx.test.espresso.intent.matcher.IntentMatchers.hasType
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withClassName
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.web.assertion.WebViewAssertions.webMatches
import androidx.test.espresso.web.sugar.Web.onWebView
import androidx.test.espresso.web.webdriver.DriverAtoms.findElement
import androidx.test.espresso.web.webdriver.DriverAtoms.getText
import androidx.test.espresso.web.webdriver.Locator
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import fr.chenry.android.freshrss.R
import fr.chenry.android.freshrss.components.articles.webviewutils.FRSSWebView
import fr.chenry.android.freshrss.components.articles.webviewutils.ShareIntent
import fr.chenry.android.freshrss.store.Store
import fr.chenry.android.freshrss.store.database.models.ArticleReadStatusUpdate
import fr.chenry.android.freshrss.store.database.models.ReadStatus
import fr.chenry.android.freshrss.testutils.EspressoIdlingResource.displayIdlingResource
import fr.chenry.android.freshrss.testutils.EspressoIdlingResource.unaryMinus
import fr.chenry.android.freshrss.testutils.FreshRSSBaseTest
import fr.chenry.android.freshrss.testutils.factories.ArticleFactory
import fr.chenry.android.freshrss.testutils.matchers.check
import fr.chenry.android.freshrss.utils.SafeResult
import fr.chenry.android.freshrss.utils.loadKoinModules
import fr.chenry.testlib.launchFragmentInAppCompatContainer
import io.mockk.Called
import io.mockk.clearMocks
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import io.mockk.unmockkAll
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.hamcrest.Matcher
import org.hamcrest.Matchers.allOf
import org.hamcrest.Matchers.any
import org.hamcrest.Matchers.containsString
import org.hamcrest.Matchers.equalTo
import org.hamcrest.Matchers.`is`
import org.hamcrest.Matchers.not
import org.junit.After
import org.junit.Assume.assumeTrue
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.dsl.module
import kotlin.test.assertEquals
import kotlin.test.assertNull
import kotlin.test.assertTrue

@RunWith(AndroidJUnit4::class)
@LargeTest
@OptIn(ExperimentalCoroutinesApi::class)
class ArticleDetailFragmentTest : FreshRSSBaseTest() {
    private val bundle = ArticleDetailFragmentArgs(application.article.id).toBundle()
    private val storeMock = mockk<Store>()
    private val unreadArticle = ArticleFactory.createArticle(
        id = "1",
        streamId = application.feed.id,
        readStatus = ReadStatus.UNREAD
    )
    private val readArticle = ArticleFactory.createArticle(
        id = "2",
        streamId = application.feed.id,
        readStatus = ReadStatus.READ
    )

    @Before
    fun setUp() {
        assumeTrue(
            "Tests skipped on API 21 as Mockk deosn't seem to work correctly on versions prior 28",
            Build.VERSION.SDK_INT > Build.VERSION_CODES.P
        )

        loadKoinModules(
            module {
                single(createdAtStart = true) { storeMock }
            }
        )

        coEvery {
            storeMock.postReadStatus(any(), any())
        } answers {
            SafeResult.failure(Exception())
        }

        runTest {
            application.db.forceInsertArticles(listOf(unreadArticle, readArticle))
        }

        while (!displayIdlingResource.isIdleNow) -displayIdlingResource
        IdlingRegistry.getInstance().register(displayIdlingResource)

        Intents.init()
    }

    @After
    fun tearDown() {
        IdlingRegistry.getInstance().unregister(displayIdlingResource)
        Intents.release()
        unmockkAll()
    }

    @Test
    fun `should-display-article`() {
        launchFragmentInAppCompatContainer<ArticleDetailFragment>(
            themeResId = R.style.AppTheme,
            fragmentArgs = bundle
        )

        onWebView(withClassName(containsString(FRSSWebView::class.simpleName)))
            .forceJavascriptEnabled()
            .withElement(findElement(Locator.ID, "article-title"))
            .check(webMatches(getText(), equalTo(application.article.title)))
    }

    @Test
    fun `should-be-able-to-share`() {
        fun chooser(vararg matchers: Matcher<Intent?>?): Matcher<Intent?>? {
            return allOf(
                hasAction(Intent.ACTION_CHOOSER),
                hasExtra(`is`(Intent.EXTRA_INTENT), allOf(*matchers))
            )
        }

        launchFragmentInAppCompatContainer<ArticleDetailFragment>(
            themeResId = R.style.AppTheme,
            fragmentArgs = bundle
        )

        val expectedExtraText = ShareIntent.format(
            ShareIntent.getTemplateAttributes(application.feed.title, application.article)
        )

        intending(any(Intent::class.java)).respondWithFunction {
            Instrumentation.ActivityResult(RESULT_OK, Intent())
        }

        onView(withId(R.id.fab_share)).perform(click())
        intended(
            chooser(
                hasAction(Intent.ACTION_SEND),
                hasType("text/plain"),
                hasExtra(Intent.EXTRA_TEXT, expectedExtraText)
            )
        )
    }

    @Test
    fun `should-be-able-to-open-in-browser`() {
        launchFragmentInAppCompatContainer<ArticleDetailFragment>(
            themeResId = R.style.AppTheme,
            fragmentArgs = bundle
        )

        intending(any(Intent::class.java)).respondWithFunction {
            Instrumentation.ActivityResult(RESULT_OK, Intent())
        }

        onView(withId(R.id.fab_open_browser)).perform(click())
        intended(allOf(hasAction(Intent.ACTION_VIEW), hasData(application.article.url)))
    }

    @Test
    fun `should-not-display-browser-button-when-no-app-can-open`() = runTest {
        val brokenArticle = ArticleFactory.createArticle(streamId = application.feed.id).let {
            it.copy(href = it.href.replace("https?://".toRegex(), ""))
        }

        application.db.insertArticles(listOf(brokenArticle))

        launchFragmentInAppCompatContainer<ArticleDetailFragment>(
            themeResId = R.style.AppTheme,
            fragmentArgs = ArticleDetailFragmentArgs(brokenArticle.id).toBundle()
        )

        onView(withId(R.id.fab_open_browser)).check(matches(not(isDisplayed())))
    }

    @Test
    fun `should-not-mark-the-article-as-read-if-article-is-already-read`() {
        clearMocks(storeMock)
        coEvery {
            storeMock.postReadStatus(readArticle, any())
        } returns SafeResult.success(Unit)

        launchFragmentInAppCompatContainer<ArticleDetailFragment>(
            themeResId = R.style.AppTheme,
            fragmentArgs = ArticleDetailFragmentArgs(readArticle.id).toBundle()
        )

        onView(withId(R.id.fragment_subscription_article_detail_web_view)).check<View> {
            coVerify { storeMock wasNot Called }
        }
    }

    @Test
    fun `should-toggle-the-menu-button-when-clicked`() = runTest {
        clearMocks(storeMock)
        coEvery {
            storeMock.postReadStatus(unreadArticle, ReadStatus.READ)
        } returns SafeResult.failure(Exception())

        launchFragmentInAppCompatContainer<ArticleDetailFragment>(
            themeResId = R.style.AppTheme,
            fragmentArgs = ArticleDetailFragmentArgs(unreadArticle.id).toBundle()
        )

        onView(withId(R.id.action_mark_read_status)).check<ActionMenuItemView> {
            assertEquals(it.itemData.title, it.context.getString(R.string.mark_read))
            val actualBitmap = it.itemData.icon!!.toBitmap()
            val expectedBitmap = ContextCompat.getDrawable(it.context, R.drawable.ic_is_unread_24dp)!!.toBitmap()
            assertTrue(actualBitmap.sameAs(expectedBitmap))
        }

        clearMocks(storeMock)
        coEvery {
            storeMock.postReadStatus(unreadArticle, ReadStatus.READ)
        } coAnswers {
            application.db.updateArticleReadStatuses(listOf(ArticleReadStatusUpdate(unreadArticle.id, ReadStatus.READ)))
            SafeResult.success(Unit)
        }

        onView(withId(R.id.action_mark_read_status)).perform(click()).check<ActionMenuItemView> {
            assertEquals(it.itemData.title, it.context.getString(R.string.mark_unread))
            val actualBitmap = it.itemData.icon!!.toBitmap()
            val expectedBitmap = ContextCompat.getDrawable(it.context, R.drawable.ic_is_read_24dp)!!.toBitmap()
            assertTrue(actualBitmap.sameAs(expectedBitmap))
        }
    }

    @Test
    fun `should-set-and-clear-action-bar-subtitle`() {
        val scenario = launchFragmentInAppCompatContainer<ArticleDetailFragment>(
            themeResId = R.style.AppTheme,
            fragmentArgs = ArticleDetailFragmentArgs(unreadArticle.id).toBundle()
        )

        scenario.onFragment { assertEquals(it.supportActionBar.subtitle, unreadArticle.title) }
        scenario.moveToState(Lifecycle.State.DESTROYED)
        scenario.activityScenario.onActivity { assertNull(it.supportActionBar!!.subtitle) }
    }
}
