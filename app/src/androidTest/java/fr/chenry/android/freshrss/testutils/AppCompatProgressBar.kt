package fr.chenry.android.freshrss.testutils

import android.content.Context
import android.graphics.drawable.Drawable
import android.os.Build
import android.util.AttributeSet
import android.widget.ProgressBar
import fr.chenry.android.freshrss.R

/**
 * This solves the problem of Espresso hanging when the progress bar is animating See
 * src/androidTest/java/fr/chenry/android/freshrss/testutils/AppCompatProgressBar.kt Source:
 * https://jasonfry.co.uk/blog/android-espresso-test-hangs-with-indeterminate-progressbar/
 */
class AppCompatProgressBar
@JvmOverloads
constructor(
    context: Context,
    attributeSet: AttributeSet? = null,
    defStyleAttr: Int = R.attr.progressBarStyle,
    defStyleRes: Int = 0
) : ProgressBar(context, attributeSet, defStyleAttr, defStyleRes) {
    override fun setIndeterminateDrawable(drawable: Drawable?) {
        super.setIndeterminateDrawable(if (hideIndeterminateDrawable()) null else drawable)
    }

    private fun hideIndeterminateDrawable(): Boolean =
        Build.VERSION.SDK_INT <= Build.VERSION_CODES.P
}
