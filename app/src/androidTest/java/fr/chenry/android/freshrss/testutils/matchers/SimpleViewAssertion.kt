package fr.chenry.android.freshrss.testutils.matchers

import android.view.View
import androidx.test.espresso.NoMatchingViewException
import androidx.test.espresso.ViewAssertion
import androidx.test.espresso.ViewInteraction
import org.hamcrest.Matcher
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.StringDescription

class SimpleViewAssertion internal constructor(private val viewMatcher: Matcher<in View?>) :
    ViewAssertion {
    override fun check(view: View?, noViewFoundException: NoMatchingViewException?) {
        val description = StringDescription()
        description.appendText("'")
        viewMatcher.describeTo(description)

        if (noViewFoundException != null) {
            description.appendText(
                "' check could not be performed because view '${noViewFoundException.viewMatcherDescription}' " +
                    "was not found.\n"
            )
            throw noViewFoundException
        } else {
            description.appendText("' doesn't match the selected view.")
            assertThat(description.toString(), view, viewMatcher)
        }
    }
}

fun that(viewMatcher: Matcher<in View?>) = SimpleViewAssertion(viewMatcher)

@Suppress("UNCHECKED_CAST")
fun <T : View> ViewInteraction.check(cb: (it: T) -> Unit): ViewInteraction =
    this.check { view, e ->
        if (e != null) throw e
        cb(view as T)
    }
