package fr.chenry.android.freshrss.utils

import android.os.Build
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.ViewAction
import androidx.test.espresso.action.GeneralClickAction
import androidx.test.espresso.action.Press
import androidx.test.espresso.action.Tap
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.RootMatchers.isDialog
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.isRoot
import androidx.test.espresso.matcher.ViewMatchers.withEffectiveVisibility
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.MediumTest
import fr.chenry.android.freshrss.R
import fr.chenry.android.freshrss.activities.MainActivity
import fr.chenry.android.freshrss.components.utils.SimpleTextDialog
import fr.chenry.android.freshrss.components.utils.simpleTextDialog
import fr.chenry.android.freshrss.testutils.FreshRSSBaseTest
import io.mockk.confirmVerified
import io.mockk.spyk
import io.mockk.verify
import org.junit.Assert.assertNull
import org.junit.Assert.assertTrue
import org.junit.Assume.assumeTrue
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@MediumTest
class SimpleTextDialogTest : FreshRSSBaseTest() {
    @get:Rule
    val activityRule = ActivityScenarioRule(MainActivity::class.java)

    @Test
    fun `simple-dialog-with-text`() {
        val activity = getActivityInstance<MainActivity>()

        activity.simpleTextDialog { text = context.getText(R.string.application) }

        onView(withId(R.id.simple_text_dialog_title_block))
            .check(matches(withEffectiveVisibility(ViewMatchers.Visibility.GONE)))

        onView(withId(R.id.simple_text_dialog_content))
            .check(matches(withText(R.string.application)))
    }

    @Test
    fun `simple-dialog-with-text-and-title`() {
        val activity = getActivityInstance<MainActivity>()

        activity.simpleTextDialog {
            title = context.getText(R.string.application)
            text = context.getText(R.string.connection_password_warning)
        }

        onView(withId(R.id.simple_text_dialog_title_block))
            .inRoot(isDialog())
            .check(matches(isDisplayed()))
        onView(withId(R.id.simple_text_dialog_icon))
            .inRoot(isDialog())
            .check(matches(withEffectiveVisibility(ViewMatchers.Visibility.GONE)))

        onView(withId(R.id.simple_text_dialog_title))
            .inRoot(isDialog())
            .check(matches(withText(R.string.application)))

        onView(withId(R.id.simple_text_dialog_content))
            .inRoot(isDialog())
            .check(matches(withText(R.string.connection_password_warning)))
    }

    @Test
    fun `simple-dialog-with-text-title-and-icon`() {
        val activity = getActivityInstance<MainActivity>()

        activity.simpleTextDialog {
            title = context.getText(R.string.application)
            text = context.getText(R.string.connection_password_warning)
            icon = R.drawable.warning
        }

        onView(withId(R.id.simple_text_dialog_title_block))
            .inRoot(isDialog())
            .check(matches(isDisplayed()))
        onView(withId(R.id.simple_text_dialog_icon))
            .inRoot(isDialog())
            .check(matches(isDisplayed()))
        onView(withId(R.id.simple_text_dialog_title))
            .inRoot(isDialog())
            .check(matches(withText(R.string.application)))
        onView(withId(R.id.simple_text_dialog_content))
            .inRoot(isDialog())
            .check(matches(withText(R.string.connection_password_warning)))
    }

    @Test
    fun `simple-dialog-with-text-title-icon-and-tint`() {
        val activity = getActivityInstance<MainActivity>()

        activity.simpleTextDialog {
            title = context.getText(R.string.application)
            text = context.getText(R.string.connection_password_warning)
            icon = R.drawable.warning
            iconTint = R.color.warning
        }

        onView(withId(R.id.simple_text_dialog_title_block))
            .inRoot(isDialog())
            .check(matches(isDisplayed()))
        onView(withId(R.id.simple_text_dialog_icon))
            .inRoot(isDialog())
            .check(matches(isDisplayed()))
        onView(withId(R.id.simple_text_dialog_title))
            .inRoot(isDialog())
            .check(matches(withText(R.string.application)))
        onView(withId(R.id.simple_text_dialog_content))
            .inRoot(isDialog())
            .check(matches(withText(R.string.connection_password_warning)))
    }

    @Test
    fun `simple-dialog-ok-action`() {
        assumeTrue(
            "Tests skipped on API 21 as Mockk deosn't seem to work correctly on versions prior 28",
            Build.VERSION.SDK_INT > Build.VERSION_CODES.P
        )

        val activity = getActivityInstance<MainActivity>()

        var dialogFragment =
            activity.simpleTextDialog { text = context.getText(R.string.application) }

        val viewInteraction =
            onView(withText(android.R.string.ok)).inRoot(isDialog()).check(matches(isDisplayed()))
        assertTrue(dialogFragment.dialog!!.isShowing)
        viewInteraction.perform(click())
        assertNull(dialogFragment.dialog)

        val mock = spyk<SimpleTextDialog.() -> Unit>({})
        dialogFragment =
            activity.simpleTextDialog {
                text = context.getText(R.string.application)
                onOk(mock)
            }

        onView(withText(android.R.string.ok))
            .inRoot(isDialog())
            .check(matches(isDisplayed()))
            .perform(click())
        verify { mock(dialogFragment) }
        confirmVerified(mock)
    }

    @Test
    fun `simple-dialog-cancel-action`() {
        assumeTrue(
            "Tests skipped on API 21 as Mockk deosn't seem to work correctly on versions prior 28",
            Build.VERSION.SDK_INT > Build.VERSION_CODES.P
        )

        val activity = getActivityInstance<MainActivity>()

        val mock = spyk<SimpleTextDialog.() -> Unit>({})
        val dialogFragment =
            activity.simpleTextDialog {
                text = context.getText(R.string.application)
                onCancel(mock)
            }

        onView(withText(android.R.string.cancel))
            .inRoot(isDialog())
            .check(matches(isDisplayed()))
            .perform(click())
        verify { mock(dialogFragment) }
        confirmVerified(mock)
    }

    @Test
    fun `simple-dialog-cancellable`() {
        val activity = getActivityInstance<MainActivity>()

        var dialogFragment = activity.simpleTextDialog { text = context.getText(R.string.application) }

        onView(withText(android.R.string.ok)).inRoot(isDialog()).check(matches(isDisplayed()))
        assertTrue(dialogFragment.dialog!!.isShowing)
        onView(isRoot()).perform(clickOutsideDialog())
        assertNull(dialogFragment.dialog)

        dialogFragment =
            activity.simpleTextDialog {
                text = context.getText(R.string.application)
                isCancelable = false
            }

        onView(withText(android.R.string.ok)).inRoot(isDialog()).check(matches(isDisplayed()))
        assertTrue(dialogFragment.dialog!!.isShowing)
        onView(isRoot()).perform(clickOutsideDialog())
        assertTrue(dialogFragment.dialog!!.isShowing)
    }

    private fun clickOutsideDialog(): ViewAction {
        return GeneralClickAction(
            Tap.SINGLE,
            { view ->
                val screenPos = IntArray(2)
                view.getLocationOnScreen(screenPos)
                val screenX = (screenPos[0] + 0).toFloat()
                val screenY = (screenPos[1] - 200).toFloat()
                floatArrayOf(screenX, screenY)
            },
            Press.FINGER,
            0,
            0
        )
    }
}
