import java.util.Properties
import java.io.FileInputStream
import org.jetbrains.kotlin.cli.common.toBooleanLenient

plugins {
    id("com.android.application")
    id("kotlin-android")
    id("kotlin-parcelize")
    id("kotlin-kapt")
    id("androidx.navigation.safeargs.kotlin")
    id("com.diffplug.spotless")
    id("fr.chenry.appversioning.appversioning")
}

val props = Properties().apply {
    runCatching {
        load(FileInputStream(rootProject.file("local.properties")))
    }
}

android {
    val schemaLocation = "$projectDir/schemas"
    val maxApiLevel = 33
    val minApiLvel = 21
    val javaTarget = JavaVersion.VERSION_11

    compileSdk = maxApiLevel
    defaultConfig {
        applicationId = "fr.chenry.android.freshrss"
        minSdk = minApiLvel
        targetSdk = maxApiLevel

        versionCode = appVersions.versionCode
        versionName = appVersions.versionName

        testInstrumentationRunner = "fr.chenry.android.freshrss.testutils.FreshRSSTestRunner"
        testInstrumentationRunnerArguments.putAll(mapOf("clearPackageData" to "true"))
        javaCompileOptions {
            annotationProcessorOptions {
                arguments.putAll(
                    mapOf(
                        "room.schemaLocation" to "$projectDir/schemas",
                        "room.incremental" to "true",
                        "room.expandProjection" to "true"
                    )
                )
            }
        }
    }

    compileOptions {
        sourceCompatibility = javaTarget
        targetCompatibility = javaTarget
    }

    kotlinOptions {
        jvmTarget = javaTarget.majorVersion
        freeCompilerArgs = listOf(
            "-Xcontext-receivers",
            "-Xinline-classes",
            "-Xjvm-default=enable",
            "-opt-in=kotlin.RequiresOptIn"
        )
    }

    sourceSets {
        getByName("androidTest").assets.srcDirs(files(schemaLocation))
    }

    buildTypes {
        val release = getByName("release") {
            isMinifyEnabled = true
            isShrinkResources = true
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
        }

        create("jenkins") {
            initWith(release)
        }

        create("debugrelease") {
            initWith(release)
            signingConfig = signingConfigs.getByName("debug")
        }

        configureEach {
            if(name == "debug" || name == "debugrelease") {
                resValue("string", "debug.hostname", props.getProperty("debug.hostname", ""))
                resValue("string", "debug.username", props.getProperty("debug.username", ""))
                resValue("string", "debug.password", props.getProperty("debug.password", ""))
                resValue("string", "debug.active", props.getProperty("debug.active", "false"))
            } else {
                resValue("string", "debug.hostname", "")
                resValue("string", "debug.username", "")
                resValue("string", "debug.password", "")
                resValue("string", "debug.active", "false")
            }
        }
    }

    applicationVariants.configureEach {
        outputs.all {
            this as com.android.build.gradle.internal.api.BaseVariantOutputImpl
            outputFileName = if(name == "jenkins")
                "FreshRSS-develop-jenkins.apk" else
                "FreshRSS-$versionName-$name.apk"
        }
    }

    buildFeatures {
        dataBinding = true
        viewBinding = true
        compose = true
    }

    composeOptions {
        kotlinCompilerExtensionVersion = "1.3.1"
    }

    testOptions {
        animationsDisabled = true
        unitTests.all {
            it.outputs.upToDateWhen { false }
            it.testLogging {
                events("passed", "skipped", "failed", "standardOut", "standardError")
                showStandardStreams = true
            }
        }
    }

    lint {
        disable.addAll(listOf("AllowBackup", "VectorPath", "GradleDependency", "MissingTranslation", "InvalidPackage"))
    }
    namespace = "fr.chenry.android.freshrss"
}

spotless {
    format("misc") {
        target("**/*.gradle", "**/*.md", "**/*.properties", "**/.gitignore", "**/*.kt")
        trimTrailingWhitespace()
        endWithNewline()
    }

    kotlin {
        target("**/*.kt")
        indentWithSpaces()
        ktlint(libs.versions.ktlint.get()).userData(
            mapOf("trim_trailing_whitespace" to true.toString())
        )
    }
}

dependencies {
    // Kotlin stuff
    implementation("org.jetbrains.kotlin:kotlin-reflect:${libs.versions.kotlin.get()}")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:${libs.versions.kotlinx.coroutines.get()}")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-android:${libs.versions.kotlinx.coroutines.get()}")

    // AndroidX
    implementation("androidx.core:core-ktx:1.8.0")
    implementation("androidx.appcompat:appcompat:${libs.versions.androidx.appcompat.get()}")
    implementation("androidx.appcompat:appcompat-resources:${libs.versions.androidx.appcompat.get()}")
    implementation("androidx.activity:activity-ktx:${libs.versions.androidx.activity.get()}")
    implementation("androidx.swiperefreshlayout:swiperefreshlayout:1.1.0")  // Circular progress bar
    implementation("androidx.constraintlayout:constraintlayout:2.1.4")
    implementation("androidx.recyclerview:recyclerview:1.2.1")
    implementation("androidx.preference:preference-ktx:1.2.0")
    implementation("com.google.android.material:material:1.6.1")

    // ViewModel and LiveData
    implementation("androidx.lifecycle:lifecycle-runtime-ktx:${libs.versions.androidx.lifecycle.get()}")
    implementation("androidx.lifecycle:lifecycle-viewmodel-ktx:${libs.versions.androidx.lifecycle.get()}")
    implementation("androidx.lifecycle:lifecycle-livedata-ktx:${libs.versions.androidx.lifecycle.get()}")
    implementation("androidx.lifecycle:lifecycle-reactivestreams-ktx:${libs.versions.androidx.lifecycle.get()}")
    implementation("androidx.lifecycle:lifecycle-process:${libs.versions.androidx.lifecycle.get()}")
    implementation("androidx.lifecycle:lifecycle-service:${libs.versions.androidx.lifecycle.get()}")
    implementation("androidx.fragment:fragment-ktx:${libs.versions.androidx.fragment.get()}")
    implementation("androidx.navigation:navigation-testing:2.5.3")
    kapt("androidx.lifecycle:lifecycle-compiler:${libs.versions.androidx.lifecycle.get()}")

    // Compose stuff
    implementation("androidx.activity:activity-compose:${libs.versions.androidx.activity.get()}")
    implementation("androidx.compose.material:material:${libs.versions.androidx.compose.get()}")
    implementation("androidx.compose.animation:animation:${libs.versions.androidx.compose.get()}")
    implementation("androidx.compose.ui:ui-tooling:${libs.versions.androidx.compose.get()}")
    implementation("androidx.compose.runtime:runtime-livedata:${libs.versions.androidx.compose.get()}")
    implementation("androidx.navigation:navigation-compose:${libs.versions.androidx.navigation.get()}")
    implementation("androidx.lifecycle:lifecycle-viewmodel-compose:${libs.versions.androidx.lifecycle.get()}")
    implementation("androidx.lifecycle:lifecycle-runtime-compose:${libs.versions.androidx.lifecycle.get()}")
    implementation("com.google.android.material:compose-theme-adapter:1.1.16")
    implementation("com.google.accompanist:accompanist-swiperefresh:${libs.versions.google.accompanist.get()}")
    implementation("com.google.accompanist:accompanist-placeholder-material:${libs.versions.google.accompanist.get()}")

    // Navigation
    implementation("androidx.navigation:navigation-fragment-ktx:${libs.versions.androidx.navigation.get()}")
    implementation("androidx.navigation:navigation-ui-ktx:${libs.versions.androidx.navigation.get()}")

    // Room & roomigrant
    kapt("androidx.room:room-compiler:${libs.versions.androidx.room.get()}")
    implementation("androidx.room:room-ktx:${libs.versions.androidx.room.get()}")
    implementation("androidx.room:room-runtime:${libs.versions.androidx.room.get()}")
    implementation("androidx.room:room-rxjava2:${libs.versions.androidx.room.get()}")
    implementation("io.reactivex.rxjava2:rxandroid:2.1.1")
    implementation("com.github.MatrixDev.Roomigrant:RoomigrantLib:${libs.versions.roomigrant.get()}")
    kapt("com.github.MatrixDev.Roomigrant:RoomigrantCompiler:${libs.versions.roomigrant.get()}")

    // HTTP
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin:${libs.versions.jackson.get()}")
    implementation("com.fasterxml.jackson.datatype:jackson-datatype-joda:${libs.versions.jackson.get()}")
    implementation("com.squareup.retrofit2:retrofit:${libs.versions.squareup.retrofit.get()}")
    implementation("com.squareup.retrofit2:converter-jackson:${libs.versions.squareup.retrofit.get()}")
    implementation("com.squareup.retrofit2:converter-scalars:${libs.versions.squareup.retrofit.get()}")
    implementation("com.squareup.okhttp3:logging-interceptor:${libs.versions.squareup.okhttp.get()}")

    // WorkManager
    implementation("androidx.work:work-runtime-ktx:${libs.versions.androidx.work.get()}")
    implementation("com.google.guava:guava:31.1-android")

    // Utils
    implementation("org.apache.commons:commons-text:1.9")
    implementation("joda-time:joda-time:2.11.1")
    implementation("com.squareup.picasso:picasso:2.71828")
    implementation("com.x5dev:chunk-templates:3.6.2")
    implementation("eu.davidea:flexible-adapter:5.1.0")
    implementation("eu.davidea:flexible-adapter-ui:1.0.0")
    implementation("com.github.skydoves:expandablelayout:1.0.7")
    implementation("org.jsoup:jsoup:1.15.1")
    implementation("io.insert-koin:koin-core:${libs.versions.koin.get()}")
    implementation("io.insert-koin:koin-android:${libs.versions.koin.get()}")
    implementation("com.guolindev.permissionx:permissionx:1.7.1")

    // Bug report
    implementation("ch.acra:acra-http:${libs.versions.acra.get()}")
    implementation("ch.acra:acra-notification:${libs.versions.acra.get()}")
    compileOnly("com.google.auto.service:auto-service-annotations:${libs.versions.autoservice.get()}")
    kapt("com.google.auto.service:auto-service:${libs.versions.autoservice.get()}")

    // Tests
    testImplementation("junit:junit:4.13.2")
    debugImplementation("org.hamcrest:hamcrest-library:2.2")
    debugImplementation("com.squareup.okhttp3:mockwebserver:${libs.versions.squareup.okhttp.get()}")
    debugImplementation("com.github.javafaker:javafaker:1.0.2")

    /*
     * org.json:json is used with explicit permission of its copyright holders :
     *
     * Copyright (c) 2002 JSON.org
     *
     * Permission is hereby granted, free of charge, to any person obtaining a copy
     * of this software and associated documentation files (the "Software"), to deal
     * in the Software without restriction, including without limitation the rights
     * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
     * copies of the Software, and to permit persons to whom the Software is
     * furnished to do so, subject to the following conditions:
     *
     * The above copyright notice and this permission notice shall be included in all
     * copies or substantial portions of the Software.
     *
     * The Software shall be used for Good, not Evil.
     */
    testImplementation("org.json:json:20220320")
    testImplementation("io.mockk:mockk:${libs.versions.mockk.get()}")

    testImplementation("io.insert-koin:koin-test:${libs.versions.koin.get()}")
    testImplementation(libs.kotest.assertionsCore)

    debugImplementation("androidx.test:core:${libs.versions.androidx.test.core.get()}")
    androidTestImplementation("androidx.test:core-ktx:${libs.versions.androidx.test.core.get()}")
    debugImplementation("androidx.test:rules:${libs.versions.androidx.test.core.get()}")
    debugImplementation("androidx.test:runner:${libs.versions.androidx.test.core.get()}")

    androidTestImplementation("androidx.arch.core:core-testing:2.1.0")
    androidTestImplementation("androidx.test.ext:junit:${libs.versions.androidx.test.junit.get()}")
    androidTestImplementation("androidx.test.ext:junit-ktx:${libs.versions.androidx.test.junit.get()}")
    androidTestImplementation("io.mockk:mockk-android:${libs.versions.mockk.get()}")
    debugImplementation("org.jetbrains.kotlinx:kotlinx-coroutines-test:${libs.versions.kotlinx.coroutines.get()}") {
        exclude(group = "org.jetbrains.kotlinx", module = "kotlinx-coroutines-debug")
    }

    androidTestImplementation("io.github.java-diff-utils:java-diff-utils:4.12")

    // Espresso
    androidTestImplementation("androidx.test.espresso:espresso-core:${libs.versions.androidx.test.espresso.get()}")
    androidTestImplementation("androidx.test.espresso:espresso-contrib:${libs.versions.androidx.test.espresso.get()}") {
        exclude(group = "androidx.lifecycle")
    }
    androidTestImplementation("androidx.test.espresso:espresso-web:${libs.versions.androidx.test.espresso.get()}")
    androidTestImplementation("androidx.test.espresso:espresso-intents:${libs.versions.androidx.test.espresso.get()}")
    implementation("androidx.test.espresso:espresso-idling-resource:${libs.versions.androidx.test.espresso.get()}")

    debugImplementation(libs.androidx.fragment.fragmentTesting) {
        exclude(group = "androidx.test", module = "core")
    }

    androidTestImplementation(libs.kotest.assertionsCore)

    androidTestImplementation("androidx.work:work-testing:${libs.versions.androidx.work.get()}")
    androidTestImplementation("androidx.test.uiautomator:uiautomator:2.2.0")
    androidTestImplementation("androidx.room:room-testing:${libs.versions.androidx.room.get()}")
    androidTestImplementation("androidx.navigation:navigation-testing:${libs.versions.androidx.navigation.get()}")
    androidTestImplementation("io.insert-koin:koin-test:${libs.versions.koin.get()}")
    debugImplementation(project(":testlib"))

    androidTestImplementation("androidx.compose.ui:ui-test-junit4:${libs.versions.androidx.compose.get()}") {
        exclude(group = "androidx.activity")
    }
    debugImplementation("androidx.compose.ui:ui-test-manifest:${libs.versions.androidx.compose.get()}")
}
