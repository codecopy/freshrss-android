buildscript {
    repositories {
        google()
        mavenCentral()
    }

    dependencies {
        classpath("com.android.tools.build:gradle:7.4.2")
        classpath(kotlin("gradle-plugin", version = libs.versions.kotlin.get()))
        classpath("androidx.navigation:navigation-safe-args-gradle-plugin:${libs.versions.androidx.navigation.get()}")
        classpath("com.diffplug.spotless:spotless-plugin-gradle:6.12.0")
    }
}

allprojects {
    repositories {
        google()
        maven { url = uri("https://jcenter.bintray.com") }
        maven { url = uri("https://jitpack.io") }
        mavenCentral()
    }
}

tasks.register("lintAllFix") {
    dependsOn("app:lintFix", "app:spotlessApply" )
}
