plugins {
    id("com.android.library")
    id("org.jetbrains.kotlin.android")
}

android {
    compileSdk = 32

    defaultConfig {
        minSdk = 21
        targetSdk = 32
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_11
        targetCompatibility = JavaVersion.VERSION_11
    }

    kotlinOptions {
        jvmTarget = "11"
    }
    buildTypes {
        create("jenkins") {
            initWith(getByName("release"))
        }
    }
    namespace = "fr.chenry.testlib"
}

dependencies {
    debugImplementation(libs.androidx.appcompat)
    debugImplementation(libs.androidx.fragment.fragmentTesting)
}
