package fr.chenry.testlib

import android.annotation.SuppressLint
import android.content.ComponentName
import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.annotation.IdRes
import androidx.annotation.RestrictTo
import androidx.annotation.StyleRes
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentFactory
import androidx.fragment.app.commitNow
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.test.core.app.ActivityScenario
import androidx.test.core.app.ApplicationProvider
import java.io.Closeable

inline fun <reified F : Fragment> launchFragmentInAppCompatContainer(
    fragmentArgs: Bundle? = null,
    @StyleRes themeResId: Int = R.style.AppCompatFragmentScenarioEmptyFragmentActivityTheme,
    initialState: Lifecycle.State = Lifecycle.State.RESUMED,
    factory: FragmentFactory? = null
): AppCompatFragmentScenario<F> = AppCompatFragmentScenario.launchInContainer(
    F::class.java, fragmentArgs, themeResId, initialState,
    factory
)

inline fun <reified F : Fragment, T : Any> AppCompatFragmentScenario<F>.withFragment(
    crossinline block: F.() -> T
): T {
    lateinit var value: T
    var err: Throwable? = null
    onFragment { fragment ->
        try {
            value = block(fragment)
        } catch (t: Throwable) {
            err = t
        }
    }
    err?.let { throw it }
    return value
}

class AppCompatFragmentScenario<F : Fragment> private constructor(
    @Suppress("MemberVisibilityCanBePrivate") /* synthetic access */
    internal val fragmentClass: Class<F>,
    val activityScenario: ActivityScenario<EmptyFragmentAppCompatActivity>
) : Closeable {
    @RestrictTo(RestrictTo.Scope.LIBRARY)
    class EmptyFragmentAppCompatActivity : AppCompatActivity() {
        @SuppressLint("RestrictedApi")
        override fun onCreate(savedInstanceState: Bundle?) {
            setTheme(
                intent.getIntExtra(
                    THEME_EXTRAS_BUNDLE_KEY,
                    R.style.AppCompatFragmentScenarioEmptyFragmentActivityTheme
                )
            )

            val factory = FragmentFactoryHolderViewModel.getInstance(this).fragmentFactory
            if (factory != null) supportFragmentManager.fragmentFactory = factory

            super.onCreate(savedInstanceState)
        }

        companion object {
            const val THEME_EXTRAS_BUNDLE_KEY = "fr.chenry.android.freshrss.testutils.AppCompatFragmentScenario" +
                ".EmptyFragmentAppCompatActivity.THEME_EXTRAS_BUNDLE_KEY"
        }
    }

    @RestrictTo(RestrictTo.Scope.LIBRARY)
    internal class FragmentFactoryHolderViewModel : ViewModel() {
        var fragmentFactory: FragmentFactory? = null

        override fun onCleared() {
            super.onCleared()
            fragmentFactory = null
        }

        companion object {
            @Suppress("MemberVisibilityCanBePrivate")
            internal val FACTORY: ViewModelProvider.Factory =
                object : ViewModelProvider.Factory {
                    @Suppress("UNCHECKED_CAST")
                    override fun <T : ViewModel> create(modelClass: Class<T>): T {
                        val viewModel = FragmentFactoryHolderViewModel()
                        return viewModel as T
                    }
                }

            fun getInstance(activity: FragmentActivity): FragmentFactoryHolderViewModel {
                val viewModel: FragmentFactoryHolderViewModel by activity.viewModels { FACTORY }
                return viewModel
            }
        }
    }

    fun moveToState(newState: Lifecycle.State): AppCompatFragmentScenario<F> {
        if (newState == Lifecycle.State.DESTROYED) {
            activityScenario.onActivity { activity ->
                val fragment = activity.supportFragmentManager
                    .findFragmentByTag(FRAGMENT_TAG)
                // Null means the fragment has been destroyed already.
                if (fragment != null) {
                    activity.supportFragmentManager.commitNow {
                        remove(fragment)
                    }
                }
            }
        } else {
            activityScenario.onActivity { activity ->
                val fragment = requireNotNull(
                    activity.supportFragmentManager.findFragmentByTag(FRAGMENT_TAG)
                ) {
                    "The fragment has been removed from the FragmentManager already."
                }
                activity.supportFragmentManager.commitNow {
                    setMaxLifecycle(fragment, newState)
                }
            }
        }
        return this
    }

    fun recreate(): AppCompatFragmentScenario<F> {
        activityScenario.recreate()
        return this
    }

    fun interface FragmentAction<F : Fragment> {
        fun perform(fragment: F)
    }

    fun onFragment(action: FragmentAction<F>): AppCompatFragmentScenario<F> {
        activityScenario.onActivity { activity ->
            val fragment = requireNotNull(activity.supportFragmentManager.findFragmentByTag(FRAGMENT_TAG)) {
                "The fragment has been removed from the FragmentManager already."
            }
            check(fragmentClass.isInstance(fragment))
            action.perform(requireNotNull(fragmentClass.cast(fragment)))
        }
        return this
    }

    override fun close() {
        activityScenario.close()
    }

    companion object {
        private const val FRAGMENT_TAG = "AppCompatFragmentScenario_Fragment_Tag"

        @JvmOverloads
        @JvmStatic
        fun <F : Fragment> launchInContainer(
            fragmentClass: Class<F>,
            fragmentArgs: Bundle? = null,
            @StyleRes themeResId: Int = R.style.AppCompatFragmentScenarioEmptyFragmentActivityTheme,
            initialState: Lifecycle.State = Lifecycle.State.RESUMED,
            factory: FragmentFactory? = null
        ): AppCompatFragmentScenario<F> = internalLaunch(
            fragmentClass,
            fragmentArgs,
            themeResId,
            initialState,
            factory,
            android.R.id.content
        )

        @SuppressLint("RestrictedApi")
        internal fun <F : Fragment> internalLaunch(
            fragmentClass: Class<F>,
            fragmentArgs: Bundle?,
            @StyleRes themeResId: Int,
            initialState: Lifecycle.State,
            factory: FragmentFactory?,
            @IdRes containerViewId: Int
        ): AppCompatFragmentScenario<F> {
            require(initialState != Lifecycle.State.DESTROYED) {
                "Cannot set initial Lifecycle state to $initialState for AppCompatFragmentScenario"
            }

            val componentName = ComponentName(
                ApplicationProvider.getApplicationContext(),
                EmptyFragmentAppCompatActivity::class.java
            )

            val startActivityIntent = Intent.makeMainActivity(componentName).putExtra(
                EmptyFragmentAppCompatActivity.THEME_EXTRAS_BUNDLE_KEY, themeResId
            )

            val scenario = AppCompatFragmentScenario(fragmentClass, ActivityScenario.launch(startActivityIntent))

            scenario.activityScenario.onActivity { activity ->
                if (factory != null) {
                    FragmentFactoryHolderViewModel.getInstance(activity).fragmentFactory = factory
                    activity.supportFragmentManager.fragmentFactory = factory
                }
                val fragment = activity.supportFragmentManager.fragmentFactory
                    .instantiate(requireNotNull(fragmentClass.classLoader), fragmentClass.name)
                fragment.arguments = fragmentArgs
                activity.supportFragmentManager.commitNow {
                    add(containerViewId, fragment, FRAGMENT_TAG)
                    setMaxLifecycle(fragment, initialState)
                }
            }

            return scenario
        }
    }
}
